<?php



/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('v1/writingAdmin/test/index', 'v1\TestController@index');
Route::group(["prefix"=>"v1/writingAdmin","middleware"=>"AdminApiAuth"],function (){
    // 项目页面
    Route::get('/project/index', 'v1\ProjectController@index');
    // 编辑提交
    Route::put('/project/update', 'v1\ProjectController@update');


    // 用典列表
    Route::get('/allusion/index', 'v1\AllusionController@index');

    // 添加
    Route::post('/allusion/add', 'v1\AllusionController@add');

    // 编辑页面
    Route::get('/allusion/edit/{id}', 'v1\AllusionController@edit');

    // 编辑提交
    Route::put('/allusion/update/{id}', 'v1\AllusionController@update');

    // 调整状态
    Route::put('/allusion/status/{id}', 'v1\AllusionController@status');

    // 排序
    Route::put('/allusion/sorts/{id}', 'v1\AllusionController@sorts');

    // 删除
    Route::delete('/allusion/del/{id}', 'v1\AllusionController@del');

    //批量删除
    Route::delete('/allusion/delAll', 'v1\AllusionController@delAll');


    // 选集列表
    Route::get('/anthology/index', 'v1\AnthologyController@index');

    // 添加
    Route::post('/anthology/add', 'v1\AnthologyController@add');

    // 编辑页面
    Route::get('/anthology/edit/{id}', 'v1\AnthologyController@edit');

    // 编辑提交
    Route::put('/anthology/update/{id}', 'v1\AnthologyController@update');

    // 调整状态
    Route::put('/anthology/status/{id}', 'v1\AnthologyController@status');

    // 排序
    Route::put('/anthology/sorts/{id}', 'v1\AnthologyController@sorts');

    // 删除
    Route::delete('/anthology/del/{id}', 'v1\AnthologyController@del');

    //批量删除
    Route::delete('/anthology/delAll', 'v1\AnthologyController@delAll');



    // 词牌列表
    Route::get('/brand/index', 'v1\BrandController@index');

    // 添加
    Route::post('/brand/add', 'v1\BrandController@add');

    // 编辑页面
    Route::get('/brand/edit/{id}', 'v1\BrandController@edit');

    // 编辑提交
    Route::put('/brand/update/{id}', 'v1\BrandController@update');

    // 调整状态
    Route::put('/brand/status/{id}', 'v1\BrandController@status');

    // 排序
    Route::put('/brand/sorts/{id}', 'v1\BrandController@sorts');

    // 删除
    Route::delete('/brand/del/{id}', 'v1\BrandController@del');

    //批量删除
    Route::delete('/brand/delAll', 'v1\BrandController@delAll');


    // 城市列表
    Route::get('/citysMountain/index', 'v1\CitysMountainController@index');

    // 添加
    Route::post('/citysMountain/add', 'v1\CitysMountainController@add');

    // 编辑页面
    Route::get('/citysMountain/edit/{id}', 'v1\CitysMountainController@edit');

    // 编辑提交
    Route::put('/citysMountain/update/{id}', 'v1\CitysMountainController@update');

    // 调整状态
    Route::put('/citysMountain/status/{id}', 'v1\CitysMountainController@status');

    // 排序
    Route::put('/citysMountain/sorts/{id}', 'v1\CitysMountainController@sorts');

    // 删除
    Route::delete('/citysMountain/del/{id}', 'v1\CitysMountainController@del');

    //批量删除
    Route::delete('/citysMountain/delAll', 'v1\CitysMountainController@delAll');


    // 写景列表
    Route::get('/describeSceneryMountain/index', 'v1\DescribeSceneryMountainController@index');

    // 添加
    Route::post('/describeSceneryMountain/add', 'v1\DescribeSceneryMountainController@add');

    // 编辑页面
    Route::get('/describeSceneryMountain/edit/{id}', 'v1\DescribeSceneryMountainController@edit');

    // 编辑提交
    Route::put('/describeSceneryMountain/update/{id}', 'v1\DescribeSceneryMountainController@update');

    // 调整状态
    Route::put('/describeSceneryMountain/status/{id}', 'v1\DescribeSceneryMountainController@status');

    // 排序
    Route::put('/describeSceneryMountain/sorts/{id}', 'v1\DescribeSceneryMountainController@sorts');

    // 删除
    Route::delete('/describeSceneryMountain/del/{id}', 'v1\DescribeSceneryMountainController@del');

    //批量删除
    Route::delete('/describeSceneryMountain/delAll', 'v1\DescribeSceneryMountainController@delAll');



    // 朝代列表
    Route::get('/dynasty/index', 'v1\DynastyController@index');

    // 添加
    Route::post('/dynasty/add', 'v1\DynastyController@add');

    // 编辑页面
    Route::get('/dynasty/edit/{id}', 'v1\DynastyController@edit');

    // 编辑提交
    Route::put('/dynasty/update/{id}', 'v1\DynastyController@update');

    // 调整状态
    Route::put('/dynasty/status/{id}', 'v1\DynastyController@status');

    // 排序
    Route::put('/dynasty/sorts/{id}', 'v1\DynastyController@sorts');

    // 删除
    Route::delete('/dynasty/del/{id}', 'v1\DynastyController@del');

    //批量删除
    Route::delete('/dynasty/delAll', 'v1\DynastyController@delAll');



    // 名山列表
    Route::get('/famousMountain/index', 'v1\FamousMountainController@index');

    // 添加
    Route::post('/famousMountain/add', 'v1\FamousMountainController@add');

    // 编辑页面
    Route::get('/famousMountain/edit/{id}', 'v1\FamousMountainController@edit');

    // 编辑提交
    Route::put('/famousMountain/update/{id}', 'v1\FamousMountainController@update');

    // 调整状态
    Route::put('/famousMountain/status/{id}', 'v1\FamousMountainController@status');

    // 排序
    Route::put('/famousMountain/sorts/{id}', 'v1\FamousMountainController@sorts');

    // 删除
    Route::delete('/famousMountain/del/{id}', 'v1\FamousMountainController@del');

    //批量删除
    Route::delete('/famousMountain/delAll', 'v1\FamousMountainController@delAll');


    // 节日列表
    Route::get('/festivalsMountain/index', 'v1\FestivalsMountainController@index');

    // 添加
    Route::post('/festivalsMountain/add', 'v1\FestivalsMountainController@add');

    // 编辑页面
    Route::get('/festivalsMountain/edit/{id}', 'v1\FestivalsMountainController@edit');

    // 编辑提交
    Route::put('/festivalsMountain/update/{id}', 'v1\FestivalsMountainController@update');

    // 调整状态
    Route::put('/festivalsMountain/status/{id}', 'v1\FestivalsMountainController@status');

    // 排序
    Route::put('/festivalsMountain/sorts/{id}', 'v1\FestivalsMountainController@sorts');

    // 删除
    Route::delete('/festivalsMountain/del/{id}', 'v1\FestivalsMountainController@del');

    //批量删除
    Route::delete('/festivalsMountain/delAll', 'v1\FestivalsMountainController@delAll');


    // 花卉列表
    Route::get('/flowersPlant/index', 'v1\FlowersPlantController@index');

    // 添加
    Route::post('/flowersPlant/add', 'v1\FlowersPlantController@add');

    // 编辑页面
    Route::get('/flowersPlant/edit/{id}', 'v1\FlowersPlantController@edit');

    // 编辑提交
    Route::put('/flowersPlant/update/{id}', 'v1\FlowersPlantController@update');

    // 调整状态
    Route::put('/flowersPlant/status/{id}', 'v1\FlowersPlantController@status');

    // 排序
    Route::put('/flowersPlant/sorts/{id}', 'v1\FlowersPlantController@sorts');

    // 删除
    Route::delete('/flowersPlant/del/{id}', 'v1\FlowersPlantController@del');

    //批量删除
    Route::delete('/flowersPlant/delAll', 'v1\FlowersPlantController@delAll');

    // 地理列表
    Route::get('/geography/index', 'v1\GeographyController@index');

    // 添加
    Route::post('/geography/add', 'v1\GeographyController@add');

    // 编辑页面
    Route::get('/geography/edit/{id}', 'v1\GeographyController@edit');

    // 编辑提交
    Route::put('/geography/update/{id}', 'v1\GeographyController@update');

    // 调整状态
    Route::put('/geography/status/{id}', 'v1\GeographyController@status');

    // 排序
    Route::put('/geography/sorts/{id}', 'v1\GeographyController@sorts');

    // 删除
    Route::delete('/geography/del/{id}', 'v1\GeographyController@del');

    //批量删除
    Route::delete('/geography/delAll', 'v1\GeographyController@delAll');


    // 诗单列表
    Route::get('/poemBook/index', 'v1\PoemBookController@index');

    // 添加
    Route::post('/poemBook/add', 'v1\PoemBookController@add');

    // 编辑页面
    Route::get('/poemBook/edit/{id}', 'v1\PoemBookController@edit');

    // 编辑提交
    Route::put('/poemBook/update/{id}', 'v1\PoemBookController@update');

    // 调整状态
    Route::put('/poemBook/status/{id}', 'v1\PoemBookController@status');

    // 排序
    Route::put('/poemBook/sorts/{id}', 'v1\PoemBookController@sorts');

    // 删除
    Route::delete('/poemBook/del/{id}', 'v1\PoemBookController@del');

    //批量删除
    Route::delete('/poemBook/delAll', 'v1\PoemBookController@delAll');


    // 时令列表
    Route::get('/season/index', 'v1\SeasonController@index');

    // 添加
    Route::post('/season/add', 'v1\SeasonController@add');

    // 编辑页面
    Route::get('/season/edit/{id}', 'v1\SeasonController@edit');

    // 编辑提交
    Route::put('/season/update/{id}', 'v1\SeasonController@update');

    // 调整状态
    Route::put('/season/status/{id}', 'v1\SeasonController@status');

    // 排序
    Route::put('/season/sorts/{id}', 'v1\SeasonController@sorts');

    // 删除
    Route::delete('/season/del/{id}', 'v1\SeasonController@del');

    //批量删除
    Route::delete('/season/delAll', 'v1\SeasonController@delAll');



    // 节气列表
    Route::get('/solarTerm/index', 'v1\SolarTermController@index');

    // 添加
    Route::post('/solarTerm/add', 'v1\SolarTermController@add');

    // 编辑页面
    Route::get('/solarTerm/edit/{id}', 'v1\SolarTermController@edit');

    // 编辑提交
    Route::put('/solarTerm/update/{id}', 'v1\SolarTermController@update');

    // 调整状态
    Route::put('/solarTerm/status/{id}', 'v1\SolarTermController@status');

    // 排序
    Route::put('/solarTerm/sorts/{id}', 'v1\SolarTermController@sorts');

    // 删除
    Route::delete('/solarTerm/del/{id}', 'v1\SolarTermController@del');

    //批量删除
    Route::delete('/solarTerm/delAll', 'v1\SolarTermController@delAll');



    // 主题列表
    Route::get('/theme/index', 'v1\ThemeController@index');

    // 添加
    Route::post('/theme/add', 'v1\ThemeController@add');

    // 编辑页面
    Route::get('/theme/edit/{id}', 'v1\ThemeController@edit');

    // 编辑提交
    Route::put('/theme/update/{id}', 'v1\ThemeController@update');

    // 调整状态
    Route::put('/theme/status/{id}', 'v1\ThemeController@status');

    // 排序
    Route::put('/theme/sorts/{id}', 'v1\ThemeController@sorts');

    // 删除
    Route::delete('/theme/del/{id}', 'v1\ThemeController@del');

    //批量删除
    Route::delete('/theme/delAll', 'v1\ThemeController@delAll');



    // 时间列表
    Route::get('/timeMsMountain/index', 'v1\TimeMsMountainController@index');

    // 添加
    Route::post('/timeMsMountain/add', 'v1\TimeMsMountainController@add');

    // 编辑页面
    Route::get('/timeMsMountain/edit/{id}', 'v1\TimeMsMountainController@edit');

    // 编辑提交
    Route::put('/timeMsMountain/update/{id}', 'v1\TimeMsMountainController@update');

    // 调整状态
    Route::put('/timeMsMountain/status/{id}', 'v1\TimeMsMountainController@status');

    // 排序
    Route::put('/timeMsMountain/sorts/{id}', 'v1\TimeMsMountainController@sorts');

    // 删除
    Route::delete('/timeMsMountain/del/{id}', 'v1\TimeMsMountainController@del');

    //批量删除
    Route::delete('/timeMsMountain/delAll', 'v1\TimeMsMountainController@delAll');



    // 课本列表
    Route::get('/textbook/index', 'v1\TextbookController@index');

    // 添加
    Route::post('/textbook/add', 'v1\TextbookController@add');

    // 编辑页面
    Route::get('/textbook/edit/{id}', 'v1\TextbookController@edit');

    // 编辑提交
    Route::put('/textbook/update/{id}', 'v1\TextbookController@update');

    // 调整状态
    Route::put('/textbook/status/{id}', 'v1\TextbookController@status');

    // 排序
    Route::put('/textbook/sorts/{id}', 'v1\TextbookController@sorts');

    // 删除
    Route::delete('/textbook/del/{id}', 'v1\TextbookController@del');

    //批量删除
    Route::delete('/textbook/delAll', 'v1\TextbookController@delAll');



    // 存储配置
    Route::post('/config/setStorage', 'v1\ConfigController@setStorage');

    // 获取配置
    Route::get('/config/getStorage', 'v1\ConfigController@getStorage');


    // 作品列表
    Route::get('/work/index', 'v1\WorkController@index');

    // 添加
    Route::post('/work/add', 'v1\WorkController@add');

    // 编辑页面
    Route::get('/work/edit/{id}', 'v1\WorkController@edit');

    // 编辑提交
    Route::put('/work/update/{id}', 'v1\WorkController@update');

    // 调整状态
    Route::put('/work/status/{id}', 'v1\WorkController@status');

    // 排序
    Route::put('/work/sorts/{id}', 'v1\WorkController@sorts');

    // 删除
    Route::delete('/work/del/{id}', 'v1\WorkController@del');

    //批量删除
    Route::delete('/work/delAll', 'v1\WorkController@delAll');

    // 作者列表
    Route::get('/author/index', 'v1\AuthorController@index');

    // 添加
    Route::post('/author/add', 'v1\AuthorController@add');

    // 编辑页面
    Route::get('/author/edit/{id}', 'v1\AuthorController@edit');

    // 编辑提交
    Route::put('/author/update/{id}', 'v1\AuthorController@update');

    // 调整状态
    Route::put('/author/status/{id}', 'v1\AuthorController@status');

    // 排序
    Route::put('/author/sorts/{id}', 'v1\AuthorController@sorts');

    // 删除
    Route::delete('/author/del/{id}', 'v1\AuthorController@del');

    //批量删除
    Route::delete('/author/delAll', 'v1\AuthorController@delAll');

    // 会员管理
    // 列表
    Route::get('/user/index', 'v1\UserController@index');

    // 编辑页面
    Route::get('/user/edit/{id}', 'v1\UserController@edit');

    // 编辑提交
    Route::put('/user/update/{id}', 'v1\UserController@update');

    // 调整状态
    Route::put('/user/status/{id}', 'v1\UserController@status');

    // 删除
    Route::delete('/user/del/{id}', 'v1\UserController@del');

    //批量删除
    Route::delete('/user/delAll', 'v1\UserController@delAll');


    // 图片管理
    // 列表
    Route::get('/picture/index', 'v1\PictureController@index');

    // 添加
    Route::post('/picture/add', 'v1\PictureController@add');

    // 编辑页面
    Route::get('/picture/edit/{id}', 'v1\PictureController@edit');

    // 编辑提交
    Route::put('/picture/update/{id}', 'v1\PictureController@update');

    // 调整状态
    Route::put('/picture/status/{id}', 'v1\PictureController@status');

    // 排序
    Route::put('/picture/sorts/{id}', 'v1\PictureController@sorts');

    // 删除
    Route::delete('/picture/del/{id}', 'v1\PictureController@del');

    //批量删除
    Route::delete('/picture/delAll', 'v1\PictureController@delAll');

    // 通知管理
    // 列表
    Route::get('/notice/index', 'v1\NoticeController@index');

    // 添加
    Route::post('/notice/add', 'v1\NoticeController@add');

    // 编辑页面
    Route::get('/notice/edit/{id}', 'v1\NoticeController@edit');

    // 编辑提交
    Route::put('/notice/update/{id}', 'v1\NoticeController@update');

    // 调整状态
    Route::put('/notice/status/{id}', 'v1\NoticeController@status');

    // 排序
    Route::put('/notice/sorts/{id}', 'v1\NoticeController@sorts');

    // 删除
    Route::delete('/notice/del/{id}', 'v1\NoticeController@del');

    //批量删除
    Route::delete('/notice/delAll', 'v1\NoticeController@delAll');
});


Route::group(["prefix"=>"v1/writing"],function (){

    // 首页
    // 首页名句卡片列表
    Route::get('/index/getQuoteCardList', 'api\v1\IndexController@getQuoteCardList');
    // 图片列表
    Route::get('/index/getPictureList', 'api\v1\IndexController@getPictureList');
    // 图片详情
    Route::get('/index/getPictureDetails', 'api\v1\IndexController@getPictureDetails');

    // 通知列表
    Route::get('/index/getNoticeList', 'api\v1\IndexController@getNoticeList');
    // 通知详情
    Route::get('/index/getNoticeDetails', 'api\v1\IndexController@getNoticeDetails');

    // 书库数据
    Route::get('/library/getLibraryData', 'api\v1\LibraryController@getLibraryData');


    // 作品或名句列表
    Route::get('/work/getWorkList', 'api\v1\WorkController@getWorkList');

    // 作品详情
    Route::get('/work/getWorkDetails', 'api\v1\WorkController@getWorkDetails');

    // 作者列表
    Route::get('/author/getAuthorList', 'api\v1\AuthorController@getAuthorList');

    // 作者详情
    Route::get('/author/getAuthorDetails', 'api\v1\AuthorController@getAuthorDetails');

    // 关于我们
    Route::get('/my/getAboutUs', 'api\v1\MyController@getAboutUs');

    // 登录
    Route::post('/my/login', 'api\v1\MyController@login');
    // 获取用户信息
    Route::get('/my/getUserInfoData', 'api\v1\MyController@getUserInfoData');


    // 获取二维码
    Route::post('/my/getUelQrCode', 'api\v1\MyController@getUelQrCode');



    // 我的登陆后动态信息
    Route::get('/my/getDynamicInformation', 'api\v1\MyController@getDynamicInformation');

    // 收藏作品
    Route::post('/collection/setWorks', 'api\v1\CollectionController@setWorks');

    // 收藏作品列表
    Route::get('/collection/getWorksList', 'api\v1\CollectionController@getWorksList');

    // 收藏作者
    Route::post('/collection/setAuthor', 'api\v1\CollectionController@setAuthor');

    // 收藏作者列表
    Route::get('/collection/getAuthorList', 'api\v1\CollectionController@getAuthorList');
});
