<?php
// +----------------------------------------------------------------------
// | Name: 咪乐多管理系统 [ 为了快速搭建软件应用而生的，希望能够帮助到大家提高开发效率。 ]
// +----------------------------------------------------------------------
// | Copyright: (c) 2020~2021 https://www.lvacms.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed: 这是一个自由软件，允许对程序代码进行修改，但希望您留下原有的注释。
// +----------------------------------------------------------------------
// | Author: 西安咪乐多软件 <997786358@qq.com>
// +----------------------------------------------------------------------
// | Version: V1
// +----------------------------------------------------------------------

/**
 * @Name 服务
 * @Description
 * @Auther 西安咪乐多软件
 * @Date 2021/11/20 19:06
 */
namespace Modules\WritingAdmin\Services\Dynasty;
use Modules\WritingAdmin\Models\WritingDynasty;
use Modules\WritingAdmin\Services\BaseApiServices;
use Modules\Common\Exceptions\MessageData;

class DynastyServices extends BaseApiServices
{
    public function index(array $data)
    {
        $model = WritingDynasty::query();
        $model = $this->setWhereQueryProject($model);
        $model = $this->queryCondition($model, $data, "name");
        if (isset($data['sort'])) {
            $model = $this->querySort($model, $data['sort']);
        }
        $list = $model->with([
                'imageTo'=>function($query){
                    $query->select('id','url','open');
                }
            ])
            ->orderBy('id', 'desc')
            ->paginate($data['limit'])->toArray();
        return $this->apiSuccess('', [
            'list' => $list['data'],
            'total' => $list['total']
        ]);
    }
    public function add(array $data)
    {
        $data = $this->getCommonId($data);
        return $this->commonCreate(WritingDynasty::query(),$data);
    }
    public function edit(int $id)
    {
        $data = WritingDynasty::query()->with([
            'imageTo'=>function($query){
                $query->select('id','url','open');
            }
        ])->find($id)?:$this->apiError(MessageData::GET_API_ERROR);
        $data = $data->toArray();
        return $this->apiSuccess(MessageData::GET_API_SUCCESS,$data);
    }
    public function update(int $id,array $data)
    {
        return $this->commonUpdate(WritingDynasty::query(),$id,$data);
    }
    public function status(int $id,array $data)
    {
        return $this->commonStatusUpdate(WritingDynasty::query(),$id,$data);
    }
    public function sorts(int $id,array $data)
    {
        return $this->commonSortsUpdate(WritingDynasty::query(),$id,$data);
    }
    public function del(int $id)
    {
        return $this->commonDestroy(WritingDynasty::query(),['id'=>$id]);
    }
    public function delAll(array $idArr)
    {
        return $this->commonDestroy(WritingDynasty::query(),$idArr);
    }
}
