<?php
// +----------------------------------------------------------------------
// | Name: 咪乐多管理系统 [ 为了快速搭建软件应用而生的，希望能够帮助到大家提高开发效率。 ]
// +----------------------------------------------------------------------
// | Copyright: (c) 2020~2021 https://www.lvacms.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed: 这是一个自由软件，允许对程序代码进行修改，但希望您留下原有的注释。
// +----------------------------------------------------------------------
// | Author: 西安咪乐多软件 <997786358@qq.com>
// +----------------------------------------------------------------------
// | Version: V1
// +----------------------------------------------------------------------

/**
 * @Name 服务
 * @Description
 * @Auther 西安咪乐多软件
 * @Date 2021/11/20 19:06
 */
namespace Modules\WritingAdmin\Services\Config;
use Illuminate\Support\Facades\Cache;
use Modules\WritingAdmin\Models\WritingAllusion;
use Modules\WritingAdmin\Models\WritingAnthology;
use Modules\WritingAdmin\Models\WritingAuthor;
use Modules\WritingAdmin\Models\WritingBrand;
use Modules\WritingAdmin\Models\WritingCitysMountain;
use Modules\WritingAdmin\Models\WritingDescribeSceneryMountain;
use Modules\WritingAdmin\Models\WritingDynasty;
use Modules\WritingAdmin\Models\WritingFamousMountain;
use Modules\WritingAdmin\Models\WritingFestivalsMountain;
use Modules\WritingAdmin\Models\WritingFlowersPlant;
use Modules\WritingAdmin\Models\WritingGeography;
use Modules\WritingAdmin\Models\WritingPoemBook;
use Modules\WritingAdmin\Models\WritingSeason;
use Modules\WritingAdmin\Models\WritingSolarTerm;
use Modules\WritingAdmin\Models\WritingTextbook;
use Modules\WritingAdmin\Models\WritingTheme;
use Modules\WritingAdmin\Models\WritingTimeMsMountain;
use Modules\WritingAdmin\Services\BaseApiServices;

class ConfigServices extends BaseApiServices
{
    public function setStorage($status = false)
    {
//        $projectId = $this->getProjectId();
        $projectId = 9;
        $allusion_list = WritingAllusion::query()->select('id','name','name_tr','image_id')->where(['project_id'=>$projectId,'status'=>1])->with([
            'imageTo'=>function($query){
                $query->select('id','url','open');
            }
        ])->orderBy('sort','asc')->orderBy('id','desc')->get()->toArray();

        $anthology_list = WritingAnthology::query()->select('id','name','name_tr','image_id')->where(['project_id'=>$projectId,'status'=>1])->with([
            'imageTo'=>function($query){
                $query->select('id','url','open');
            }
        ])->orderBy('sort','asc')->orderBy('id','desc')->get()->toArray();

        $brand_list = WritingBrand::query()->select('id','name','name_tr','image_id')->where(['project_id'=>$projectId,'status'=>1])->with([
            'imageTo'=>function($query){
                $query->select('id','url','open');
            }
        ])->orderBy('sort','asc')->orderBy('id','desc')->get()->toArray();

        $citys_mountain_list = WritingCitysMountain::query()->select('id','name','name_tr','image_id')->where(['project_id'=>$projectId,'status'=>1])->with([
            'imageTo'=>function($query){
                $query->select('id','url','open');
            }
        ])->orderBy('sort','asc')->orderBy('id','desc')->get()->toArray();

        $describe_scenerys_mountain_list = WritingDescribeSceneryMountain::query()->select('id','name','name_tr','image_id')->where(['project_id'=>$projectId,'status'=>1])->with([
            'imageTo'=>function($query){
                $query->select('id','url','open');
            }
        ])->orderBy('sort','asc')->orderBy('id','desc')->get()->toArray();


        $dynasty_list = WritingDynasty::query()->select('id','name','name_tr')->where(['project_id'=>$projectId,'status'=>1])->with([
            'imageTo'=>function($query){
                $query->select('id','url','open');
            }
        ])->orderBy('sort','asc')->orderBy('id','desc')->get()->toArray();

        $famous_mountain_list = WritingFamousMountain::query()->select('id','name','name_tr','image_id')->where(['project_id'=>$projectId,'status'=>1])->with([
            'imageTo'=>function($query){
                $query->select('id','url','open');
            }
        ])->orderBy('sort','asc')->orderBy('id','desc')->get()->toArray();

        $festivals_mountain_list = WritingFestivalsMountain::query()->select('id','name','name_tr','image_id')->where(['project_id'=>$projectId,'status'=>1])->with([
            'imageTo'=>function($query){
                $query->select('id','url','open');
            }
        ])->orderBy('sort','asc')->orderBy('id','desc')->get()->toArray();

        $flowers_plant_list = WritingFlowersPlant::query()->select('id','name','name_tr','image_id')->where(['project_id'=>$projectId,'status'=>1])->with([
            'imageTo'=>function($query){
                $query->select('id','url','open');
            }
        ])->orderBy('sort','asc')->orderBy('id','desc')->get()->toArray();

        $geography_list = WritingGeography::query()->select('id','name','name_tr','image_id')->where(['project_id'=>$projectId,'status'=>1])->with([
            'imageTo'=>function($query){
                $query->select('id','url','open');
            }
        ])->orderBy('sort','asc')->orderBy('id','desc')->get()->toArray();

        $poem_book_list = WritingPoemBook::query()->select('id','name','name_tr','image_id')->where(['project_id'=>$projectId,'status'=>1])->with([
            'imageTo'=>function($query){
                $query->select('id','url','open');
            }
        ])->orderBy('sort','asc')->orderBy('id','desc')->get()->toArray();

        $season_list = WritingSeason::query()->select('id','name','name_tr','image_id')->where(['project_id'=>$projectId,'status'=>1])->with([
            'imageTo'=>function($query){
                $query->select('id','url','open');
            }
        ])->orderBy('sort','asc')->orderBy('id','desc')->get()->toArray();

        $solar_term_list = WritingSolarTerm::query()->select('id','name','name_tr','image_id')->where(['project_id'=>$projectId,'status'=>1])->with([
            'imageTo'=>function($query){
                $query->select('id','url','open');
            }
        ])->orderBy('sort','asc')->orderBy('id','desc')->get()->toArray();

        $textbook_list1 = WritingTextbook::query()->select('id','name','name_tr','image_id')->where(['project_id'=>$projectId,'status'=>1,'type'=>1])->with([
            'imageTo'=>function($query){
                $query->select('id','url','open');
            }
        ])->orderBy('sort','asc')->orderBy('id','desc')->get()->toArray();

        $textbook_list2 = WritingTextbook::query()->select('id','name','name_tr','image_id')->where(['project_id'=>$projectId,'status'=>1,'type'=>2])->with([
            'imageTo'=>function($query){
                $query->select('id','url','open');
            }
        ])->orderBy('sort','asc')->orderBy('id','desc')->get()->toArray();

        $textbook_list3 = WritingTextbook::query()->select('id','name','name_tr','image_id')->where(['project_id'=>$projectId,'status'=>1,'type'=>3])->with([
            'imageTo'=>function($query){
                $query->select('id','url','open');
            }
        ])->orderBy('sort','asc')->orderBy('id','desc')->get()->toArray();

        $textbook_list = WritingTextbook::query()->select('id','name','name_tr','image_id')->where(['project_id'=>$projectId,'status'=>1])->with([
            'imageTo'=>function($query){
                $query->select('id','url','open');
            }
        ])->orderBy('sort','asc')->orderBy('id','desc')->get()->toArray();

        $theme_list = WritingTheme::query()->select('id','name','name_tr','image_id')->where(['project_id'=>$projectId,'status'=>1])->with([
            'imageTo'=>function($query){
                $query->select('id','url','open');
            }
        ])->orderBy('sort','asc')->orderBy('id','desc')->get()->toArray();

        $time_ms_mountain_list = WritingTimeMsMountain::query()->select('id','name','name_tr','image_id')->where(['project_id'=>$projectId,'status'=>1])->with([
            'imageTo'=>function($query){
                $query->select('id','url','open');
            }
        ])->orderBy('sort','asc')->orderBy('id','desc')->get()->toArray();

        $author_list = WritingAuthor::query()->select('id','name','name_tr')->where(['project_id'=>$projectId,'status'=>1])->orderBy('sort','asc')->orderBy('id','desc')->get()->toArray();

        Cache::put('WritingAdminAuthorList',json_encode($author_list,JSON_UNESCAPED_UNICODE));

        $data = [
            'allusion_list'=>$allusion_list,
            'anthology_list'=>$anthology_list,
            'brand_list'=>$brand_list,
            'citys_mountain_list'=>$citys_mountain_list,
            'describe_scenerys_mountain_list'=>$describe_scenerys_mountain_list,
            'dynasty_list'=>$dynasty_list,
            'famous_mountain_list'=>$famous_mountain_list,
            'festivals_mountain_list'=>$festivals_mountain_list,
            'flowers_plant_list'=>$flowers_plant_list,
            'geography_list'=>$geography_list,
            'poem_book_list'=>$poem_book_list,
            'season_list'=>$season_list,
            'solar_term_list'=>$solar_term_list,
            'textbook_list1'=>$textbook_list1,
            'textbook_list2'=>$textbook_list2,
            'textbook_list3'=>$textbook_list3,
            'theme_list'=>$theme_list,
            'time_ms_mountain_list'=>$time_ms_mountain_list,
            'textbook_list'=>$textbook_list,
//            'author_list'=>$author_list
        ];
        Cache::put('WritingAdminConfig',json_encode($data,JSON_UNESCAPED_UNICODE));
        if($status){
            return $data;
        }
        return $this->apiSuccess('存储配置成功');
    }
    public function getStorage($status = false){
        $data = Cache::get('WritingAdminConfig');
        if($data){
            $data = json_decode($data,true);
            if(!$status){
                $author_list = Cache::get('WritingAdminAuthorList');
                if($author_list){
                    $data['author_list'] = json_decode($author_list,true);
                }else{
                    $data['author_list'] = [];
                }
            }
        }else{
            $data = $this->setStorage(true);
        }
        if($status){
            return $data;
        }
        return $this->apiSuccess('',$data);
    }
    public function getLibraryData(){
        $data = $this->getStorage(true);
        $arr = [];
        if($data){
            unset($data['textbook_list']);
            $arr = [
                [
                    "name"=>"小学",
                    "name_tr"=>"小學",
                    "type_name"=>"textbook_id",
                    "children"=>$data['textbook_list1']
                ],
                [
                    "name"=>"初中",
                    "name_tr"=>"初中",
                    "type_name"=>"textbook_id",
                    "children"=>$data['textbook_list2']
                ],
                [
                    "name"=>"高中",
                    "name_tr"=>"高中",
                    "type_name"=>"textbook_id",
                    "children"=>$data['textbook_list3']
                ],
                [
                    "name"=>"用典",
                    "name_tr"=>"用典",
                    "type_name"=>"allusion_id",
                    "children"=>$data['allusion_list']
                ],
                [
                    "name"=>"选集",
                    "name_tr"=>"選集",
                    "type_name"=>"anthology_id",
                    "children"=>$data['anthology_list']
                ],
                [
                    "name"=>"词牌",
                    "name_tr"=>"詞牌",
                    "type_name"=>"brand_id",
                    "children"=>$data['brand_list']
                ],
                [
                    "name"=>"城市",
                    "name_tr"=>"城市",
                    "type_name"=>"city_id",
                    "children"=>$data['citys_mountain_list']
                ],
                [
                    "name"=>"写景",
                    "name_tr"=>"寫景",
                    "type_name"=>"describe_scenery_id",
                    "children"=>$data['describe_scenerys_mountain_list']
                ],
                [
                    "name"=>"朝代",
                    "name_tr"=>"朝代",
                    "type_name"=>"dynasty_id",
                    "children"=>$data['dynasty_list']
                ],
                [
                    "name"=>"名山",
                    "name_tr"=>"名山",
                    "type_name"=>"famous_mountain_id",
                    "children"=>$data['famous_mountain_list']
                ],
                [
                    "name"=>"节日",
                    "name_tr"=>"節日",
                    "type_name"=>"festival_id",
                    "children"=>$data['festivals_mountain_list']
                ],
                [
                    "name"=>"花卉",
                    "name_tr"=>"花卉",
                    "type_name"=>"flowers_plant_id",
                    "children"=>$data['flowers_plant_list']
                ],
                [
                    "name"=>"地理",
                    "name_tr"=>"地理",
                    "type_name"=>"geography_id",
                    "children"=>$data['geography_list']
                ],
                [
                    "name"=>"诗单",
                    "name_tr"=>"詩單",
                    "type_name"=>"poem_book_id",
                    "children"=>$data['poem_book_list']
                ],
                [
                    "name"=>"时令",
                    "name_tr"=>"時令",
                    "type_name"=>"season_id",
                    "children"=>$data['season_list']
                ],
                [
                    "name"=>"节气",
                    "name_tr"=>"節氣",
                    "type_name"=>"solar_term_id",
                    "children"=>$data['solar_term_list']
                ],
                [
                    "name"=>"主题",
                    "name_tr"=>"主題",
                    "type_name"=>"theme_id",
                    "children"=>$data['theme_list']
                ],
                [
                    "name"=>"时间",
                    "name_tr"=>"時間",
                    "type_name"=>"time_m_id",
                    "children"=>$data['time_ms_mountain_list']
                ],
            ];
        }
        return $this->apiSuccess('',$arr);
    }
}
