<?php
// +----------------------------------------------------------------------
// | Name: 咪乐多管理系统 [ 为了快速搭建软件应用而生的，希望能够帮助到大家提高开发效率。 ]
// +----------------------------------------------------------------------
// | Copyright: (c) 2020~2021 https://www.lvacms.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed: 这是一个自由软件，允许对程序代码进行修改，但希望您留下原有的注释。
// +----------------------------------------------------------------------
// | Author: 西安咪乐多软件 <997786358@qq.com>
// +----------------------------------------------------------------------
// | Version: V1
// +----------------------------------------------------------------------

/**
 * @Name 服务
 * @Description
 * @Auther 西安咪乐多软件
 * @Date 2021/11/20 19:06
 */
namespace Modules\WritingAdmin\Services\CollectionAuthor;
use Modules\WritingAdmin\Models\WritingAuthor;
use Modules\WritingAdmin\Models\WritingCollectionAuthor;
use Modules\WritingAdmin\Services\BaseApiServices;

class CollectionAuthorServices extends BaseApiServices
{
    public function setAuthor($data){
        if(!isset($data['id']) || $data['id']<=0 || !isset($data['user_id']) || $data['user_id']<=0){
            $this->apiError('网络错误');
        }
        if(!WritingAuthor::query()->where(['id'=>$data['id']])->value('id')){
            $this->apiError('作者不存在');
        }
        $id = WritingCollectionAuthor::query()->where([
            "project_id"=>$data['project_id'],
            'admin_id'=>1,
            'user_id'=>$data['user_id'],
            'author_id'=>$data['id']
        ])->value('id');
        if($id){
            $res = WritingCollectionAuthor::query()->where(['id'=>$id])->delete();
        }else{
            $res = WritingCollectionAuthor::query()->insert([
                "project_id"=>$data['project_id'],
                'admin_id'=>1,
                'user_id'=>$data['user_id'],
                'author_id'=>$data['id'],
                'created_at'=> date('Y-m-d H:i:s')
            ]);
        }
        if(!$res){
            $this->apiError('网络错误，请重试');
        }
        if($id){
            return $this->apiSuccess('取消收藏成功');
        }else{
            return $this->apiSuccess('收藏成功');
        }
    }
    public function getAuthorList($data){
        $model = WritingCollectionAuthor::query()->select('id','author_id','created_at')
            ->where(['user_id'=>$data['user_id'],"project_id"=>$data['project_id']])
            ->whereHas('author_one',function($quest)use($data){
                $quest->where(['status'=>1]);
            })
            ->with(['author_one'=>function($quest){
                $quest->select('id','name','name_tr','dynasty','dynasty_tr');
            }]);
        if(isset($data['dynasty_id']) && $data['dynasty_id'] > 0){
            $model = $model->whereHas('author_one',function($quest)use($data){
                $quest->whereIn('dynasty_id',explode(',',$data['dynasty_id']));
            });
        }
        $list = $model->orderBy('id','desc')
            ->paginate($data['limit'])->toArray();
        return $this->apiSuccess('', [
            'list' => $list['data'],
            'total' => $list['total']
        ]);
    }
}
