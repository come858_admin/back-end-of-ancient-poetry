<?php
// +----------------------------------------------------------------------
// | Name: 咪乐多管理系统 [ 为了快速搭建软件应用而生的，希望能够帮助到大家提高开发效率。 ]
// +----------------------------------------------------------------------
// | Copyright: (c) 2020~2021 https://www.lvacms.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed: 这是一个自由软件，允许对程序代码进行修改，但希望您留下原有的注释。
// +----------------------------------------------------------------------
// | Author: 西安咪乐多软件 <997786358@qq.com>
// +----------------------------------------------------------------------
// | Version: V1
// +----------------------------------------------------------------------

/**
 * @Name
 * @Description
 * @Auther 西安咪乐多软件
 * @Date 2021/11/20 19:06
 */

namespace Modules\WritingAdmin\Services\Notice;

use Modules\WritingAdmin\Models\WritingNotice;
use Modules\WritingAdmin\Services\BaseApiServices;
use Modules\Common\Exceptions\MessageData;

class NoticeServices extends BaseApiServices
{
    public function index(array $data){
        $model = WritingNotice::query();
        $model = $this->setWhereQueryProject($model);
        $model = $this->queryCondition($model,$data,"content");
        if(isset($data['sort'])){
            $model = $this->querySort($model,$data['sort']);
        }
        if(isset($data['type']) && $data['type'] > 0){
            $model = $model->where('type',$data['type']);
        }
        if(isset($data['open']) && $data['open'] > 0){
            $model = $model->where('open',$data['open']);
        }
        $list = $model->orderBy('id','desc')
            ->paginate($data['limit'])->toArray();
        return $this->apiSuccess('',[
            'list'=>$list['data'],
            'total'=>$list['total']
        ]);
    }


    public function add(array $data){
        $data = $this->getCommonId($data);
        if($data['content']){
            $data['content'] = $this->setPicUrl($data['content']);
        }
        return $this->commonCreate(WritingNotice::query(),$data);
    }

    public function edit(int $id){
        $data = WritingNotice::query()->find($id)?:$this->apiError(MessageData::GET_API_ERROR);
        $data = $data->toArray();
        if($data['content']){
            $data['content'] = $this->getReplacePicUrl($data['content']);
        }
        return $this->apiSuccess(MessageData::GET_API_SUCCESS,$data);
    }

    public function update(int $id,array $data){
        if($data['content']){
            $data['content'] = $this->setPicUrl($data['content']);
        }
        return $this->commonUpdate(WritingNotice::query(),$id,$data);
    }
    public function status(int $id,array $data){
        return $this->commonStatusUpdate(WritingNotice::query(),$id,$data);
    }
    public function sorts(int $id,array $data){
        return $this->commonSortsUpdate(WritingNotice::query(),$id,$data);
    }
    public function del(int $id){
        return $this->commonDestroy(WritingNotice::query(),['id'=>$id]);
    }
    public function delAll(array $idArr){
        return $this->commonDestroy(WritingNotice::query(),$idArr);
    }

    public function getNoticeList($data){
        $model = WritingNotice::query();
        if(isset($data['type']) && $data['type'] > 0){
            $model = $model->where('type',$data['type']);
        }
        $list = $model->where(['project_id'=>$data['project_id'],'status'=>1])
            ->select('id','open','url','title')
            ->orderBy('sort','asc')
            ->orderBy('id','desc')
            ->get()->toArray();
        return $this->apiSuccess('',$list);
    }

    public function getNoticeDetails($data){
        if(!isset($data['id']) || $data['id'] <= 0){
            $this->apiError('缺少或错误参数id');
        }
        $model = WritingNotice::query();
        $info = $model->select('id','title','content')->where(['project_id'=>$data['project_id'],'status'=>1,'id'=>$data['id']])->first();
        if(!$info){
            $this->apiError('暂无数据');
        }
        $info = $info->toArray();
        if($info['content']){
            $info['content'] = $this->getReplacePicUrl($info['content']);
        }
        return $this->apiSuccess('',$info);
    }
}
