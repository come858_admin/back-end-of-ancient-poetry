<?php
// +----------------------------------------------------------------------
// | Name: 咪乐多管理系统 [ 为了快速搭建软件应用而生的，希望能够帮助到大家提高开发效率。 ]
// +----------------------------------------------------------------------
// | Copyright: (c) 2020~2021 https://www.lvacms.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed: 这是一个自由软件，允许对程序代码进行修改，但希望您留下原有的注释。
// +----------------------------------------------------------------------
// | Author: 西安咪乐多软件 <997786358@qq.com>
// +----------------------------------------------------------------------
// | Version: V1
// +----------------------------------------------------------------------

/**
 * @Name 项目管理模型
 * @Description
 * @Auther 西安咪乐多软件
 * @Date 2021/11/20 19:03
 */

namespace Modules\WritingAdmin\Models;


use Modules\Admin\Models\AdminImage;

class AdminProject extends BaseApiModel
{
    /**
     * @name 站点logo关联图片   多对一
     * @author 西安咪乐多软件
     * @date 2021/11/21 17:12
     **/
    public function logoImage()
    {
        return $this->belongsTo(AdminImage::class,'logo_id','id');
    }
    /**
     * @name 站点ico关联图片   多对一
     * @author 西安咪乐多软件
     * @date 2021/11/21 17:12
     **/
    public function icoImage()
    {
        return $this->belongsTo(AdminImage::class,'ico_id','id');
    }

}
