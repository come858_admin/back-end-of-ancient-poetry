<?php

//use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Jialeo\LaravelSchemaExtend\Schema;

class CreateWritingTimeMsMountainsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('writing_time_ms_mountains', function (Blueprint $table) {
            $table->comment = '时间表';
            $table->increments('id')->comment('时间ID');
            $table->integer('project_id')->comment('项目ID');
            $table->integer('admin_id')->comment('管理员ID');
            $table->string('name',30)->default('')->comment('名称');
            $table->string('name_tr',30)->default('')->comment('名称繁体');
            $table->integer('image_id')->nullable()->comment('图片ID');
            $table->text('content')->nullable()->comment('内容');
            $table->text('content_tr')->nullable()->comment('内容繁体');
            $table->text('baidu_wiki')->nullable()->comment('抓取地址');
            $table->tinyInteger('status')->default(1)->comment('状态:0=禁用,1=启用');
            $table->integer('sort')->default(1)->comment('排序');
            $table->timestamp('created_at')->nullable()->comment('创建时间');
            $table->timestamp('updated_at')->nullable()->comment('更新时间');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('writing_time_ms_mountains');
    }
}
