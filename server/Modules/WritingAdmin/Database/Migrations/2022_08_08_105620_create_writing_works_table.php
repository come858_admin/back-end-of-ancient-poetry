<?php

//use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Jialeo\LaravelSchemaExtend\Schema;

class CreateWritingWorksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('writing_works', function (Blueprint $table) {
            $table->comment = '作品表';
            $table->increments('id')->comment('作品ID');
            $table->integer('project_id')->comment('项目ID');
            $table->integer('admin_id')->comment('管理员ID');
            $table->text('title')->nullable()->comment('标题');
            $table->text('title_tr')->nullable()->comment('标题繁体');
            $table->integer('author_id')->nullable()->comment('作者ID');
            $table->string('author',50)->nullable()->default('')->comment('作者');
            $table->string('author_tr',50)->nullable()->default('')->comment('作者繁体');
            $table->integer('dynasty_id')->nullable()->comment('朝代ID');
            $table->string('dynasty',30)->nullable()->default('')->comment('朝代');
            $table->string('dynasty_tr',30)->nullable()->default('')->comment('朝代繁体');
            $table->string('kind',20)->nullable()->default('')->comment('体裁拼音');
            $table->string('kind_cn',20)->nullable()->default('')->comment('体裁');
            $table->string('kind_cn_tr',20)->nullable()->default('')->comment('体裁繁体');
            $table->text('baidu_wiki')->nullable()->comment('抓取地址');
            $table->longtext('content')->nullable()->comment('内容');
            $table->longtext('content_tr')->nullable()->comment('内容繁体');
            $table->longtext('intro')->nullable()->comment('评析');
            $table->longtext('intro_tr')->nullable()->comment('评析繁体');
            $table->longtext('annotation')->nullable()->comment('注释');
            $table->longtext('annotation_tr')->nullable()->comment('注释繁体');
            $table->longtext('translation')->nullable()->comment('译文');
            $table->longtext('translation_tr')->nullable()->comment('译文繁体');
            $table->longtext('master_comment')->nullable()->comment('辑评');
            $table->longtext('master_comment_tr')->nullable()->comment('辑评繁体');
            $table->string('quote')->nullable()->default('')->comment('名句');
            $table->string('quote_tr')->nullable()->default('')->comment('名句繁体');
            $table->tinyInteger('status')->default(1)->comment('状态:0=禁用,1=启用');
            $table->integer('sort')->default(1)->comment('排序');
            $table->integer('allusion_id')->nullable()->comment('用典ID');
            $table->integer('anthology_id')->nullable()->comment('选集ID');
            $table->integer('theme_id')->nullable()->comment('主题ID');
            $table->integer('brand_id')->nullable()->comment('词牌ID');
            $table->integer('geography_id')->nullable()->comment('地理ID');
            $table->integer('famous_mountain_id')->nullable()->comment('名山ID');
            $table->integer('city_id')->nullable()->comment('城市ID');
            $table->integer('time_m_id')->nullable()->comment('时间ID');
            $table->integer('describe_scenery_id')->nullable()->comment('写景ID');
            $table->integer('festival_id')->nullable()->comment('节日ID');
            $table->integer('solar_term_id')->nullable()->comment('节气ID');
            $table->integer('season_id')->nullable()->comment('时令ID');
            $table->integer('flowers_plant_id')->nullable()->comment('花卉ID');
            $table->integer('textbook_id')->nullable()->comment('课本ID');
            $table->integer('poem_book_id')->nullable()->comment('诗单ID');
            $table->integer('pv_exhibition_number')->default(0)->nullable()->comment('展示PV数');
            $table->integer('uv_exhibition_number')->default(0)->nullable()->comment('展示UV数');
            $table->timestamp('created_at')->nullable()->comment('创建时间');
            $table->timestamp('updated_at')->nullable()->comment('更新时间');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('writing_works');
    }
}
