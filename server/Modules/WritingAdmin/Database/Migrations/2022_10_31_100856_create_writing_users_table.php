<?php

//use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Jialeo\LaravelSchemaExtend\Schema;

class CreateWritingUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('writing_users', function (Blueprint $table) {
            $table->comment = '会员管理表';
            $table->increments('id')->comment('会员管理ID');
            $table->integer('project_id')->comment('项目ID');
            $table->integer('admin_id')->comment('管理员ID');
            $table->string('gzh_open_id')->nullable()->default('')->comment('微信公众号open_id');
            $table->string('xcx_open_id')->nullable()->default('')->comment('微信小程序open_id');
            $table->string('zj_open_id')->nullable()->default('')->comment('字节小程序open_id');
            $table->string('nick_name')->nullable()->default('')->comment('昵称');
            $table->string('avatar_url')->nullable()->default('')->comment('头像');
            $table->string('name')->nullable()->default('')->comment('姓名');
            $table->string('mobile')->nullable()->default('')->comment('电话');
            $table->tinyInteger('type')->nullable()->default(1)->comment('用户类型：1=普通用户，2=业务员');
            $table->integer('pid')->nullable()->default(0)->comment('上级ID');
            $table->tinyInteger('status')->default(1)->comment('是否为顶级业务员:0=否,1=是');
            $table->tinyInteger('open')->default(1)->comment('用户渠道:1=微信,2=抖音');
            $table->timestamp('created_at')->nullable()->comment('创建时间');
            $table->timestamp('updated_at')->nullable()->comment('更新时间');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('writing_users');
    }
}
