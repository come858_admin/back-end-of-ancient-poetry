<?php

//use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Jialeo\LaravelSchemaExtend\Schema;

class CreateWritingWorkTextbooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('writing_work_textbooks', function (Blueprint $table) {
            $table->comment = '课本和作品关联表';
            $table->increments('id')->comment('课本和作品关联ID');
            $table->integer('work_id')->nullable()->comment('作品ID');
            $table->integer('textbook_id')->nullable()->comment('课本ID');
            $table->timestamp('created_at')->nullable()->comment('创建时间');
            $table->timestamp('updated_at')->nullable()->comment('更新时间');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('writing_work_textbooks');
    }
}
