<?php

namespace Modules\WritingAdmin\Http\Requests\Common;

use Illuminate\Foundation\Http\FormRequest;

class UserPageRequest extends FormRequest
{
    /**
     * php artisan module:make-request AdminRequest Admin
     */

    public function authorize()
    {
        return true;
    }
	public function rules()
    {
        return [
			'limit' 	            => 'required|is_positive_integer',
			'page'      	        => 'required|is_positive_integer',
            "project_id"            =>'required|is_positive_integer',
            "user_id"               =>'required|is_positive_integer',
        ];
    }
	public function messages(){
		return [
			'limit.required' 				    => '缺少参数limit！',
			'limit.is_positive_integer' 		=> '参数错误limit！',

            'page.required' 				    => '缺少参数page！',
            'page.is_positive_integer' 		    => '参数错误page！',

            'project_id.required'               =>'缺少参数project_id',
            'project_id.is_positive_integer'    =>'project_id参数格式错误',

            'user_id.required'                  =>'缺少参数user_id',
            'user_id.is_positive_integer'       =>'user_id参数格式错误',
		];
	}
}









