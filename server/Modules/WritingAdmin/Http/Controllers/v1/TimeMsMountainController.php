<?php
// +----------------------------------------------------------------------
// | Name: 咪乐多管理系统 [ 为了快速搭建软件应用而生的，希望能够帮助到大家提高开发效率。 ]
// +----------------------------------------------------------------------
// | Copyright: (c) 2020~2021 https://www.lvacms.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed: 这是一个自由软件，允许对程序代码进行修改，但希望您留下原有的注释。
// +----------------------------------------------------------------------
// | Author: 西安咪乐多软件 <997786358@qq.com>
// +----------------------------------------------------------------------
// | Version: V1
// +----------------------------------------------------------------------



namespace Modules\WritingAdmin\Http\Controllers\v1;


use Illuminate\Http\Request;
use Modules\Common\Requests\CommonIdArrRequest;
use Modules\Common\Requests\CommonPageRequest;
use Modules\Common\Requests\CommonSortRequest;
use Modules\Common\Requests\CommonStatusRequest;
use Modules\WritingAdmin\Http\Controllers\BaseApiControllers;
use Modules\WritingAdmin\Services\TimeMsMountain\TimeMsMountainServices;

class TimeMsMountainController extends BaseApiControllers
{
    public function index(CommonPageRequest $request)
    {
        return (new TimeMsMountainServices())->index($request->only([
            "limit",
            "name",
            "created_at",
            "updated_at",
            "status",
            "sort"
        ]));
    }
    public function add(Request $request)
    {
        return (new TimeMsMountainServices())->add($request->only([
            "name",
            "name_tr",
            "image_id",
            "content",
            "content_tr",
            "baidu_wiki",
            "status",
            "sort"
        ]));
    }
    public function edit(int $id)
    {
        return (new TimeMsMountainServices())->edit($id);
    }
    public function update(Request $request,int $id)
    {
        return (new TimeMsMountainServices())->update($id,$request->only([
            "name",
            "name_tr",
            "image_id",
            "content",
            "content_tr",
            "baidu_wiki",
            "status",
            "sort"
        ]));
    }
    public function status(CommonStatusRequest $request,int $id){
        return (new TimeMsMountainServices())->status($id,$request->only([
            "status"
        ]));
    }
    public function sorts(CommonSortRequest $request,int $id){
        return (new TimeMsMountainServices())->sorts($id,$request->only([
            "sort"
        ]));
    }
    public function del(int $id){
        return (new TimeMsMountainServices())->del($id);
    }
    public function delAll(CommonIdArrRequest $request)
    {
        return (new TimeMsMountainServices())->delAll($request->get('idArr'));
    }
}
