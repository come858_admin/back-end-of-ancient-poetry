<?php
// +----------------------------------------------------------------------
// | Name: 咪乐多管理系统 [ 为了快速搭建软件应用而生的，希望能够帮助到大家提高开发效率。 ]
// +----------------------------------------------------------------------
// | Copyright: (c) 2020~2021 https://www.lvacms.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed: 这是一个自由软件，允许对程序代码进行修改，但希望您留下原有的注释。
// +----------------------------------------------------------------------
// | Author: 西安咪乐多软件 <997786358@qq.com>
// +----------------------------------------------------------------------
// | Version: V1
// +----------------------------------------------------------------------



namespace Modules\WritingAdmin\Http\Controllers\v1;


use Illuminate\Http\Request;
use Modules\Common\Requests\CommonIdArrRequest;
use Modules\Common\Requests\CommonPageRequest;
use Modules\Common\Requests\CommonSortRequest;
use Modules\Common\Requests\CommonStatusRequest;
use Modules\WritingAdmin\Http\Controllers\BaseApiControllers;
use Modules\WritingAdmin\Services\Work\WorkServices;

class WorkController extends BaseApiControllers
{
    public function index(CommonPageRequest $request)
    {
        return (new WorkServices())->index($request->only([
            "limit",
            "title",
            "created_at",
            "updated_at",
            "status",
            "sort",
            "author_id",
            "dynasty_id",
            "allusion_id",
            "anthology_id",
            "theme_id",
            "brand_id",
            "geography_id",
            "famous_mountain_id",
            "city_id",
            "time_m_id",
            "describe_scenery_id",
            "festival_id",
            "solar_term_id",
            "season_id",
            "flowers_plant_id",
            "textbook_id",
            "poem_book_id"
        ]));
    }
    public function add(Request $request)
    {
        return (new WorkServices())->add($request->only([
            "title",
            "title_tr",
            "author_id",
            "author",
            "author_tr",
            "dynasty_id",
            "dynasty",
            "dynasty_tr",
            "kind",
            "kind_cn",
            "kind_cn_tr",
            "baidu_wiki",
            "content",
            "content_tr",
            "intro",
            "intro_tr",
            "annotation",
            "annotation_tr",
            "translation",
            "translation_tr",
            "master_comment",
            "master_comment_tr",
            "quote",
            "quote_tr",
            "status",
            "sort",
            "allusion_id",
            "anthology_id",
            "theme_id",
            "brand_id",
            "geography_id",
            "famous_mountain_id",
            "city_id",
            "time_m_id",
            "describe_scenery_id",
            "festival_id",
            "solar_term_id",
            "season_id",
            "flowers_plant_id",
            "textbook_id",
            "poem_book_id",
            "textbook_to"
        ]));
    }
    public function edit(int $id)
    {
        return (new WorkServices())->edit($id);
    }
    public function update(Request $request,int $id)
    {
        return (new WorkServices())->update($id,$request->only([
            "title",
            "title_tr",
            "author_id",
            "author",
            "author_tr",
            "dynasty_id",
            "dynasty",
            "dynasty_tr",
            "kind",
            "kind_cn",
            "kind_cn_tr",
            "baidu_wiki",
            "content",
            "content_tr",
            "intro",
            "intro_tr",
            "annotation",
            "annotation_tr",
            "translation",
            "translation_tr",
            "master_comment",
            "master_comment_tr",
            "quote",
            "quote_tr",
            "status",
            "sort",
            "allusion_id",
            "anthology_id",
            "theme_id",
            "brand_id",
            "geography_id",
            "famous_mountain_id",
            "city_id",
            "time_m_id",
            "describe_scenery_id",
            "festival_id",
            "solar_term_id",
            "season_id",
            "flowers_plant_id",
            "textbook_id",
            "poem_book_id",
            "textbook_to"
        ]));
    }
    public function status(CommonStatusRequest $request,int $id){
        return (new WorkServices())->status($id,$request->only([
            "status"
        ]));
    }
    public function sorts(CommonSortRequest $request,int $id){
        return (new WorkServices())->sorts($id,$request->only([
            "sort"
        ]));
    }
    public function del(int $id){
        return (new WorkServices())->del($id);
    }
    public function delAll(CommonIdArrRequest $request)
    {
        return (new WorkServices())->delAll($request->get('idArr'));
    }
}
