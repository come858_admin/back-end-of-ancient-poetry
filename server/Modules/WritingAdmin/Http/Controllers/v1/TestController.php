<?php
// +----------------------------------------------------------------------
// | Name: 咪乐多管理系统 [ 为了快速搭建软件应用而生的，希望能够帮助到大家提高开发效率。 ]
// +----------------------------------------------------------------------
// | Copyright: (c) 2020~2021 https://www.lvacms.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed: 这是一个自由软件，允许对程序代码进行修改，但希望您留下原有的注释。
// +----------------------------------------------------------------------
// | Author: 西安咪乐多软件 <997786358@qq.com>
// +----------------------------------------------------------------------
// | Version: V1
// +----------------------------------------------------------------------



namespace Modules\WritingAdmin\Http\Controllers\v1;


use Illuminate\Http\Request;
use Modules\WritingAdmin\Http\Controllers\BaseApiControllers;
use Modules\WritingAdmin\Services\Command\Readable1Services;

class TestController extends BaseApiControllers
{
    public function index(Request $request)
    {
//        return (new Readable1Services())->index($request->only(['type','x-lc-id','x-lc-sign','i']));
        return (new Readable1Services())->setGenerateAudio($request->get('fileUrl'));
    }
}
