<?php
// +----------------------------------------------------------------------
// | Name: 咪乐多管理系统 [ 为了快速搭建软件应用而生的，希望能够帮助到大家提高开发效率。 ]
// +----------------------------------------------------------------------
// | Copyright: (c) 2020~2021 https://www.lvacms.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed: 这是一个自由软件，允许对程序代码进行修改，但希望您留下原有的注释。
// +----------------------------------------------------------------------
// | Author: 西安咪乐多软件 <997786358@qq.com>
// +----------------------------------------------------------------------
// | Version: V1
// +----------------------------------------------------------------------



namespace Modules\WritingAdmin\Http\Controllers\v1;


use Illuminate\Http\Request;
use Modules\Common\Requests\CommonIdArrRequest;
use Modules\Common\Requests\CommonPageRequest;
use Modules\Common\Requests\CommonSortRequest;
use Modules\Common\Requests\CommonStatusRequest;
use Modules\WritingAdmin\Http\Controllers\BaseApiControllers;
use Modules\WritingAdmin\Services\Author\AuthorServices;
use Modules\WritingAdmin\Services\Work\WorkServices;

class AuthorController extends BaseApiControllers
{
    public function index(CommonPageRequest $request)
    {
        return (new AuthorServices())->index($request->only([
            "limit",
            "name",
            "created_at",
            "updated_at",
            "status",
            "dynasty_id",
            "sort"
        ]));
    }
    public function add(Request $request)
    {
        return (new AuthorServices())->add($request->only([
            "name",
            "name_tr",
            "death_year",
            "birth_year",
            "desc",
            "desc_tr",
            "dynasty_id",
            "dynasty",
            "dynasty_tr",
            "baidu_wiki",
            "status",
            "sort",
            "works_count",
        ]));
    }
    public function edit(int $id)
    {
        return (new AuthorServices())->edit($id);
    }
    public function update(Request $request,int $id)
    {
        return (new AuthorServices())->update($id,$request->only([
            "name",
            "name_tr",
            "death_year",
            "birth_year",
            "desc",
            "desc_tr",
            "dynasty_id",
            "dynasty",
            "dynasty_tr",
            "baidu_wiki",
            "status",
            "sort",
            "works_count",
        ]));
    }
    public function status(CommonStatusRequest $request,int $id){
        return (new AuthorServices())->status($id,$request->only([
            "status"
        ]));
    }
    public function sorts(CommonSortRequest $request,int $id){
        return (new AuthorServices())->sorts($id,$request->only([
            "sort"
        ]));
    }
    public function del(int $id){
        return (new AuthorServices())->del($id);
    }
    public function delAll(CommonIdArrRequest $request)
    {
        return (new AuthorServices())->delAll($request->get('idArr'));
    }
}
