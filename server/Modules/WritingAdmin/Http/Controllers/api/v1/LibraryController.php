<?php


namespace Modules\WritingAdmin\Http\Controllers\api\v1;


use Illuminate\Http\Request;
use Modules\WritingAdmin\Http\Controllers\BaseApiControllers;
use Modules\WritingAdmin\Services\Config\ConfigServices;

class LibraryController extends BaseApiControllers
{
    public function getLibraryData(){
        return (new ConfigServices())->getLibraryData();
    }
}
