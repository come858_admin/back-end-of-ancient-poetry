<?php


namespace Modules\WritingAdmin\Http\Controllers\api\v1;

use Modules\WritingAdmin\Http\Controllers\BaseApiControllers;
use Modules\WritingAdmin\Http\Requests\Common\PageRequest;
use Modules\WritingAdmin\Http\Requests\Common\ProjectRequest;
use Modules\WritingAdmin\Services\Author\AuthorServices;

class AuthorController extends BaseApiControllers
{
    public function getAuthorList(PageRequest $request){
        return (new AuthorServices())->getAuthorList($request->only([
            "project_id",
            "limit",
            "key",
            "dynasty_id"
        ]));
    }

    public function getAuthorDetails(ProjectRequest $request){
        return (new AuthorServices())->getAuthorDetails($request->only([
            "project_id",
            "id",
            "user_id"
        ]));
    }
}
