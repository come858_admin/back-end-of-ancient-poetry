<?php

namespace Modules\WritingAdmin\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Modules\WritingAdmin\Services\Command\Readable1Services;

class Creategetdxcn5ItemQueue implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $data;

    /**
     * 任务尝试次数
     *
     * @var int
     */
    public $tries = 10;

    /**
     * 任务失败前允许的最大异常数
     *
     * @var int
     */
    public $maxExceptions = 3;

    /**
     * 在超时之前任务可以运行的秒数
     *
     * @var int
     */
    public $timeout = 3000;
    public $sleep = 30;

    /**
     * Create a new job instance.
     *
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        (new Readable1Services())->getDetails5Job($this->data['id'],$this->data['url'],$this->data['collectionId'],$this->data['page'],$this->data['perPage'],$this->data['data']);
    }
}
