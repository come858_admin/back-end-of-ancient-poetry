<?php


namespace Modules\Common\Lib\Crawler\Video;
use QL\QueryList;

class Bz implements Index
{
    private $status = 1;
    private $message = 'B站视频提取成功';
    public function index(string $url,string $httpUrl)
    {
        return $this->init($url,$httpUrl);
    }
    private function init(string $url,string $httpUrl){
        $video_url = null;
        $title = null;
        $info = QueryList::get($url)->getHtml();
        preg_match_all("/<meta data-vue-meta=\"true\" itemprop=\"url\" content=\"(.*?)\/\">/",$info,$info);
        if(!isset($info[1][0]) || !$info[1][0]){
            $this->message = "你要观看的视频不存在";
            $this->status = 0;
        }else{
            $info = QueryList::get($info[1][0])->getHtml();
            preg_match_all("/<script>window.__INITIAL_STATE__=(.*?)\;\(function\(\)\{var s\;\(s=document.currentScript\|\|document.scripts\[document.scripts.length-1\]\).parentNode.removeChild\(s\)\;\}\(\)\)\;<\/script>/",$info,$info);
            if(!isset($info[1][0]) || !$info[1][0]){
                $this->message = "你要观看的视频不存在";
                $this->status = 0;
            }else{
//                dd($info[1][0]);
                $info = json_decode($info[1][0],true);
                if(!isset($info['aid']) || !isset($info['videoData']['cid']) || !isset($info['videoData']['title'])){
                    $this->message = "你要观看的视频不存在";
                    $this->status = 0;
                }else{
                    $avid = $info['aid'];
                    $cid = $info['videoData']['cid'];
                    $title = $info['videoData']['title'];
                    $url = "https://api.bilibili.com/x/player/playurl?avid={$avid}&cid={$cid}&qn=16&type=mp4&platform=html5";
                    $res = $this->httpRequest($url);
                    $res = json_decode($res['body'],true);
                    if(isset($res['code']) && $res['code'] == 0 && isset($res['data']['durl'][0]['url']) && $res['data']['durl'][0]['url']){
                        $video_url = $res['data']['durl'][0]['url'];
                    }
                }
            }
        }
        return [
            'status'=>$this->status,
            'data'=>[
                'video_url'=>$video_url,
                'title'=>$title
            ],
            'message'=>$this->message
        ];
    }


    private function httpRequest($url, $data = null, $cookieFile = false, $headers = false, $proxy = false, $outTime = 5)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
        if (!empty($data)) {
            curl_setopt($curl, CURLOPT_POST, 1);
            if (is_array($data)) {
                curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
            } else {
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
            }
        }
        if ($cookieFile) {
            curl_setopt($curl, CURLOPT_COOKIEJAR, $cookieFile);
            curl_setopt($curl, CURLOPT_COOKIEFILE, $cookieFile);
        }
        if (is_array($headers)) {
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        }
        if (is_array($proxy)) {
            curl_setopt($curl, CURLOPT_PROXY, $proxy['ip']);
            curl_setopt($curl, CURLOPT_PROXYPORT, $proxy['port']);
        } else if (is_string($proxy)) {
            curl_setopt($curl, CURLOPT_PROXY, $proxy);
        }
        curl_setopt($curl, CURLOPT_MAXREDIRS, 3);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($curl, CURLOPT_TIMEOUT, $outTime);
        curl_setopt($curl, CURLOPT_HEADER, true);
        curl_setopt($curl, CURLINFO_HEADER_OUT, true);
        $response = curl_exec($curl);
        $curlInfo = curl_getinfo($curl);
        $header = substr($response, 0, $curlInfo['header_size']); // 根据头大小获取头信息
        $body = substr($response, $curlInfo['header_size']);

        curl_close($curl);
        return [
            'header' => $header,
            'body' => $body,
            'request_header' => $curlInfo['request_header']
        ];
    }
}
