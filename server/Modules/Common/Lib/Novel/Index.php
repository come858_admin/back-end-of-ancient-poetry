<?php


namespace Modules\Common\Lib\Novel;


interface Index
{
    public function getList(int $sortId,int $page);
    public function getDescription(string $ext);
    public function getChapter(string $ext);
    public function getChapterInfo(string $name,string $url);
}
