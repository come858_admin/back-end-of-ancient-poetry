<?php


namespace Modules\Common\Lib\Novel;


class Factory
{
    static private $keyList = [
        "QugeNovel"=>"\Modules\Common\Lib\Novel\QugeNovel"
    ];
    static function getlist(string $key,int $sortId,int $page){
        if(!isset(self::$keyList[$key])){
            return [
              'status'=>0,
              'message'=>'key错误'
            ];
        }
        $newClass = new self::$keyList[$key]();
        return $newClass->getList($sortId,$page);
    }

    static function getDescription(string $key,string $ext){
        if(!isset(self::$keyList[$key])){
            return [
                'status'=>0,
                'message'=>'key错误'
            ];
        }
        $newClass = new self::$keyList[$key]();
        return $newClass->getDescription($ext);
    }
    static function getChapter(string $key,string $ext){
        if(!isset(self::$keyList[$key])){
            return [
                'status'=>0,
                'message'=>'key错误'
            ];
        }
        $newClass = new self::$keyList[$key]();
        return $newClass->getChapter($ext);
    }
    static function getChapterInfo(string $key,string $name,string $url){
        if(!isset(self::$keyList[$key])){
            return [
                'status'=>0,
                'message'=>'key错误'
            ];
        }
        $newClass = new self::$keyList[$key]();
        return $newClass->getChapterInfo($name,$url);
    }
}
