<?php
// +----------------------------------------------------------------------
// | Name: 咪乐多管理系统 [ 为了快速搭建软件应用而生的，希望能够帮助到大家提高开发效率。 ]
// +----------------------------------------------------------------------
// | Copyright: (c) 2020~2022 https://www.lvacms.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed: 这是一个自由软件，允许对程序代码进行修改，但希望您留下原有的注释。
// +----------------------------------------------------------------------
// | Author: 西安咪乐多软件 <997786358@qq.com>
// +----------------------------------------------------------------------
// | Version: V1
// +----------------------------------------------------------------------

/**
 * @Name 七牛云图片上传
 * @Description
 * @Auther 西安咪乐多软件
 * @Date 2022/1/4 04:33
 */

namespace Modules\Common\Lib\Qiniu;


use Modules\Common\Services\BaseService;
use zgldh\QiniuStorage\QiniuStorage;

class QiniuIndex
{
    private static function getPath(){
        return '2iwanli/image/' . date("Ym/d", time()) . '/';
    }

    public static function upload($fileCharater){
        // 初始化
        $disk = QiniuStorage::disk('qiniu');
        // 重命名文件
        $fileName = md5($fileCharater->getClientOriginalName().time().rand()).'.'.$fileCharater->getClientOriginalExtension();
        $path = self::getPath();
        // 上传到七牛
        $bool = $disk->put($path.$fileName,file_get_contents($fileCharater->getRealPath()));
        // 判断是否上传成功
        if($bool){
            $url = $disk->downloadUrl($path.$fileName);
            $httpUrl = BaseService::getHttp(2);
            return str_replace($httpUrl,"",$url);
        }
        return false;
    }

    public static function contentUpload($fileCharater,string $url){
        // 初始化
        $disk = QiniuStorage::disk('qiniu');
        // 重命名文件
        $fileName = md5($url.time().rand()).'.png';
        $path = self::getPath();
        // 上传到七牛
        $bool = $disk->put($path.$fileName,$fileCharater);
        // 判断是否上传成功
        if($bool){
            $url = $disk->downloadUrl($path.$fileName);
            $httpUrl = BaseService::getHttp(2);
            return str_replace($httpUrl,"",$url);
        }
        return false;
    }
}
