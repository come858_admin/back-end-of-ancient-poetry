<?php

namespace Modules\Common\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CommonPageRequest extends FormRequest
{
    /**
     * php artisan module:make-request AdminRequest Admin
     */

    public function authorize()
    {
        return true;
    }
	public function rules()
    {
        return [
			'limit' 	    => 'required|is_positive_integer',
			'page'      	=> 'required|is_positive_integer'
        ];
    }
	public function messages(){
		return [
			'limit.required' 				    => '缺少参数！',
			'limit.is_positive_integer' 		=> '参数错误！',

            'page.required' 				    => '缺少参数！',
            'page.is_positive_integer' 		    => '参数错误！',
		];
	}
}









