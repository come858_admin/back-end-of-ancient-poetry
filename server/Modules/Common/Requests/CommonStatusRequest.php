<?php

namespace Modules\Common\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CommonStatusRequest extends FormRequest
{
    /**
     * php artisan module:make-request AdminRequest Admin
     */

    public function authorize()
    {
        return true;
    }
	public function rules()
    {
        return [
			'status' 	=> 'required|is_status_integer',

        ];
    }
	public function messages(){
		return [
			'status.required' 				=> '缺少参数！',
			'status.is_status_integer' 		=> '参数错误！',
		];
	}
}









