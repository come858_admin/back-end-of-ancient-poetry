<?php

namespace Modules\Admin\Http\Requests\Project;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProjectRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $pathArr = explode('/',$this->path());
        $id = array_pop($pathArr);
        return [
            'name'          => 'required|max:100|unique:admin_projects,name,'.$id,
            "url"           =>'required|max:100',
            "description"   =>'required|max:255',
            "keywords"      =>'required:max:255',
            "status"        =>'required|is_status_integer',
            "type"          =>'required|is_positive_integer'
        ];
    }

    public function messages()
    {
        return [
            'name.required'                 =>'请输入项目名称',
            'name.max'                      =>'项目名称最大长度100个字符',
            'name.unique'                   =>'项目名称已存在',

            'url.required'                  =>'请输入项目地址',
            'url.max'                       =>'项目地址最大长度100个字符',


            'description.required'          =>'请输入项目描述',
            'description.max'               =>'项目描述最大长度255个字符',


            'keywords.required'             =>'请输入项目关键词',
            'keywords.max'                  =>'项目关键词最大长度255个字符',


            'status.required'               =>'请选择状态',
            'status.is_status_integer'      =>'您选择的状态异常',
            'type.required'                 =>'请选择项目类型',
            'type.is_status_integer'        =>'您选择的项目类型异常',
        ];
    }
}
