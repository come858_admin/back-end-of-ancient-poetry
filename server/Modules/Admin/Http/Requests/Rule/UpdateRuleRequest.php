<?php

namespace Modules\Admin\Http\Requests\Rule;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRuleRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'path'              => 'max:100',
            'url'               => 'max:100',
            'redirect'          => 'max:100',
            'name'              => 'required|max:100',
            'type'              => 'required|is_positive_integer',
            'icon'              => 'max:50',
            "affix"             =>'required|is_status_integer',
            "pid"               =>'required|is_pid_integer',
            "sort"              =>'required|is_positive_integer',
            "auth_open"         =>'required|is_status_integer',
            "status"            =>'required|is_status_integer',
        ];
    }

    public function messages()
    {
        return [
            'path.max'                      =>'标识最大长度100个字符',

            'url.max'                       =>'路由文件最大长度100个字符',

            'redirect.max'                  =>'重定向路径最大长度100个字符',

            'name.required'                 =>'请输入菜单名称',
            'name.max'                      =>'菜单名称最大长度100个字符',

            'type.required'                 =>'请选择菜单类型',
            'type.is_positive_integer'      =>'您选择的菜单类型异常',

            'auth_open.required'            =>'请选择是否验证',
            'auth_open.is_status_integer'   =>'您选择的是否验证状态异常',

            'affix.required'                =>'请选择固定面板',
            'affix.is_status_integer'       =>'您选择的固定面板状态异常',
            'icon.max'                      =>'图标最大长度100个字符',

            'pid.required'                  =>'请选择父级菜单',
            'pid.is_pid_integer'            =>'您选择的父级菜单异常',

            'sort.required'                 =>'请输入排序',
            'sort.is_positive_integer'      =>'排序只允许为大于0的数字',
            'status.required'               =>'请选择状态',
            'status.is_status_integer'      =>'您选择的状态异常',
        ];
    }
}
