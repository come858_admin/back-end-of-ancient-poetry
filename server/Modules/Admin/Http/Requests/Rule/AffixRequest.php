<?php

namespace Modules\Admin\Http\Requests\Rule;

use Illuminate\Foundation\Http\FormRequest;

class AffixRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "affix"         =>'required|is_status_integer'
        ];
    }

    public function messages()
    {
        return [
            'affix.required'            =>'缺少参数',
            'affix.is_status_integer'   =>'参数错误',
        ];
    }
}
