<?php

namespace Modules\Admin\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class AddAdminRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'          => 'required|max:30',
            'group_id'      => 'required|is_positive_integer',
            'project_id'    => 'required|is_array_integer',
            'username'      => 'required|unique:admin_admins|regex:/^[a-zA-Z0-9]{4,14}$/',
            'password'      => 'required|confirmed|regex:/^[a-zA-Z0-9]{4,14}$/',
            'phone'         => 'required|unique:admin_admins|regex:/^1[34578]\d{9}$/',
            "status"        => 'required|is_status_integer',
        ];
    }

    public function messages()
    {
        return [
            'name.required'                 =>'请输入姓名',
            'name.max'                      =>'姓名最大长度30个字符',
            'group_id.required'             =>'请选择角色',
            'group_id.is_positive_integer'  =>'请选择角色',

            'project_id.required'           =>'请选择项目',
            'project_id.is_array_integer'   =>'请选择项目',

            'username.required'             =>'请输入账号',
            'username.unique'               =>'账号已存在',
            'username.regex'                =>'账号必须4到14位的数字或字母',

            'phone.required'                =>'请输入手机号',
            'phone.unique'                  =>'手机号已存在',
            'phone.regex'                   =>'请输入正确的手机号',

            'password.required'             =>'请输入密码',
            'password.confirmed'            =>'密码和确认密码输入不一致',
            'password.regex'                =>'密码必须4到14位的数字或字母',

            'status.required'               =>'请选择状态',
            'status.is_status_integer'      =>'您选择的状态异常',
        ];
    }
}
