<?php
// +----------------------------------------------------------------------
// | Name: 咪乐多管理系统 [ 为了快速搭建软件应用而生的，希望能够帮助到大家提高开发效率。 ]
// +----------------------------------------------------------------------
// | Copyright: (c) 2020~2021 https://www.lvacms.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed: 这是一个自由软件，允许对程序代码进行修改，但希望您留下原有的注释。
// +----------------------------------------------------------------------
// | Author: 西安咪乐多软件 <997786358@qq.com>
// +----------------------------------------------------------------------
// | Version: V1
// +----------------------------------------------------------------------

/**
 * @Name  项目管理
 * @Description
 * @Auther 西安咪乐多软件
 * @Date 2021/11/20 17:43
 */

namespace Modules\Admin\Http\Controllers\v1;


use Modules\Admin\Http\Controllers\BaseApiController;
use Modules\Admin\Http\Requests\Project\AddProjectRequest;
use Modules\Admin\Http\Requests\Project\UpdateProjectRequest;
use Modules\Admin\Services\Project\ProjectServices;
use Modules\Common\Requests\CommonPageRequest;
use Modules\Common\Requests\CommonStatusRequest;

class ProjectController extends BaseApiController
{
    /**
     * @OA\Get(
     *     path="/admin/project/index",
     *     operationId="admin-project-index",
     *     tags={"Project"},
     *     summary="项目列表",
     *     description="项目列表。",
     *     security={{ "Authorization-Bearer":{},"Authorization-key":{} }},
     *     @OA\Parameter(ref="#/components/parameters/page"),
     *     @OA\Parameter(ref="#/components/parameters/limit"),
     *     @OA\Parameter(ref="#/components/parameters/status"),
     *     @OA\Parameter(ref="#/components/parameters/created_at[0]"),
     *     @OA\Parameter(ref="#/components/parameters/created_at[1]"),
     *     @OA\Parameter(ref="#/components/parameters/updated_at[0]"),
     *     @OA\Parameter(ref="#/components/parameters/updated_at[1]"),
     *     @OA\Parameter(
     *          name="name",
     *          in="query",
     *          description="项目名称",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Response(
     *          response=200,
     *          description="请求成功",
     *          @OA\JsonContent(type="object",description="成功返回信息",required={"status","message","data"},
     *              @OA\Property(property="status", type="integer",format="20000",description="自定义状态码，20000表示成功"),
     *              @OA\Property(property="message", type="string",description="提示信息"),
     *              @OA\Property(property="data", type="object",description="成功返回具体的列表数据",required={"total","list"},
     *                  @OA\Property(property="total",type="integer",description="数据总条数"),
     *                  @OA\Property(property="list", type="array",description="项目列表信息",
     *                       @OA\Items(type="object",description="单条项目信息",required={"id","name","logo_id","ico_id","url","description","keywords","status","created_at","updated_at","logo_image","ico_image"},
     *                          @OA\Property(property="id", type="integer",description="项目ID"),
     *                          @OA\Property(property="name", type="string",description="项目名称"),
     *                          @OA\Property(property="logo_id", type="integer",description="站点logoID"),
     *                          @OA\Property(property="ico_id", type="integer",description="站点标识ID"),
     *                          @OA\Property(property="url", type="string",description="项目地址"),
     *                          @OA\Property(property="description", type="string",description="项目描述"),
     *                          @OA\Property(property="keywords", type="string",description="项目关键词"),
     *                          @OA\Property(property="status", type="integer",description="状态:0=禁用,1=启用"),
     *                          @OA\Property(property="created_at", type="date",description="创建时间(Y-m-d H:i:s)"),
     *                          @OA\Property(property="updated_at", type="date",description="更新时间(Y-m-d H:i:s 或 null )"),
     *                          @OA\Property(property="logo_image", type="object",description="站点logo数据",required={"id","url","open","http_url"},
     *                              @OA\Property(property="id", type="integer",description="图片id"),
     *                              @OA\Property(property="url", type="string",description="图片初始路径"),
     *                              @OA\Property(property="open", type="integer",description="图片类型:1=本地,2=七牛云"),
     *                              @OA\Property(property="http_url", type="string",description="图片完整路径"),
     *                          ),
     *                          @OA\Property(property="ico_image", type="object",description="站点logo数据",required={"id","url","open","http_url"},
     *                              @OA\Property(property="id", type="integer",description="图片id"),
     *                              @OA\Property(property="url", type="string",description="图片初始路径"),
     *                              @OA\Property(property="open", type="integer",description="图片类型:1=本地,2=七牛云"),
     *                              @OA\Property(property="http_url", type="string",description="图片完整路径"),
     *                          )
     *                       )
     *                  )
     *              ),
     *              example={"status": 20000,"message": "操作成功！","data": {"list": {{"id": 1,"name": "测试项目","logo_id": 1,"ico_id": 1,"url": "http://www.lvacms.cn","description": "项目描述","keywords": "项目关键词","status": 1,"created_at": "2021-11-27 23:40:07","updated_at": null,"logo_image": {"id": 1,"url": "/storage/20211127/EYm2rEFDhGbu1TKLJ8L95KYY3yXwJY18tZEjBNo5.png","open": 1,"http_url": "http://www.lvuni.com/storage/20211127/EYm2rEFDhGbu1TKLJ8L95KYY3yXwJY18tZEjBNo5.png"},"ico_image": {"id": 1,"url": "/storage/20211127/EYm2rEFDhGbu1TKLJ8L95KYY3yXwJY18tZEjBNo5.png","open": 1,"http_url": "http://www.lvuni.com/storage/20211127/EYm2rEFDhGbu1TKLJ8L95KYY3yXwJY18tZEjBNo5.png"}}},"total": 1}}
     *          )
     *       )
     * )
     * @param CommonPageRequest $request
     * @return \Modules\Common\Services\JSON
     */
    public function index(CommonPageRequest $request){
        return (new ProjectServices())->index($request->only([
            "limit",
            "name",
            "created_at",
            "updated_at",
            "status",
            "sort",
            "type"
        ]));
    }


    /**
     * @OA\Post(
     *     path="/admin/project/add",
     *     operationId="admin-project-add",
     *     tags={"Project"},
     *     summary="添加项目",
     *     description="添加项目。",
     *     security={{ "Authorization-Bearer":{},"Authorization-key":{} }},
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             mediaType="application/x-www-form-urlencoded",
     *             @OA\Schema(
     *                 required={"name","logo_id","ico_id","url","description","keywords","status"},
     *                 @OA\Property(
     *                      property="name",
     *                      type="string",
     *                      description="项目名称",
     *                      default="测试项目"
     *                  ),
     *                  @OA\Property(
     *                      property="logo_id",
     *                      type="integer",
     *                      description="站点logo",
     *                      default=1
     *                  ),
     *                  @OA\Property(
     *                      property="ico_id",
     *                      type="integer",
     *                      description="站点标识",
     *                      default=1
     *                  ),
     *                  @OA\Property(
     *                      property="url",
     *                      type="string",
     *                      description="项目地址",
     *                      default="http://www.lvacms.cn"
     *                  ),
     *                  @OA\Property(
     *                      property="description",
     *                      type="string",
     *                      description="项目描述",
     *                      default="项目描述"
     *                  ),
     *                  @OA\Property(
     *                      property="keywords",
     *                      type="string",
     *                      description="项目关键词",
     *                      default="项目关键词"
     *                  ),
     *                  @OA\Property(
     *                      property="status",
     *                      type="integer",
     *                      description="状态:0=禁用,1=启用",
     *                      default=1
     *                  )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *          response=200,
     *          description="请求成功",
     *          @OA\JsonContent(ref="#/components/schemas/success")
     *       )
     * )
     * @param AddProjectRequest $request
     * @return \Modules\Common\Services\JSON
     */
    public function add(AddProjectRequest $request)
    {
        return (new ProjectServices())->add($request->only([
            "name",
            "logo_id",
            "ico_id",
            "url",
            "description",
            "keywords",
            "status",
            "type"
        ]));
    }

    /**
     * @OA\Get(
     *     path="/admin/project/edit/{id}",
     *     operationId="admin-project-edit",
     *     tags={"Project"},
     *     summary="编辑项目页面",
     *     description="获取编辑项目页面信息。",
     *     security={{ "Authorization-Bearer":{},"Authorization-key":{} }},
     *     @OA\Parameter(ref="#/components/parameters/id"),
     *     @OA\Response(
     *          response=200,
     *          description="请求成功",
     *          @OA\JsonContent(type="object",description="成功返回信息",required={"status","message","data"},
     *              @OA\Property(property="status", type="integer",format="20000",description="自定义状态码，20000表示成功"),
     *              @OA\Property(property="message", type="string",description="提示信息"),
     *              @OA\Property(property="data", type="object",description="成功返回具体的项目数据",required={"id","name","logo_id","ico_id","url","description","keywords","status","logo_image","ico_image"},
     *                  @OA\Property(property="id", type="integer",description="项目ID"),
     *                  @OA\Property(property="name", type="string",description="项目名称"),
     *                  @OA\Property(property="logo_id", type="integer",description="站点logoID"),
     *                  @OA\Property(property="ico_id", type="integer",description="站点标识ID"),
     *                  @OA\Property(property="url", type="string",description="项目地址"),
     *                  @OA\Property(property="description", type="string",description="项目描述"),
     *                  @OA\Property(property="keywords", type="string",description="项目关键词"),
     *                  @OA\Property(property="status", type="integer",description="状态:0=禁用,1=启用"),
     *                  @OA\Property(property="logo_image", type="object",description="站点logo数据",required={"id","url","open","http_url"},
     *                        @OA\Property(property="id", type="integer",description="图片id"),
     *                        @OA\Property(property="url", type="string",description="图片初始路径"),
     *                        @OA\Property(property="open", type="integer",description="图片类型:1=本地,2=七牛云"),
     *                        @OA\Property(property="http_url", type="string",description="图片完整路径"),
     *                   ),
     *                   @OA\Property(property="ico_image", type="object",description="站点logo数据",required={"id","url","open","http_url"},
     *                         @OA\Property(property="id", type="integer",description="图片id"),
     *                         @OA\Property(property="url", type="string",description="图片初始路径"),
     *                         @OA\Property(property="open", type="integer",description="图片类型:1=本地,2=七牛云"),
     *                         @OA\Property(property="http_url", type="string",description="图片完整路径"),
     *                   )
     *              ),
     *              example={"status": 20000,"message": "获取成功！","data": {"id": 1,"name": "测试项目","logo_id": 1,"ico_id": 1,"url": "http://www.lvacms.cn","description": "项目描述","keywords": "项目关键词","status": 1,"logo_image": {"id": 1,"url": "/storage/20211127/EYm2rEFDhGbu1TKLJ8L95KYY3yXwJY18tZEjBNo5.png","open": 1,"http_url": "http://www.lvuni.com/storage/20211127/EYm2rEFDhGbu1TKLJ8L95KYY3yXwJY18tZEjBNo5.png"},"ico_image": {"id": 1,"url": "/storage/20211127/EYm2rEFDhGbu1TKLJ8L95KYY3yXwJY18tZEjBNo5.png","open": 1,"http_url": "http://www.lvuni.com/storage/20211127/EYm2rEFDhGbu1TKLJ8L95KYY3yXwJY18tZEjBNo5.png"}}}
     *          )
     *       )
     * )
     * @param int $id
     * @return \Modules\Common\Services\JSON
     */
    public function edit(int $id){
        return (new ProjectServices())->edit($id);
    }

    /**
     * @OA\Put(
     *     path="/admin/project/update/{id}",
     *     operationId="admin-project-update",
     *     tags={"Project"},
     *     summary="编辑项目",
     *     description="编辑项目提交。",
     *     security={{ "Authorization-Bearer":{},"Authorization-key":{} }},
     *     @OA\Parameter(ref="#/components/parameters/id"),
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             mediaType="application/x-www-form-urlencoded",
     *             @OA\Schema(
     *                 required={"name","logo_id","ico_id","url","description","keywords","status"},
     *                 @OA\Property(
     *                      property="name",
     *                      type="string",
     *                      description="项目名称",
     *                      default="测试项目"
     *                  ),
     *                  @OA\Property(
     *                      property="logo_id",
     *                      type="integer",
     *                      description="站点logo",
     *                      default=1
     *                  ),
     *                  @OA\Property(
     *                      property="ico_id",
     *                      type="integer",
     *                      description="站点标识",
     *                      default=1
     *                  ),
     *                  @OA\Property(
     *                      property="url",
     *                      type="string",
     *                      description="项目地址",
     *                      default="http://www.lvacms.cn"
     *                  ),
     *                  @OA\Property(
     *                      property="description",
     *                      type="string",
     *                      description="项目描述",
     *                      default="项目描述"
     *                  ),
     *                  @OA\Property(
     *                      property="keywords",
     *                      type="string",
     *                      description="项目关键词",
     *                      default="项目关键词"
     *                  ),
     *                  @OA\Property(
     *                      property="status",
     *                      type="integer",
     *                      description="状态:0=禁用,1=启用",
     *                      default=1
     *                  )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *          response=200,
     *          description="请求成功",
     *          @OA\JsonContent(ref="#/components/schemas/success")
     *       )
     * )
     * @param UpdateProjectRequest $request
     * @param int $id
     * @return \Modules\Common\Services\JSON
     */
    public function update(UpdateProjectRequest $request,int $id)
    {
        return (new ProjectServices())->update($id,$request->only([
            "name",
            "logo_id",
            "ico_id",
            "url",
            "description",
            "keywords",
            "status",
            "type"
        ]));
    }

    /**
     * @OA\Put(
     *     path="/admin/project/status/{id}",
     *     operationId="admin-project-status",
     *     tags={"Project"},
     *     summary="项目调整状态",
     *     description="项目调整状态。",
     *     security={{ "Authorization-Bearer":{},"Authorization-key":{} }},
     *     @OA\Parameter(ref="#/components/parameters/id"),
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             mediaType="application/x-www-form-urlencoded",
     *             @OA\Schema(ref="#/components/schemas/status")
     *         )
     *     ),
     *     @OA\Response(
     *          response=200,
     *          description="请求成功",
     *          @OA\JsonContent(ref="#/components/schemas/success")
     *       )
     * )
     * @param CommonStatusRequest $request
     * @param int $id
     * @return \Modules\Common\Services\JSON
     */
    public function status(CommonStatusRequest $request,int $id){
        return (new ProjectServices())->status($id,$request->only([
            "status"
        ]));
    }

    /**
     * @OA\Delete(
     *     path="/admin/project/del/{id}",
     *     operationId="admin-project-del",
     *     tags={"Project"},
     *     summary="项目删除",
     *     description="项目删除。",
     *     security={{ "Authorization-Bearer":{},"Authorization-key":{} }},
     *     @OA\Parameter(ref="#/components/parameters/id"),
     *     @OA\Response(
     *          response=200,
     *          description="请求成功",
     *          @OA\JsonContent(ref="#/components/schemas/success")
     *       )
     * )
     * @param int $id
     * @return \Modules\Common\Services\JSON
     */
    public function del(int $id){
        return (new ProjectServices())->del($id);
    }
}
