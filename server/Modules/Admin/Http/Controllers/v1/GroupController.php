<?php
// +----------------------------------------------------------------------
// | Name: 咪乐多管理系统 [ 为了快速搭建软件应用而生的，希望能够帮助到大家提高开发效率。 ]
// +----------------------------------------------------------------------
// | Copyright: (c) 2020~2021 https://www.lvacms.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed: 这是一个自由软件，允许对程序代码进行修改，但希望您留下原有的注释。
// +----------------------------------------------------------------------
// | Author: 西安咪乐多软件 <997786358@qq.com>
// +----------------------------------------------------------------------
// | Version: V1
// +----------------------------------------------------------------------

/**
 * @Name 角色管理
 * @Description
 * @Auther 西安咪乐多软件
 * @Date 2021/11/20 17:43
 */

namespace Modules\Admin\Http\Controllers\v1;


use Modules\Admin\Http\Controllers\BaseApiController;
use Modules\Admin\Http\Requests\Group\AddGroupRequest;
use Modules\Admin\Http\Requests\Group\UpdateGroupRequest;
use Modules\Admin\Http\Requests\Group\AccessUpdateGroupRequest;
use Modules\Admin\Services\Group\GroupServices;
use Modules\Admin\Services\Rule\RuleServices;
use Modules\Common\Requests\CommonPageRequest;
use Modules\Common\Requests\CommonStatusRequest;

class GroupController extends BaseApiController
{
    /**
     * @OA\Get(
     *     path="/admin/group/index",
     *     operationId="admin-group-index",
     *     tags={"Group"},
     *     summary="角色列表",
     *     description="角色列表。",
     *     security={{ "Authorization-Bearer":{},"Authorization-key":{} }},
     *     @OA\Parameter(ref="#/components/parameters/page"),
     *     @OA\Parameter(ref="#/components/parameters/limit"),
     *     @OA\Parameter(ref="#/components/parameters/status"),
     *     @OA\Parameter(ref="#/components/parameters/created_at[0]"),
     *     @OA\Parameter(ref="#/components/parameters/created_at[1]"),
     *     @OA\Parameter(ref="#/components/parameters/updated_at[0]"),
     *     @OA\Parameter(ref="#/components/parameters/updated_at[1]"),
     *     @OA\Parameter(
     *          name="name",
     *          in="query",
     *          description="角色名称",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Response(
     *          response=200,
     *          description="请求成功",
     *          @OA\JsonContent(type="object",description="成功返回信息",required={"status","message","data"},
     *              @OA\Property(property="status", type="integer",format="20000",description="自定义状态码，20000表示成功"),
     *              @OA\Property(property="message", type="string",description="提示信息"),
     *              @OA\Property(property="data", type="object",description="成功返回具体的列表数据",required={"total","list"},
     *                  @OA\Property(property="total",type="integer",description="数据总条数"),
     *                  @OA\Property(property="list", type="array",description="角色列表信息",
     *                       @OA\Items(type="object",description="单条角色信息",required={"id","name","content","status","created_at","updated_at"},
     *                          @OA\Property(property="id", type="integer",description="角色ID"),
     *                          @OA\Property(property="name", type="string",description="角色名称"),
     *                          @OA\Property(property="content", type="string",description="角色描述"),
     *                          @OA\Property(property="status", type="integer",description="状态:0=禁用,1=启用"),
     *                          @OA\Property(property="created_at", type="date",description="创建时间(Y-m-d H:i:s)"),
     *                          @OA\Property(property="updated_at", type="date",description="更新时间(Y-m-d H:i:s 或 null )")
     *                       )
     *                  )
     *              ),
     *              example={"status": 20000,"message": "操作成功！","data": {"list": {{"id": 1,"name": "超级管理员","content": "角色描述","status": 1,"rules": null,"created_at": "2021-11-28 00:40:13","updated_at": null}},"total": 1}}
     *          )
     *       )
     * )
     * @param CommonPageRequest $request
     * @return \Modules\Common\Services\JSON
     */
    public function index(CommonPageRequest $request){
        return (new GroupServices())->index($request->only([
            "limit",
            "name",
            "created_at",
            "updated_at",
            "status",
            "sort"
        ]));
    }


    /**
     * @OA\Post(
     *     path="/admin/group/add",
     *     operationId="admin-group-add",
     *     tags={"Group"},
     *     summary="添加角色",
     *     description="添加角色。",
     *     security={{ "Authorization-Bearer":{},"Authorization-key":{} }},
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             mediaType="application/x-www-form-urlencoded",
     *             @OA\Schema(
     *                 required={"name","content","status"},
     *                 @OA\Property(
     *                      property="name",
     *                      type="string",
     *                      description="角色名称",
     *                      default="超级管理员"
     *                  ),
     *                  @OA\Property(
     *                      property="content",
     *                      type="string",
     *                      description="角色描述",
     *                      default="角色描述"
     *                  ),
     *                  @OA\Property(
     *                      property="status",
     *                      type="integer",
     *                      description="状态:0=禁用,1=启用",
     *                      default=1
     *                  )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *          response=200,
     *          description="请求成功",
     *          @OA\JsonContent(ref="#/components/schemas/success")
     *       )
     * )
     * @param AddGroupRequest $request
     * @return \Modules\Common\Services\JSON
     */
    public function add(AddGroupRequest $request)
    {
        return (new GroupServices())->add($request->only([
            "name",
            "content",
            "status",
        ]));
    }

    /**
     * @OA\Get(
     *     path="/admin/group/edit/{id}",
     *     operationId="admin-group-edit",
     *     tags={"Group"},
     *     summary="编辑角色页面",
     *     description="获取编辑角色页面信息。",
     *     security={{ "Authorization-Bearer":{},"Authorization-key":{} }},
     *     @OA\Parameter(ref="#/components/parameters/id"),
     *     @OA\Response(
     *          response=200,
     *          description="请求成功",
     *          @OA\JsonContent(type="object",description="成功返回信息",required={"status","message","data"},
     *              @OA\Property(property="status", type="integer",format="20000",description="自定义状态码，20000表示成功"),
     *              @OA\Property(property="message", type="string",description="提示信息"),
     *              @OA\Property(property="data", type="object",description="成功返回具体的角色数据",required={"id","name","content","status"},
     *                   @OA\Property(property="id", type="integer",description="角色ID"),
     *                   @OA\Property(property="name", type="string",description="角色名称"),
     *                   @OA\Property(property="content", type="string",description="角色描述"),
     *                   @OA\Property(property="status", type="integer",description="状态:0=禁用,1=启用"),
     *              ),
     *              example={"status": 20000,"message": "获取成功！","data": {"id": 1,"name": "超级管理员","content": "角色描述","status": 1}}
     *          )
     *       )
     * )
     * @param int $id
     * @return \Modules\Common\Services\JSON
     */
    public function edit(int $id){
        return (new GroupServices())->edit($id);
    }

    /**
     * @OA\Put(
     *     path="/admin/group/update/{id}",
     *     operationId="admin-group-update",
     *     tags={"Group"},
     *     summary="编辑角色提交",
     *     description="编辑角色提交。",
     *     security={{ "Authorization-Bearer":{},"Authorization-key":{} }},
     *     @OA\Parameter(ref="#/components/parameters/id"),
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             mediaType="application/x-www-form-urlencoded",
     *             @OA\Schema(
     *                 required={"name","content","status"},
     *                 @OA\Property(
     *                      property="name",
     *                      type="string",
     *                      description="角色名称",
     *                      default="超级管理员"
     *                  ),
     *                  @OA\Property(
     *                      property="content",
     *                      type="string",
     *                      description="角色描述",
     *                      default="角色描述"
     *                  ),
     *                  @OA\Property(
     *                      property="status",
     *                      type="integer",
     *                      description="状态:0=禁用,1=启用",
     *                      default=1
     *                  )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *          response=200,
     *          description="请求成功",
     *          @OA\JsonContent(ref="#/components/schemas/success")
     *       )
     * )
     * @param UpdateGroupRequest $request
     * @param int $id
     * @return \Modules\Common\Services\JSON
     */
    public function update(UpdateGroupRequest $request,int $id)
    {
        return (new GroupServices())->update($id,$request->only([
            "name",
            "content",
            "status",
        ]));
    }

    /**
     * @OA\Get(
     *     path="/admin/group/access/{id}",
     *     operationId="admin-group-access",
     *     tags={"Group"},
     *     summary="权限规则页面",
     *     description="获取权限规则页面信息。",
     *     security={{ "Authorization-Bearer":{},"Authorization-key":{} }},
     *     @OA\Parameter(ref="#/components/parameters/id"),
     *     @OA\Response(
     *          response=200,
     *          description="请求成功",
     *          @OA\JsonContent(type="object",description="成功返回信息",required={"status","message","data"},
     *              @OA\Property(property="status", type="integer",format="20000",description="自定义状态码，20000表示成功"),
     *              @OA\Property(property="message", type="string",description="提示信息"),
     *              @OA\Property(property="data", type="object",description="菜单列表和当前角色的菜单权限数据",
     *                  @OA\Property(property="list", type="array",description="菜单列表",
     *                      @OA\Items(type="object",description="单条角色信息",required={"id","name","pid"},
     *                          @OA\Property(property="id", type="integer",description="菜单ID"),
     *                          @OA\Property(property="name", type="string",description="菜单名称"),
     *                          @OA\Property(property="pid", type="integer",description="父级ID"),
     *                          @OA\Property(property="children", type="array",description="下级数据",
     *                              @OA\items()
     *                          )
     *                      )
     *                  ),
     *                  @OA\Property(property="rules", type="array",description="当前角色的菜单权限",
     *                      @OA\items(type="integer",description="菜单ID")
     *                  )
     *              ),
     *              example={"status": 20000,"message": "操作成功！","data": {"list": {{"id": 3,"name": "系统管理","pid": 0,"children": {{"id": 5,"name": "系统管理","pid": 3}}}},"rules": {"1","2"}}}
     *          )
     *       )
     * )
     * @param int $id
     * @return \Modules\Common\Services\JSON
     */
    public function access(int $id){
        return (new RuleServices())->access($id);
    }
    /**
     * @OA\Put(
     *     path="/admin/group/accessUpdate/{id}",
     *     operationId="admin-group-accessUpdate",
     *     tags={"Group"},
     *     summary="权限规则提交",
     *     description="权限规则提交。",
     *     security={{ "Authorization-Bearer":{},"Authorization-key":{} }},
     *     @OA\Parameter(ref="#/components/parameters/id"),
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             mediaType="application/x-www-form-urlencoded",
     *             @OA\Schema(
     *                  required={"rules"},
     *                  @OA\Property(
     *                      property="rules",
     *                      type="array",
     *                      description="菜单多个ID组成的数组",
     *                      @OA\items(type="integer")
     *                  )
     *              )
     *         )
     *     ),
     *     @OA\Response(
     *          response=200,
     *          description="请求成功",
     *          @OA\JsonContent(ref="#/components/schemas/success")
     *       )
     * )
     * @param CommonStatusRequest $request
     * @param int $id
     * @return \Modules\Common\Services\JSON
     */
    public function accessUpdate($id,AccessUpdateGroupRequest $request){
        return (new GroupServices())->accessUpdate($id,$request->only([
            "rules"
        ]));
    }

    /**
     * @OA\Put(
     *     path="/admin/group/status/{id}",
     *     operationId="admin-group-status",
     *     tags={"Group"},
     *     summary="角色调整状态",
     *     description="角色调整状态。",
     *     security={{ "Authorization-Bearer":{},"Authorization-key":{} }},
     *     @OA\Parameter(ref="#/components/parameters/id"),
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             mediaType="application/x-www-form-urlencoded",
     *             @OA\Schema(ref="#/components/schemas/status")
     *         )
     *     ),
     *     @OA\Response(
     *          response=200,
     *          description="请求成功",
     *          @OA\JsonContent(ref="#/components/schemas/success")
     *       )
     * )
     * @param CommonStatusRequest $request
     * @param int $id
     * @return \Modules\Common\Services\JSON
     */
    public function status(CommonStatusRequest $request,int $id){
        return (new GroupServices())->status($id,$request->only([
            "status"
        ]));
    }

    /**
     * @OA\Delete(
     *     path="/admin/group/del/{id}",
     *     operationId="admin-group-del",
     *     tags={"Group"},
     *     summary="角色删除",
     *     description="角色删除。",
     *     security={{ "Authorization-Bearer":{},"Authorization-key":{} }},
     *     @OA\Parameter(ref="#/components/parameters/id"),
     *     @OA\Response(
     *          response=200,
     *          description="请求成功",
     *          @OA\JsonContent(ref="#/components/schemas/success")
     *       )
     * )
     * @param int $id
     * @return \Modules\Common\Services\JSON
     */
    public function del(int $id){
        return (new GroupServices())->del($id);
    }
}
