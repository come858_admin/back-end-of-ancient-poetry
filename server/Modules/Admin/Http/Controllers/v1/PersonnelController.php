<?php
// +----------------------------------------------------------------------
// | Name: 咪乐多管理系统 [ 为了快速搭建软件应用而生的，希望能够帮助到大家提高开发效率。 ]
// +----------------------------------------------------------------------
// | Copyright: (c) 2020~2021 https://www.lvacms.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed: 这是一个自由软件，允许对程序代码进行修改，但希望您留下原有的注释。
// +----------------------------------------------------------------------
// | Author: 西安咪乐多软件 <997786358@qq.com>
// +----------------------------------------------------------------------
// | Version: V1
// +----------------------------------------------------------------------



namespace Modules\Admin\Http\Controllers\v1;


use Illuminate\Http\Request;
use Modules\Admin\Services\Personnel\PersonnelServices;
use Modules\Common\Requests\CommonIdArrRequest;
use Modules\Common\Requests\CommonPageRequest;
use Modules\Common\Requests\CommonStatusRequest;
use Modules\Admin\Http\Controllers\BaseApiController;

class PersonnelController extends BaseApiController
{
    public function index(CommonPageRequest $request)
    {
        return (new PersonnelServices())->index($request->only([
            "limit",
            "name",
            "department_id",
            "sex",
            "nation",
            "date_of_birth",
            "education",
            "marital_status",
            "position",
            "political_outlook",
            "personnel_type",
            "entry_time",
            "status",
            "created_at",
            "updated_at",
            "sort"
        ]));
    }
    public function add(Request $request)
    {
        return (new PersonnelServices())->add($request->only([
            "name",
            "department_id",
            "sex",
            "nation",
            "date_of_birth",
            "education",
            "marital_status",
            "position",
            "political_outlook",
            "personnel_type",
            "entry_time",
            "status",
        ]));
    }
    public function edit(int $id)
    {
        return (new PersonnelServices())->edit($id);
    }
    public function update(Request $request,int $id)
    {
        return (new PersonnelServices())->update($id,$request->only([
            "name",
            "department_id",
            "sex",
            "nation",
            "date_of_birth",
            "education",
            "marital_status",
            "position",
            "political_outlook",
            "personnel_type",
            "entry_time",
            "status",
        ]));
    }
    public function status(CommonStatusRequest $request,int $id){
        return (new PersonnelServices())->status($id,$request->only([
            "status"
        ]));
    }
    public function del(int $id){
        return (new PersonnelServices())->del($id);
    }
    public function delAll(CommonIdArrRequest $request)
    {
        return (new PersonnelServices())->delAll($request->get('idArr'));
    }
    public function getPersonnelList(){
        return (new PersonnelServices())->getPersonnelList();
    }
}
