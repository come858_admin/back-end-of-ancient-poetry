<?php
// +----------------------------------------------------------------------
// | Name: 咪乐多管理系统 [ 为了快速搭建软件应用而生的，希望能够帮助到大家提高开发效率。 ]
// +----------------------------------------------------------------------
// | Copyright: (c) 2020~2021 https://www.lvacms.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed: 这是一个自由软件，允许对程序代码进行修改，但希望您留下原有的注释。
// +----------------------------------------------------------------------
// | Author: 西安咪乐多软件 <997786358@qq.com>
// +----------------------------------------------------------------------
// | Version: V1
// +----------------------------------------------------------------------

/**
 * @Name 菜单管理
 * @Description
 * @Auther 西安咪乐多软件
 * @Date 2021/11/20 17:43
 */

namespace Modules\Admin\Http\Controllers\v1;



use Illuminate\Http\Request;
use Modules\Admin\Http\Controllers\BaseApiController;
use Modules\Admin\Http\Requests\Rule\AddRuleRequest;
use Modules\Admin\Http\Requests\Rule\AffixRequest;
use Modules\Admin\Http\Requests\Rule\AuthOpenRequest;
use Modules\Admin\Http\Requests\Rule\UpdateRuleRequest;
use Modules\Admin\Services\Rule\RuleServices;
use Modules\Common\Requests\CommonSortRequest;
use Modules\Common\Requests\CommonStatusRequest;

class RuleController extends BaseApiController
{

    /**
     * @OA\Get(
     *     path="/admin/rule/index",
     *     operationId="admin-rule-index",
     *     tags={"Rule"},
     *     summary="菜单列表",
     *     description="菜单列表。",
     *     security={{ "Authorization-Bearer":{},"Authorization-key":{} }},
     *     @OA\Response(
     *          response=200,
     *          description="请求成功",
     *          @OA\JsonContent(type="object",description="成功返回信息",required={"status","message","data"},
     *              @OA\Property(property="status", type="integer",format="20000",description="自定义状态码，20000表示成功"),
     *              @OA\Property(property="message", type="string",description="提示信息"),
     *              @OA\Property(property="data", type="array",description="成功返回具体的菜单列表数据",
     *                  @OA\Items(type="object",description="单条角色信息",required={"id","path","url","redirect","name","type","status","auth_open","level","affix","icon","pid","sort","created_at","updated_at"},
     *                     @OA\Property(property="id", type="integer",description="菜单ID"),
     *                     @OA\Property(property="path", type="string",description="标识"),
     *                     @OA\Property(property="url", type="string",description="路由文件"),
     *                     @OA\Property(property="redirect", type="string",description="重定向路径"),
     *                     @OA\Property(property="name", type="string",description="菜单名称"),
     *                     @OA\Property(property="icon", type="string",description="图标"),
     *                     @OA\Property(property="type", type="integer",description="菜单类型:1=模块,2=目录,3=菜单"),
     *                     @OA\Property(property="status", type="integer",description="状态:0=禁用,1=启用"),
     *                     @OA\Property(property="auth_open", type="integer",description="是否验证权限:0=否,1=是"),
     *                     @OA\Property(property="level", type="integer",description="级别"),
     *                     @OA\Property(property="affix", type="integer",description="是否固定面板:0=否,1=是"),
     *                     @OA\Property(property="pid", type="integer",description="父级ID"),
     *                     @OA\Property(property="sort", type="integer",description="排序"),
     *                     @OA\Property(property="created_at", type="date",description="创建时间(Y-m-d H:i:s)"),
     *                     @OA\Property(property="updated_at", type="date",description="更新时间(Y-m-d H:i:s 或 null )"),
     *                     @OA\Property(property="children", type="array",description="下级数据",
     *                          @OA\items()
     *                      )
     *                  )
     *              ),
     *              example={"status": 20000,"message": "操作成功！","data": {{"id": 3,"path": "/admin","url": "/admin/admin/index","redirect": "/admin/admin/index","name": "系统管理","type": 1,"status": 1,"auth_open": 1,"level": 1,"affix": 1,"icon": "fa fa-home","pid": 0,"sort": 50,"created_at": "2021-11-28 01:18:52","updated_at": null,"children": {{"id": 4,"path": "/admin","url": "/admin/admin/index","redirect": "/admin/admin/index","name": "系统管理","type": 1,"status": 1,"auth_open": 1,"level": 1,"affix": 1,"icon": "fa fa-home","pid": 3,"sort": 50,"created_at": "2021-11-28 01:18:59","updated_at": null}}}}}
     *          )
     *       )
     * )
     * @param Request $request
     * @return \Modules\Common\Services\JSON
     */
    public function index(Request $request){
        return (new RuleServices())->index($request->only(['sort']));
    }


    /**
     * @OA\Post(
     *     path="/admin/rule/add",
     *     operationId="admin-rule-add",
     *     tags={"Rule"},
     *     summary="添加菜单",
     *     description="添加菜单。",
     *     security={{ "Authorization-Bearer":{},"Authorization-key":{} }},
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             mediaType="application/x-www-form-urlencoded",
     *             @OA\Schema(
     *                 required={"name","type","status","auth_open","level","affix","pid","sort"},
     *                 @OA\Property(
     *                      property="name",
     *                      type="string",
     *                      description="菜单名称",
     *                      default="系统管理"
     *                  ),
     *                  @OA\Property(
     *                      property="path",
     *                      type="string",
     *                      description="标识",
     *                      default="/admin"
     *                  ),
     *                  @OA\Property(
     *                      property="url",
     *                      type="string",
     *                      description="路由文件",
     *                      default="/admin/admin/index"
     *                  ),
     *                  @OA\Property(
     *                      property="redirect",
     *                      type="string",
     *                      description="重定向路径",
     *                      default="/admin/admin/index"
     *                  ),
     *                  @OA\Property(
     *                      property="type",
     *                      type="integer",
     *                      description="菜单类型:1=模块,2=目录,3=菜单",
     *                      default=1
     *                  ),
     *                  @OA\Property(
     *                      property="icon",
     *                      type="string",
     *                      description="角色描述",
     *                      default="fa fa-home"
     *                  ),
     *                  @OA\Property(
     *                      property="status",
     *                      type="integer",
     *                      description="状态:0=禁用,1=启用",
     *                      default=1
     *                  ),
     *                  @OA\Property(
     *                      property="auth_open",
     *                      type="integer",
     *                      description="是否验证权限:0=否,1=是",
     *                      default=1
     *                  ),
     *                  @OA\Property(
     *                      property="affix",
     *                      type="integer",
     *                      description="是否固定面板:0=否,1=是",
     *                      default=1
     *                  ),
     *                  @OA\Property(
     *                      property="pid",
     *                      type="integer",
     *                      description="父级ID（0为顶级）",
     *                      default=1
     *                  ),
     *                  @OA\Property(
     *                      property="sort",
     *                      type="integer",
     *                      description="排序",
     *                      default=50
     *                  )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *          response=200,
     *          description="请求成功",
     *          @OA\JsonContent(ref="#/components/schemas/success")
     *       )
     * )
     * @param AddRuleRequest $request
     * @return \Modules\Common\Services\JSON
     */
    public function add(AddRuleRequest $request)
    {
        return (new RuleServices())->add($request->only([
            'path',
            'url',
            'redirect',
            'name',
            'type',
            'icon',
            "affix",
            "pid",
            "sort",
            "auth_open",
            "status",
        ]));
    }

    /**
     * @OA\Get(
     *     path="/admin/rule/edit/{id}",
     *     operationId="admin-rule-edit",
     *     tags={"Rule"},
     *     summary="编辑菜单页面",
     *     description="获取编辑菜单页面信息。",
     *     security={{ "Authorization-Bearer":{},"Authorization-key":{} }},
     *     @OA\Parameter(ref="#/components/parameters/id"),
     *     @OA\Response(
     *          response=200,
     *          description="请求成功",
     *          @OA\JsonContent(type="object",description="成功返回信息",required={"status","message","data"},
     *              @OA\Property(property="status", type="integer",format="20000",description="自定义状态码，20000表示成功"),
     *              @OA\Property(property="message", type="string",description="提示信息"),
     *              @OA\Property(property="data", type="object",description="成功返回具体的菜单数据",required={"id","path","url","redirect","name","type","status","auth_open","level","affix","icon","pid","sort"},
     *                     @OA\Property(property="id", type="integer",description="菜单ID"),
     *                     @OA\Property(property="path", type="string",description="标识"),
     *                     @OA\Property(property="url", type="string",description="路由文件"),
     *                     @OA\Property(property="redirect", type="string",description="重定向路径"),
     *                     @OA\Property(property="name", type="string",description="菜单名称"),
     *                     @OA\Property(property="icon", type="string",description="图标"),
     *                     @OA\Property(property="type", type="integer",description="菜单类型:1=模块,2=目录,3=菜单"),
     *                     @OA\Property(property="status", type="integer",description="状态:0=禁用,1=启用"),
     *                     @OA\Property(property="auth_open", type="integer",description="是否验证权限:0=否,1=是"),
     *                     @OA\Property(property="affix", type="integer",description="是否固定面板:0=否,1=是"),
     *                     @OA\Property(property="pid", type="integer",description="父级ID"),
     *                     @OA\Property(property="sort", type="integer",description="排序")
     *              ),
     *              example={"status": 20000,"message": "获取成功！","data": {"id": 3,"path": "/admin","url": "/admin/admin/index","redirect": "/admin/admin/index","name": "系统管理","type": 1,"icon": "fa fa-home","affix": 1,"pid": 0,"sort": 50,"auth_open": 1,"status": 1}}
     *          )
     *       )
     * )
     * @param int $id
     * @return \Modules\Common\Services\JSON
     */
    public function edit(int $id){
        return (new RuleServices())->edit($id);
    }

    /**
     * @OA\Put(
     *     path="/admin/rule/update/{id}",
     *     operationId="admin-rule-update",
     *     tags={"Rule"},
     *     summary="编辑菜单提交",
     *     description="编辑菜单提交。",
     *     security={{ "Authorization-Bearer":{},"Authorization-key":{} }},
     *     @OA\Parameter(ref="#/components/parameters/id"),
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             mediaType="application/x-www-form-urlencoded",
     *             @OA\Schema(
     *                 required={"name","type","status","auth_open","level","affix","pid","sort"},
     *                 @OA\Property(
     *                      property="name",
     *                      type="string",
     *                      description="菜单名称",
     *                      default="系统管理"
     *                  ),
     *                  @OA\Property(
     *                      property="path",
     *                      type="string",
     *                      description="标识",
     *                      default="/admin"
     *                  ),
     *                  @OA\Property(
     *                      property="url",
     *                      type="string",
     *                      description="路由文件",
     *                      default="/admin/admin/index"
     *                  ),
     *                  @OA\Property(
     *                      property="redirect",
     *                      type="string",
     *                      description="重定向路径",
     *                      default="/admin/admin/index"
     *                  ),
     *                  @OA\Property(
     *                      property="type",
     *                      type="integer",
     *                      description="菜单类型:1=模块,2=目录,3=菜单",
     *                      default=1
     *                  ),
     *                  @OA\Property(
     *                      property="icon",
     *                      type="string",
     *                      description="角色描述",
     *                      default="fa fa-home"
     *                  ),
     *                  @OA\Property(
     *                      property="status",
     *                      type="integer",
     *                      description="状态:0=禁用,1=启用",
     *                      default=1
     *                  ),
     *                  @OA\Property(
     *                      property="auth_open",
     *                      type="integer",
     *                      description="是否验证权限:0=否,1=是",
     *                      default=1
     *                  ),
     *                  @OA\Property(
     *                      property="affix",
     *                      type="integer",
     *                      description="是否固定面板:0=否,1=是",
     *                      default=1
     *                  ),
     *                  @OA\Property(
     *                      property="pid",
     *                      type="integer",
     *                      description="父级ID（0为顶级）",
     *                      default=1
     *                  ),
     *                  @OA\Property(
     *                      property="sort",
     *                      type="integer",
     *                      description="排序",
     *                      default=50
     *                  )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *          response=200,
     *          description="请求成功",
     *          @OA\JsonContent(ref="#/components/schemas/success")
     *       )
     * )
     * @param UpdateRuleRequest $request
     * @param int $id
     * @return \Modules\Common\Services\JSON
     */
    public function update(UpdateRuleRequest $request,int $id)
    {
        return (new RuleServices())->update($id,$request->only([
            'path',
            'url',
            'redirect',
            'name',
            'type',
            'icon',
            "affix",
            "pid",
            "sort",
            "auth_open",
            "status",
        ]));
    }

    /**
     * @OA\Put(
     *     path="/admin/rule/status/{id}",
     *     operationId="admin-rule-status",
     *     tags={"Rule"},
     *     summary="菜单调整状态",
     *     description="菜单调整状态。",
     *     security={{ "Authorization-Bearer":{},"Authorization-key":{} }},
     *     @OA\Parameter(ref="#/components/parameters/id"),
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             mediaType="application/x-www-form-urlencoded",
     *             @OA\Schema(ref="#/components/schemas/status")
     *         )
     *     ),
     *     @OA\Response(
     *          response=200,
     *          description="请求成功",
     *          @OA\JsonContent(ref="#/components/schemas/success")
     *       )
     * )
     * @param CommonStatusRequest $request
     * @param int $id
     * @return \Modules\Common\Services\JSON
     */
    public function status(CommonStatusRequest $request,int $id){
        return (new RuleServices())->status($id,$request->only([
            "status"
        ]));
    }

    /**
     * @OA\Put(
     *     path="/admin/rule/authOpen/{id}",
     *     operationId="admin-rule-authOpen",
     *     tags={"Rule"},
     *     summary="菜单调整是否验证权限",
     *     description="菜单调整是否验证权限。",
     *     security={{ "Authorization-Bearer":{},"Authorization-key":{} }},
     *     @OA\Parameter(ref="#/components/parameters/id"),
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             mediaType="application/x-www-form-urlencoded",
     *             @OA\Schema(
     *                  required={"auth_open"},
     *                  @OA\Property(
     *                      property="auth_open",
     *                      type="integer",
     *                      description="是否验证权限:0=否,1=是",
     *                      default=1
     *                  )
     *              )
     *         )
     *     ),
     *     @OA\Response(
     *          response=200,
     *          description="请求成功",
     *          @OA\JsonContent(ref="#/components/schemas/success")
     *       )
     * )
     * @param AuthOpenRequest $request
     * @param int $id
     * @return \Modules\Common\Services\JSON
     */
    public function authOpen(AuthOpenRequest $request,int $id){
        return (new RuleServices())->status($id,$request->only([
            "auth_open"
        ]));
    }

    /**
     * @OA\Put(
     *     path="/admin/rule/affix/{id}",
     *     operationId="admin-rule-affix",
     *     tags={"Rule"},
     *     summary="菜单调整是否固定面板",
     *     description="菜单调整是否固定面板。",
     *     security={{ "Authorization-Bearer":{},"Authorization-key":{} }},
     *     @OA\Parameter(ref="#/components/parameters/id"),
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             mediaType="application/x-www-form-urlencoded",
     *             @OA\Schema(
     *                  required={"affix"},
     *                  @OA\Property(
     *                      property="affix",
     *                      type="integer",
     *                      description="是否固定面板:0=否,1=是",
     *                      default=1
     *                  )
     *              )
     *         )
     *     ),
     *     @OA\Response(
     *          response=200,
     *          description="请求成功",
     *          @OA\JsonContent(ref="#/components/schemas/success")
     *       )
     * )
     * @param AffixRequest $request
     * @param int $id
     * @return \Modules\Common\Services\JSON
     */
    public function affix(AffixRequest $request,int $id){
        return (new RuleServices())->status($id,$request->only([
            "affix"
        ]));
    }

    /**
     * @OA\Put(
     *     path="/admin/rule/sorts/{id}",
     *     operationId="admin-rule-sorts",
     *     tags={"Rule"},
     *     summary="菜单排序",
     *     description="菜单排序。",
     *     security={{ "Authorization-Bearer":{},"Authorization-key":{} }},
     *     @OA\Parameter(ref="#/components/parameters/id"),
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             mediaType="application/x-www-form-urlencoded",
     *             @OA\Schema(
     *                  required={"sort"},
     *                  @OA\Property(
     *                      property="sort",
     *                      type="integer",
     *                      description="排序",
     *                      default=50
     *                  )
     *              )
     *         )
     *     ),
     *     @OA\Response(
     *          response=200,
     *          description="请求成功",
     *          @OA\JsonContent(ref="#/components/schemas/success")
     *       )
     * )
     * @param CommonSortRequest $request
     * @param int $id
     * @return \Modules\Common\Services\JSON
     */
    public function sorts(CommonSortRequest $request,int $id){
        return (new RuleServices())->sorts($id,$request->only([
            "sort"
        ]));
    }

    /**
     * @OA\Get(
     *     path="/admin/rule/getPidList",
     *     operationId="admin-rule-getPidList",
     *     tags={"Rule"},
     *     summary="父级菜单列表",
     *     description="父级菜单列表。",
     *     security={{ "Authorization-Bearer":{},"Authorization-key":{} }},
     *     @OA\Response(
     *          response=200,
     *          description="请求成功",
     *          @OA\JsonContent(type="object",description="成功返回信息",required={"status","message","data"},
     *              @OA\Property(property="status", type="integer",format="20000",description="自定义状态码，20000表示成功"),
     *              @OA\Property(property="message", type="string",description="提示信息"),
     *              @OA\Property(property="data", type="array",description="成功返回具体的菜单列表数据",
     *                  @OA\Items(type="object",description="单条角色信息",required={"id","name","pid"},
     *                     @OA\Property(property="id", type="integer",description="菜单ID"),
     *                     @OA\Property(property="name", type="string",description="菜单名称"),
     *                     @OA\Property(property="pid", type="integer",description="父级ID"),
     *                     @OA\Property(property="children", type="array",description="下级数据",
     *                          @OA\items()
     *                      )
     *                  )
     *              ),
     *              example={"status": 20000,"message": "操作成功！","data": {{"id": 3,"name": "系统管理","pid": 0,"children": {{"id": 4,"name": "系统管理","pid": 3}}}}}
     *          )
     *       )
     * )
     * @param Request $request
     * @return \Modules\Common\Services\JSON
     */
    public function getPidList(Request $request){
        return (new RuleServices())->getPidList($request->get('id'));
    }

    /**
     * @OA\Delete(
     *     path="/admin/rule/del/{id}",
     *     operationId="admin-rule-del",
     *     tags={"Rule"},
     *     summary="菜单删除",
     *     description="菜单删除。",
     *     security={{ "Authorization-Bearer":{},"Authorization-key":{} }},
     *     @OA\Parameter(ref="#/components/parameters/id"),
     *     @OA\Response(
     *          response=200,
     *          description="请求成功",
     *          @OA\JsonContent(ref="#/components/schemas/success")
     *       )
     * )
     * @param int $id
     * @return \Modules\Common\Services\JSON
     */
    public function del(int $id){
        return (new RuleServices())->del($id);
    }
}
