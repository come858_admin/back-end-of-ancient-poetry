<?php
// +----------------------------------------------------------------------
// | Name: 咪乐多管理系统 [ 为了快速搭建软件应用而生的，希望能够帮助到大家提高开发效率。 ]
// +----------------------------------------------------------------------
// | Copyright: (c) 2020~2021 https://www.lvacms.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed: 这是一个自由软件，允许对程序代码进行修改，但希望您留下原有的注释。
// +----------------------------------------------------------------------
// | Author: 西安咪乐多软件 <997786358@qq.com>
// +----------------------------------------------------------------------
// | Version: V1
// +----------------------------------------------------------------------
/**
 * @OA\OpenApi(
 *  @OA\Info(
 *     title="LvaCMS2.0系统管理接口",
 *     version="2.0.0",
 *     description="LvaCMS2.0是基于最新Laravel 8.5框架和Ant Design Pro 5.0的后台管理系统。创立于2022年初，是一款完全免费开源的项目，他将是您轻松建站的首选利器。 框架易于功能扩展，代码维护，方便二次开发，帮助开发者简单高效降低二次开发成本，满足专注业务深度开发的需求。",
 *     termsOfService="http://www.lvacms.cn",
 *     @OA\Contact(
 *          name="宋博",
 *          url="http://www.lvacms.com",
 *          email="997786358@qq.cn"
 *     ),
 *     @OA\License(
 *         name="Apache 2.0",
 *         url="http://www.apache.org/licenses/LICENSE-2.0.html"
 *     )
 *  )
 * )
 */

/**
 * @OA\Server(
 *     url="http://www.lvacms2.0.com/api/v1",
 *     description="后台接口前缀v1"
 *  )
 */



/**
 * @OA\Server(
 *     url="http://www.lvuni.com/api/v2",
 *     description="后台接口前缀v2"
 *  )
 */


/**
 * @OA\SecurityScheme(
 *     type="apiKey",
 *     description="全局添加API Token鉴权",
 *     name="Authorization",
 *     in="header",
 *     securityScheme="Authorization-Bearer"
 * )
 */


/**
 * @OA\SecurityScheme(
 *     type="apiKey",
 *     description="全局key",
 *     name="apikey",
 *     in="header",
 *     securityScheme="Authorization-key"
 * )
 */

/**
 *@OA\Schema(
 *    schema="code200",
 *    @OA\Property(
 *        property="status(20000)",
 *        type="integer",
 *        format="20000",
 *        description="请求成功"
 *    )
 * )
 */
/**
 *@OA\Schema(
 *    schema="code400",
 *    @OA\Property(
 *        property="status(40000)",
 *        type="integer",
 *        format="40000",
 *        description="接口参数错误"
 *    )
 * )
 */






/**
 *@OA\Schema(
 *    schema="code500",
 *    @OA\Property(
 *        property="status(70001)",
 *        type="integer",
 *        format="70001",
 *        description="apikey错误"
 *    ),
 *    @OA\Property(
 *        property="status(70002)",
 *        type="integer",
 *        format="70002",
 *        description="请先登录"
 *    ),
 *    @OA\Property(
 *        property="status(70003)",
 *        type="integer",
 *        format="70003",
 *        description="token 被拉黑"
 *    ),
 *    @OA\Property(
 *        property="status(70004)",
 *        type="integer",
 *        format="70004",
 *        description="token 过期"
 *    ),
 *    @OA\Property(
 *        property="status(70005)",
 *        type="integer",
 *        format="70005",
 *        description="请先登录"
 *    ),
 *    @OA\Property(
 *        property="status(70006)",
 *        type="integer",
 *        format="70006",
 *        description="请先登录"
 *    ),
 *    @OA\Property(
 *        property="status(50001)",
 *        type="integer",
 *        format="50001",
 *        description="服务器语法错误"
 *    ),
 *    @OA\Property(
 *        property="status(50002)",
 *        type="integer",
 *        format="50002",
 *        description="服务器异常映射"
 *    ),
 *    @OA\Property(
 *        property="status(50003)",
 *        type="integer",
 *        format="50003",
 *        description="服务器运行期异常 运行时异常 运行异常 未检查异常"
 *    ),
 *    @OA\Property(
 *        property="status(50004)",
 *        type="integer",
 *        format="50004",
 *        description="服务器框架运行出错"
 *    ),
 *    @OA\Property(
 *        property="status(50005)",
 *        type="integer",
 *        format="50005",
 *        description="服务器语法错误，请注意查看信息"
 *    ),
 *    @OA\Property(
 *        property="status(50006)",
 *        type="integer",
 *        format="50006",
 *        description="服务器语法错误，请注意查看信息"
 *    ),
 *    @OA\Property(
 *        property="status(60000)",
 *        type="integer",
 *        format="60000",
 *        description="数据库链接问题"
 *    ),
 *    @OA\Property(
 *        property="status(60001)",
 *        type="integer",
 *        format="60001",
 *        description="数据模型错误"
 *    ),
 *    @OA\Property(
 *        property="status(60002)",
 *        type="integer",
 *        format="60002",
 *        description="数据库DB错误"
 *    ),
 * )
 */


/**
 * @OA\Parameter(
 *   name="page",
 *   in="query",
 *   description="页码",
 *   @OA\Schema(
 *      type="integer",
 *      format="int64"
 *   ),
 *   required=true,
 *   example=1
 * )
 **/
/**
 *@OA\Parameter(
 *   name="limit",
 *   in="query",
 *   description="每页显示条数",
 *   @OA\Schema(
 *      type="integer",
 *      format="int64"
 *   ),
 *   required=true,
 *   example=10
 *)
 **/
/**
 *@OA\Parameter(
 *   name="status",
 *   in="query",
 *   description="状态（0=禁用，1=启用）",
 *   @OA\Schema(
 *      type="integer",
 *      format="int1"
 *   )
 *)
 **/
/**
 *@OA\Parameter(
 *   name="created_at[0]",
 *   in="query",
 *   description="创建时间（需要传入['开始时间']）",
 *   @OA\Schema(
 *      type="date"
 *   )
 *)
 **/
/**
 *@OA\Parameter(
 *   name="created_at[1]",
 *   in="query",
 *   description="创建时间（需要传入['结束时间']）",
 *   @OA\Schema(
 *      type="date"
 *   )
 *)
 **/
/**
 *@OA\Parameter(
 *   name="updated_at[0]",
 *   in="query",
 *   description="更新时间（需要传入['开始时间']）",
 *   @OA\Schema(
 *      type="date"
 *   )
 *)
 **/
/**
 *@OA\Parameter(
 *   name="updated_at[1]",
 *   in="query",
 *   description="更新时间（需要传入['结束时间']）",
 *   @OA\Schema(
 *      type="date"
 *   )
 *)
 **/
/**
 *@OA\Parameter(
 *     name="id",
 *     in="path",
 *     description="ID",
 *     @OA\Schema(
 *         type="integer"
 *     ),
 *     required=true,
 *     example=1
 *)
 **/
/**
 *@OA\Schema(
 *    schema="status",
 *    required={"status"},
 *    @OA\Property(
 *          property="status",
 *          type="integer",
 *          description="状态：0=禁用,1=启用",
 *          default=1
 *    )
 *)
 **/
/**
 *@OA\Schema(
 *    schema="success",
 *    example={"status":20000,"message":"操作成功"},
 *    required={"status","message"},
 *    description="操作成功时返回的信息",
 *    @OA\Property(
 *        property="status",
 *        type="integer",
 *        description="自定义状态码，20000表示成功"
 *    ),
 *    @OA\Property(
 *        property="message",
 *        type="string",
 *        description="提示信息"
 *    )
 * )
 */
/**
 *@OA\Tag(name="Common", description="后台管理的公共接口")
 */
/**
 *@OA\Tag(name="Admin", description="后台管理的管理员管理相关接口")
 */
/**
 *@OA\Tag(name="Project", description="后台管理的项目管理相关接口")
 */
/**
 *@OA\Tag(name="Group", description="后台管理的角色管理相关接口")
 */
/**
 *@OA\Tag(name="Rule", description="后台管理的菜单管理相关接口")
 */
namespace Modules\Admin\Http\Controllers;

use Modules\Common\Controllers\BaseController;
class BaseApiController extends BaseController
{
        public function __construct()
        {

        }
}
