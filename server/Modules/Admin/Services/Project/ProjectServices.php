<?php
// +----------------------------------------------------------------------
// | Name: 咪乐多管理系统 [ 为了快速搭建软件应用而生的，希望能够帮助到大家提高开发效率。 ]
// +----------------------------------------------------------------------
// | Copyright: (c) 2020~2021 https://www.lvacms.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed: 这是一个自由软件，允许对程序代码进行修改，但希望您留下原有的注释。
// +----------------------------------------------------------------------
// | Author: 西安咪乐多软件 <997786358@qq.com>
// +----------------------------------------------------------------------
// | Version: V1
// +----------------------------------------------------------------------

/**
 * @Name 项目管理服务
 * @Description
 * @Auther 西安咪乐多软件
 * @Date 2021/11/20 19:06
 */

namespace Modules\Admin\Services\Project;


use Modules\Admin\Models\AdminProject;
use Modules\Admin\Services\BaseApiServices;
use Modules\Common\Exceptions\MessageData;

class ProjectServices extends BaseApiServices
{
    public function getProjectList(){
        $list = AdminProject::query()->select('id','name','type')->orderBy('id','desc')->get()->toArray();
        return $this->apiSuccess(   '',$list);
    }
    public function index(array $data){
        $model = AdminProject::query();
        $model = $this->queryCondition($model,$data,"name");
        if(isset($data['sort'])){
            $model = $this->querySort($model,$data['sort']);
        }
        if(isset($data['type']) && $data['type'] > 0){
            $model = $model->where('type',$data['type']);
        }
        $list = $model->with([
            'logoImage'=>function($query){
                $query->select('id','url','open');
            },
            'icoImage'=>function($query){
                $query->select('id','url','open');
            }
        ])->orderBy('id','desc')
            ->paginate($data['limit'])->toArray();
        return $this->apiSuccess('',[
            'list'=>$list['data'],
            'total'=>$list['total']
        ]);
    }


    public function add(array $data){
        return $this->commonCreate(AdminProject::query(),$data);
    }

    public function edit(int $id){
        $data = AdminProject::query()->with([
            'logoImage'=>function($query){
                $query->select('id','url','open');
            },
            'icoImage'=>function($query){
                $query->select('id','url','open');
            }
        ])->select( "id","name", "logo_id", "ico_id", "url", "description", "keywords", "status","type")->find($id)?:$this->apiError(MessageData::GET_API_ERROR);
        $data = $data->toArray();
        return $this->apiSuccess(MessageData::GET_API_SUCCESS,$data);
    }

    public function update(int $id,array $data){
        return $this->commonUpdate(AdminProject::query(),$id,$data);
    }
    public function status(int $id,array $data){
        return $this->commonStatusUpdate(AdminProject::query(),$id,$data);
    }
    public function del(int $id){
        return $this->commonDestroy(AdminProject::query(),['id'=>$id]);
    }
}
