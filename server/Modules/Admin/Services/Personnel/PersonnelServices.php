<?php
// +----------------------------------------------------------------------
// | Name: 咪乐多管理系统 [ 为了快速搭建软件应用而生的，希望能够帮助到大家提高开发效率。 ]
// +----------------------------------------------------------------------
// | Copyright: (c) 2020~2021 https://www.lvacms.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed: 这是一个自由软件，允许对程序代码进行修改，但希望您留下原有的注释。
// +----------------------------------------------------------------------
// | Author: 西安咪乐多软件 <997786358@qq.com>
// +----------------------------------------------------------------------
// | Version: V1
// +----------------------------------------------------------------------

/**
 * @Name 服务
 * @Description
 * @Auther 西安咪乐多软件
 * @Date 2021/11/20 19:06
 */
namespace Modules\Admin\Services\Personnel;
use Modules\Admin\Models\AdminPersonnel;
use Modules\Admin\Services\BaseApiServices;
use Modules\Common\Exceptions\MessageData;

class PersonnelServices extends BaseApiServices
{
    public function index(array $data)
    {
        $model = AdminPersonnel::query();
        $model = $this->queryCondition($model, $data, "name");
        if (isset($data['sort'])) {
            $model = $this->querySort($model, $data['sort']);
        }
        if (isset($data['department_id']) && $data['department_id'] != ''){
            $model = $model->where('department_id',$data['department_id']);
        }
        if (isset($data['sex']) && $data['sex'] != ''){
            $model = $model->where('sex',$data['sex']);
        }
        if (isset($data['nation']) && $data['nation'] != ''){
            $model = $model->where('nation',$data['nation']);
        }
        if (isset($data['education']) && $data['education'] != ''){
            $model = $model->where('education',$data['education']);
        }
        if (isset($data['marital_status']) && $data['marital_status'] != ''){
            $model = $model->where('marital_status',$data['marital_status']);
        }
        if (isset($data['position']) && $data['position'] != ''){
            $model = $model->where('position',$data['position']);
        }
        if (isset($data['political_outlook']) && $data['political_outlook'] != ''){
            $model = $model->where('political_outlook',$data['political_outlook']);
        }
        if (isset($data['personnel_type']) && $data['personnel_type'] != ''){
            $model = $model->where('personnel_type',$data['personnel_type']);
        }
        if (!empty($data['entry_time'])){
            $model = $model->whereBetween('entry_time',$data['entry_time']);
        }
        if (!empty($data['date_of_birth'])){
            $model = $model->whereBetween('date_of_birth',$data['date_of_birth']);
        }
        $list = $model->with([
            'departmentTo'=>function($query){
                $query->select('id','name');
            }
        ])->orderBy('id', 'desc')
            ->paginate($data['limit'])->toArray();
        return $this->apiSuccess('', [
            'list' => $list['data'],
            'total' => $list['total']
        ]);
    }




    public function indexData(array $data)
    {
        $model = AdminPersonnel::query();
        $model = $this->queryCondition($model, $data, "name");
        if (isset($data['sort'])) {
            $model = $this->querySort($model, $data['sort']);
        }
        if (isset($data['department_id']) && $data['department_id'] != ''){
            $model = $model->where('department_id',$data['department_id']);
        }
        if (isset($data['sex']) && $data['sex'] != ''){
            $model = $model->where('sex',$data['sex']);
        }
        if (isset($data['nation']) && $data['nation'] != ''){
            $model = $model->where('nation',$data['nation']);
        }
        if (isset($data['education']) && $data['education'] != ''){
            $model = $model->where('education',$data['education']);
        }
        if (isset($data['marital_status']) && $data['marital_status'] != ''){
            $model = $model->where('marital_status',$data['marital_status']);
        }
        if (isset($data['position']) && $data['position'] != ''){
            $model = $model->where('position',$data['position']);
        }
        if (isset($data['political_outlook']) && $data['political_outlook'] != ''){
            $model = $model->where('political_outlook',$data['political_outlook']);
        }
        if (isset($data['personnel_type']) && $data['personnel_type'] != ''){
            $model = $model->where('personnel_type',$data['personnel_type']);
        }
        if (!empty($data['entry_time'])){
            $model = $model->whereBetween('entry_time',$data['entry_time']);
        }
        if (!empty($data['date_of_birth'])){
            $model = $model->whereBetween('date_of_birth',$data['date_of_birth']);
        }
        $model = $model->where('status',1);
        $list = $model->with([
            'departmentTo'=>function($query){
                $query->select('id','name');
            }
        ])->orderBy('id', 'desc')
            ->paginate($data['limit'])->toArray();
        return $this->apiSuccess('', [
            'list' => $list['data'],
            'total' => $list['total']
        ]);
    }







    public function add(array $data)
    {
        return $this->commonCreate(AdminPersonnel::query(),$data);
    }
    public function edit(int $id)
    {
        $data = AdminPersonnel::query()->find($id)?:$this->apiError(MessageData::GET_API_ERROR);
        $data = $data->toArray();
        return $this->apiSuccess(MessageData::GET_API_SUCCESS,$data);
    }
    public function update(int $id,array $data)
    {
        return $this->commonUpdate(AdminPersonnel::query(),$id,$data);
    }
    public function status(int $id,array $data)
    {
        return $this->commonStatusUpdate(AdminPersonnel::query(),$id,$data);
    }
    public function sorts(int $id,array $data)
    {
        return $this->commonSortsUpdate(AdminPersonnel::query(),$id,$data);
    }
    public function del(int $id)
    {
        return $this->commonDestroy(AdminPersonnel::query(),['id'=>$id]);
    }
    public function delAll(array $idArr)
    {
        return $this->commonDestroy(AdminPersonnel::query(),$idArr);
    }
    public function getPersonnelList(){
        $data = AdminPersonnel::query()->select('id','name')->where(['status'=>1])->orderBy('id','desc')->get()->toArray();
        return $this->apiSuccess('',$data);
    }
}
