<?php
// +----------------------------------------------------------------------
// | Name: 咪乐多管理系统 [ 为了快速搭建软件应用而生的，希望能够帮助到大家提高开发效率。 ]
// +----------------------------------------------------------------------
// | Copyright: (c) 2020~2021 https://www.lvacms.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed: 这是一个自由软件，允许对程序代码进行修改，但希望您留下原有的注释。
// +----------------------------------------------------------------------
// | Author: 西安咪乐多软件 <997786358@qq.com>
// +----------------------------------------------------------------------
// | Version: V1
// +----------------------------------------------------------------------

/**
 * @Name 系统配置服务
 * @Description
 * @Auther 西安咪乐多软件
 * @Date 2021/11/20 19:06
 */

namespace Modules\Admin\Services\Config;


use Modules\Admin\Models\AdminConfig;
use Modules\Admin\Models\AdminImage;
use Modules\Admin\Services\BaseApiServices;
use Modules\Common\Exceptions\MessageData;

class ConfigServices extends BaseApiServices
{
    public function getConfig(string $code){
        $value = AdminConfig::query()->where('code',$code)->value('value')?:$this->apiError(MessageData::GET_API_ERROR);
        $value = self::unSerialize($value);
        if($code == 'SiteConfiguration'){
            $value['logo_image'] = AdminImage::query()->find($value['logo_id']);
        }else if($code == 'AboutUs'){
            $value['content'] = $this->getReplacePicUrl($value['content']);
        }
        return $this->apiSuccess(MessageData::GET_API_SUCCESS,$value);
    }


    public function setConfig(string $code,array $data){
        $data['updated_at'] = date('Y-m-d H:i:s');
        if($code == 'AboutUs'){
            $data['value']['content'] = $this->setPicUrl($data['value']['content']);
        }
        $data['value'] = serialize($data['value']);
        if (AdminConfig::query()->where('code',$code)->update($data)){
            return $this->apiSuccess(MessageData::UPDATE_API_SUCCESS);
        }
        $this->apiError(MessageData::UPDATE_API_ERROR);
    }

    public static function getCodeValue(string$code):Array
    {
        $value = AdminConfig::query()->where('code',$code)->value('value');
        return self::unSerialize($value);
    }


    public function setContent(string $content){
        if(!$content){
            $this->apiError('请输入解析内容');
        }
        $content = $this->setPicUrlContent($content);
        return $this->apiSuccess('解析成功',['content'=>$content]);
    }

    public function getConfigInfo()
    {
        $value = AdminConfig::query()->where('code','SiteConfiguration')->value('value');
        $value = self::unSerialize($value);
        $value['logo_image'] = AdminImage::query()->find($value['logo_id'])['http_url'];
        $value['logViewerUrl'] = self::getHttp(1).'/log-viewer';
        return $this->apiSuccess('',$value);
    }
}
