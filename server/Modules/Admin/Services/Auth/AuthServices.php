<?php
// +----------------------------------------------------------------------
// | Name: 咪乐多管理系统 [ 为了快速搭建软件应用而生的，希望能够帮助到大家提高开发效率。 ]
// +----------------------------------------------------------------------
// | Copyright: (c) 2020~2021 https://www.lvacms.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed: 这是一个自由软件，允许对程序代码进行修改，但希望您留下原有的注释。
// +----------------------------------------------------------------------
// | Author: 西安咪乐多软件 <997786358@qq.com>
// +----------------------------------------------------------------------
// | Version: V1
// +----------------------------------------------------------------------

/**
 * @Name  管理员信息服务
 * @Description
 * @Auther 西安咪乐多软件
 * @Date 2021/11/23 22:47
 */

namespace Modules\Admin\Services\Auth;


use Modules\Admin\Models\AdminAdmin;
use Modules\Admin\Services\BaseApiServices;

class AuthServices extends BaseApiServices
{

    public function my(){
        $userInfo = TokenServices::my();
        return $this->apiSuccess('',[
            'username'=>$userInfo->username
        ]);
    }

    public function logout(){
        TokenServices::logout();
        return $this->apiSuccess('退出成功');
    }

    public function refreshToken(){
        return $this->apiSuccess('刷新成功',(new TokenServices())->refreshToken());
    }

    public function updatePwd(array $data){
        $userInfo = TokenServices::my();
        $info = [
            'username'=>$userInfo['username'],
            'password'=>$data['y_password']
        ];
        if (true == \Auth::guard('auth_admin')->attempt($info)) {
            $res = AdminAdmin::where(['id'=>$userInfo['id']])->update([
                'password'=>bcrypt($data['password'])
            ]);
            if($res !== false){
                return $this->apiSuccess('修改成功');
            }
        }
        $this->apiError('原密码错误');
    }
}
