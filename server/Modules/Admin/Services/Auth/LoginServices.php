<?php
// +----------------------------------------------------------------------
// | Name: 咪乐多管理系统 [ 为了快速搭建软件应用而生的，希望能够帮助到大家提高开发效率。 ]
// +----------------------------------------------------------------------
// | Copyright: (c) 2020~2021 https://www.lvacms.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed: 这是一个自由软件，允许对程序代码进行修改，但希望您留下原有的注释。
// +----------------------------------------------------------------------
// | Author: 西安咪乐多软件 <997786358@qq.com>
// +----------------------------------------------------------------------
// | Version: V1
// +----------------------------------------------------------------------

/**
 * @Name 登录服务
 * @Description
 * @Auther 西安咪乐多软件
 * @Date 2021/11/23 22:24
 */

namespace Modules\Admin\Services\Auth;


use Modules\Admin\Models\AdminAdmin;
use Modules\Admin\Services\BaseApiServices;

class LoginServices extends BaseApiServices
{
    public function login(array $data){
        if (!captcha_api_check($data['captcha'], $data['key'])){
            return $this->apiError('验证码错误！');
        }
        unset($data['captcha'], $data['key']);
        if (true == \Auth::guard('auth_admin')->attempt($data)) {
            $userInfo = AdminAdmin::where(['username'=>$data['username']])->select('id','username')->first();
            if($userInfo){
                $userInfo = $userInfo->toArray();
                $userInfo['password'] = $data['password'];
                $token = (new TokenServices())->setToken($userInfo);
                $token['admin_url'] = env('ADMIN_URL','/');
                return $this->apiSuccess('登录成功！',$token);
            }
        }
        $this->apiError('账号或密码错误！');
    }
    public function getCode(){
        return $this->apiSuccess('',[
            'codeData'=>app('captcha')->create('default', true)
        ]);
    }
}
