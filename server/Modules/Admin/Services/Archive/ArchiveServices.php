<?php
// +----------------------------------------------------------------------
// | Name: 咪乐多管理系统 [ 为了快速搭建软件应用而生的，希望能够帮助到大家提高开发效率。 ]
// +----------------------------------------------------------------------
// | Copyright: (c) 2020~2021 https://www.lvacms.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed: 这是一个自由软件，允许对程序代码进行修改，但希望您留下原有的注释。
// +----------------------------------------------------------------------
// | Author: 西安咪乐多软件 <997786358@qq.com>
// +----------------------------------------------------------------------
// | Version: V1
// +----------------------------------------------------------------------

/**
 * @Name 标签管理服务
 * @Description
 * @Auther 西安咪乐多软件
 * @Date 2021/11/20 19:06
 */

namespace Modules\Admin\Services\Archive;

use Illuminate\Support\Facades\DB;
use Modules\Admin\Models\AdminArchive;
use Modules\Admin\Models\AdminImage;
use Modules\Admin\Services\ArchiveType\ArchiveTypeServices;
use Modules\BlogAdmin\Models\BlogArticle;
use Modules\BlogAdmin\Models\BlogArticleLabel;
use Modules\BlogAdmin\Models\BlogArticleType;
use Modules\BlogAdmin\Models\BlogLabel;
use Modules\BlogAdmin\Services\ArticleType\ArticleTypeServices;
use Modules\BlogAdmin\Services\BaseApiServices;
use Modules\Common\Exceptions\MessageData;

class ArchiveServices extends BaseApiServices
{
    public function index(array $data){
        $model = AdminArchive::query();
        $model = $this->queryCondition($model,$data,"description");
        if(isset($data['personnel_id']) && $data['personnel_id'] != '' && $data['personnel_id'] > 0){
            $model = $model->where('personnel_id',$data['personnel_id']);
        }
        if(isset($data['archive_type_id']) && $data['archive_type_id'] != '' && $data['archive_type_id'] > 0){
            $model = $model->where('archive_type_id',$data['archive_type_id']);
        }
        if(isset($data['sort'])){
            $model = $this->querySort($model,$data['sort']);
        }
        if (!empty($data['department_id'])){
            $model = $model->whereHas('personnelTo',function($quest)use($data){
                $quest->where('department_id',$data['department_id']);
            });
        }
        $list = $model->with([
                'archiveTo'=>function($query){
                    $query->select('id','name');
                },
                'personnelTo'=>function($query){
                    $query->select('id','name','department_id');
                },
                'personnelTo.departmentTo'=>function($query){
                    $query->select('id','name');
                },
            ])->orderBy('id','desc')
            ->paginate($data['limit'])->toArray();

        foreach ($list['data'] as $k=>$v) {
            if ($v['images']) {
                $v['images'] = AdminImage::query()->whereIn('id', explode('|', $v['images']))->get()->toArray();
            } else {
                $v['images'] = [];
            }
            $list['data'][$k] = $v;
        }
        return $this->apiSuccess('',[
            'list'=>$list['data'],
            'total'=>$list['total']
        ]);
    }
    public function add(array $data){
        if(count($data['images'])){
            $data['images'] = implode('|',array_column($data['images'],'id'));
        }else{
            $data['images'] = '';
        }
        return $this->commonCreate(AdminArchive::query(),$data);
    }
    public function edit(int $id){
        $data = AdminArchive::query()->find($id)?:$this->apiError(MessageData::GET_API_ERROR);
        $data = $data->toArray();
        if($data['images']){
            $data['images'] = AdminImage::query()->whereIn('id',explode('|',$data['images']))->get()->toArray();
        }else{
            $data['images'] = [];
        }
        $data['type_arr'] = (new ArchiveTypeServices())->getIdArr($data['archive_type_id']);
        return $this->apiSuccess(MessageData::GET_API_SUCCESS,$data);
    }
    public function update(int $id,array $data){
        if(count($data['images'])){
            $data['images'] = implode('|',array_column($data['images'],'id'));
        }else{
            $data['images'] = '';
        }
        return $this->commonUpdate(AdminArchive::query(),$id,$data);
    }
    public function status(int $id,array $data){
        return $this->commonStatusUpdate(AdminArchive::query(),$id,$data);
    }
    public function sorts(int $id,array $data){
        return $this->commonSortsUpdate(AdminArchive::query(),$id,$data);
    }
    public function del(int $id){
        return $this->commonDestroy(AdminArchive::query(),['id'=>$id]);
    }
    public function delAll(array $idArr){
        return $this->commonDestroy(AdminArchive::query(),$idArr);
    }
}
