<?php

//use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Jialeo\LaravelSchemaExtend\Schema;

class CreateAdminArchivesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_archives', function (Blueprint $table) {
            $table->comment = '档案表';
            $table->increments('id')->comment('档案ID');
            $table->integer('personnel_id')->default(0)->comment('人员ID');
            $table->integer('archive_type_id')->default(0)->comment('模板ID');
            $table->text('images')->nullable()->comment('资料文件');
            $table->string('description')->nullable()->default('')->comment('描述');
            $table->tinyInteger('status')->default(1)->comment('状态:0=隐藏,1=显示');
            $table->timestamp('created_at')->nullable()->comment('创建时间');
            $table->timestamp('updated_at')->nullable()->comment('更新时间');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_archives');
    }
}
