<?php

//use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Jialeo\LaravelSchemaExtend\Schema;

class CreateAdminTestDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_test_datas', function (Blueprint $table) {
            $table->comment = '测试表';
            $table->increments('id')->comment('ID');
            $table->string('name')->default('')->comment('名称');
            $table->longtext('content')->nullable()->comment('文章详情');
            $table->string('url')->default('')->comment('url');
            $table->timestamp('created_at')->nullable()->comment('创建时间');
            $table->timestamp('updated_at')->nullable()->comment('更新时间');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_test_datas');
    }
}
