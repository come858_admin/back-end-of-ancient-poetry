<?php

//use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Jialeo\LaravelSchemaExtend\Schema;

class CreateAdminPersonnelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_personnels', function (Blueprint $table) {
            $table->comment = '人员管理表';
            $table->increments('id')->comment('人员管理ID');
            $table->string('name',100)->default('')->comment('姓名');
            $table->integer('department_id')->nullable()->comment('部门ID');
            $table->tinyInteger('sex')->default(1)->comment('性别:0=女,1=男');
            $table->string('nation',100)->default('')->comment('民族');
            $table->timestamp('date_of_birth')->nullable()->comment('出生日期');
            $table->tinyInteger('education')->default(1)->comment('学历:1=小学，2=初中，3=高中，4=大专，5=本科，6=研究生，7=博士');
            $table->tinyInteger('marital_status')->default(1)->comment('婚姻状况:0=未婚,1=已婚');
            $table->string('position',100)->default('')->comment('职位');
            $table->string('political_outlook',100)->default('')->comment('政治面貌');
            $table->string('personnel_type',100)->default('')->comment('人员类别');
            $table->timestamp('entry_time')->nullable()->comment('入职时间');
            $table->tinyInteger('status')->default(1)->comment('状态:0=禁用,1=启用');
            $table->timestamp('created_at')->nullable()->comment('创建时间');
            $table->timestamp('updated_at')->nullable()->comment('更新时间');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_personnels');
    }
}
