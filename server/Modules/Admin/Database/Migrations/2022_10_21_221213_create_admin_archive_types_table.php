<?php

//use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Jialeo\LaravelSchemaExtend\Schema;

class CreateAdminArchiveTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_archive_types', function (Blueprint $table) {
            $table->comment = '档案模板表';
            $table->increments('id')->comment('档案模板ID');
            $table->string('name',100)->default('')->comment('模板名称');
            $table->string('description')->nullable()->default('')->comment('描述');
            $table->tinyInteger('status')->default(1)->comment('状态:0=隐藏,1=显示');
            $table->tinyInteger('level')->default(1)->comment('级别');
            $table->integer('pid')->default(0)->comment('父级ID');
            $table->integer('sort')->default(1)->comment('排序');
            $table->timestamp('created_at')->nullable()->comment('创建时间');
            $table->timestamp('updated_at')->nullable()->comment('更新时间');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_archive_types');
    }
}
