<?php
//use Illuminate\Support\Facades\Schema;
use Jialeo\LaravelSchemaExtend\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminOperationLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_operation_logs', function (Blueprint $table) {
            $table->comment = '操作日志表';
            $table->increments('id')->comment('操作日志ID');
            $table->string('name',50)->default('')->comment('操作名称');
            $table->string('url',100)->default('')->comment('操作接口');
            $table->string('pathname',100)->default('')->comment('操作路由');
            $table->string('method',10)->default('')->comment('请求方式');
            $table->string('ip',20)->default('')->comment('请求ip');
            $table->integer('admin_id')->nullable()->comment('管理员id');
            $table->longtext('data')->nullable()->comment('请求数据');
            $table->longtext('header')->nullable()->comment('请求header');
            $table->timestamp('created_at')->nullable()->comment('操作时间');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_operation_logs');
    }
}
