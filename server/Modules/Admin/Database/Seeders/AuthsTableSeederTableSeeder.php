<?php

namespace Modules\Admin\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class AuthsTableSeederTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $image_id = DB::table('admin_images')->insertGetId([
            'url' => '/logo.png',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        $group_id = DB::table('admin_groups')->insertGetId([
            'name' => '超级管理员',
            'created_at'=>date('Y-m-d H:i:s'),
            'rules'=>'1|2']);
        $project_id1 = DB::table('admin_projects')->insertGetId([
            'id'=>1,
            'name' => '测试博客',
            'created_at'=>date('Y-m-d H:i:s'),
            'url'=>'https://www.baidu.com',
            'description'=>'测试描述',
            'keywords'=>'测试关键词',
            'logo_id'=>$image_id,
            'ico_id'=>$image_id,
            'type'=>1,
        ]);
        $project_id9 = DB::table('admin_projects')->insertGetId([
            'id'=>9,
            'name' => '古文',
            'created_at'=>date('Y-m-d H:i:s'),
            'url'=>'https://www.lvacms.cn',
            'description'=>'古文',
            'keywords'=>'古文',
            'logo_id'=>$image_id,
            'ico_id'=>$image_id,
            'type'=>10,
        ]);

        $project_id10 = DB::table('admin_projects')->insertGetId([
            'id'=>10,
            'name' => '工具管理',
            'created_at'=>date('Y-m-d H:i:s'),
            'url'=>'https://www.lvacms.cn',
            'description'=>'工具管理',
            'keywords'=>'工具管理',
            'logo_id'=>$image_id,
            'ico_id'=>$image_id,
            'type'=>11,
        ]);


        DB::table('admin_admins')->insert([
            'username' => 'admin',
            'password' => bcrypt('123456'),
            'group_id' => $group_id,
            'project_id' => $project_id1 . "|" . $project_id9 . '|' . $project_id10,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        // 站点配置
        DB::table('admin_configs')->insert([
            'code' => 'SiteConfiguration',
            'value'=> serialize([
                'name'=>'古诗文后台管理',
                'image_status' => 1,
                'logo_id' => $image_id,
                'keywords'=>'古诗文',
                'description'=>'古诗文'
            ]),
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        // 关于我们
        DB::table('admin_configs')->insert([
            'code' => 'AboutUs',
            'value'=>serialize([
                'content'=>'关于我们'
            ]),
            'created_at'=>date('Y-m-d H:i:s')
        ]);



        /****************************模块管理******************************************/
        $pid1 = DB::table('admin_rules')->insertGetId([
            'name'=>'系统管理',
            'status'=>1,
            'auth_open'=>1,
            'path'=>'/admin',
            'pid'=>0,
            'level'=>1,
            'type'=>1,
            'sort'=>1,
            'icon'=>'Windows',
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        /****************************数据看板******************************************/
        $pid2 = DB::table('admin_rules')->insertGetId([
            'name'=>'数据看板',
            'path'=>'/admin/dashboard',
            'url'=>'./admin/dashboard/index',
            'status'=>1,
            'auth_open'=>1,
            'icon'=>'AreaChart',
            'pid'=>$pid1,
            'level'=>2,
            'type'=>3,
            'sort'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        /****************************权限管理******************************************/
        $pid2 = DB::table('admin_rules')->insertGetId([
            'name'=>'权限管理',
            'path'=>'/admin/auth',
            'status'=>1,
            'auth_open'=>1,
            'icon'=>'UsergroupAdd',
            'pid'=>$pid1,
            'level'=>2,
            'type'=>2,
            'sort'=>2,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        /****************************管理员管理******************************************/
        $pid3 = DB::table('admin_rules')->insertGetId([
            'name'=>'管理员管理',
            'path'=>'/admin/auth/admin',
            'url'=>'./admin/auth/admin/index',
            'status'=>1,
            'auth_open'=>1,
            'icon'=>'User',
            'pid'=>$pid2,
            'level'=>3,
            'type'=>3,
            'sort'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        /****************************角色管理******************************************/
        $pid3 = DB::table('admin_rules')->insertGetId([
            'name'=>'角色管理',
            'path'=>'/admin/auth/group',
            'url'=>'./admin/auth/group/index',
            'status'=>1,
            'auth_open'=>1,
            'icon'=>'Group',
            'pid'=>$pid2,
            'level'=>3,
            'type'=>3,
            'sort'=>2,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        /****************************菜单管理******************************************/
        $pid3 = DB::table('admin_rules')->insertGetId([
            'name'=>'菜单管理',
            'path'=>'/admin/auth/rule',
            'url'=>'./admin/auth/rule/index',
            'status'=>1,
            'auth_open'=>1,
            'icon'=>'Menu',
            'pid'=>$pid2,
            'level'=>3,
            'type'=>3,
            'sort'=>3,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        /****************************项目管理******************************************/
        $pid3 = DB::table('admin_rules')->insertGetId([
            'name'=>'项目管理',
            'path'=>'/admin/auth/project',
            'url'=>'./admin/auth/project/index',
            'status'=>1,
            'auth_open'=>1,
            'icon'=>'FundProjectionScreen',
            'pid'=>$pid2,
            'level'=>3,
            'type'=>3,
            'sort'=>4,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        /****************************系统配置******************************************/
        $pid2 = DB::table('admin_rules')->insertGetId([
            'name'=>'系统配置',
            'path'=>'/admin/config',
            'status'=>1,
            'auth_open'=>1,
            'icon'=>'Setting',
            'pid'=>$pid1,
            'level'=>2,
            'type'=>2,
            'sort'=>3,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $pid3 = DB::table('admin_rules')->insertGetId([
            'name'=>'后台配置',
            'path'=>'/admin/config/backstage',
            'url'=>'./admin/config/backstage/index',
            'status'=>1,
            'auth_open'=>1,
            'icon'=>'Rollback',
            'pid'=>$pid2,
            'level'=>3,
            'type'=>3,
            'sort'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        $pid3 = DB::table('admin_rules')->insertGetId([
            'name'=>'关于我们',
            'path'=>'/admin/config/aboutUs',
            'url'=>'./admin/config/aboutUs/index',
            'status'=>1,
            'auth_open'=>1,
            'icon'=>'QuestionCircle',
            'pid'=>$pid2,
            'level'=>3,
            'type'=>3,
            'sort'=>2,
            'created_at'=>date('Y-m-d H:i:s')
        ]);




        /****************************数据库管理******************************************/
        $pid2 = DB::table('admin_rules')->insertGetId([
            'name'=>'数据库管理',
            'path'=>'/admin/dataBase',
            'status'=>1,
            'auth_open'=>1,
            'icon'=>'Database',
            'pid'=>$pid1,
            'level'=>2,
            'type'=>2,
            'sort'=>4,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $pid3 = DB::table('admin_rules')->insertGetId([
            'name'=>'数据表管理',
            'path'=>'/admin/dataBase/tables',
            'url'=>'./admin/dataBase/tables/index',
            'status'=>1,
            'auth_open'=>1,
            'icon'=>'Table',
            'pid'=>$pid2,
            'level'=>3,
            'type'=>3,
            'sort'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        $pid3 = DB::table('admin_rules')->insertGetId([
            'name'=>'备份列表',
            'path'=>'/admin/dataBase/restoreData',
            'url'=>'./admin/dataBase/restoreData/index',
            'status'=>1,
            'auth_open'=>1,
            'icon'=>'Copy',
            'pid'=>$pid2,
            'level'=>3,
            'type'=>3,
            'sort'=>2,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        /****************************日志管理******************************************/
        $pid2 = DB::table('admin_rules')->insertGetId([
            'name'=>'日志管理',
            'path'=>'/admin/log',
            'status'=>1,
            'auth_open'=>1,
            'icon'=>'FileSync',
            'pid'=>$pid1,
            'level'=>2,
            'type'=>2,
            'sort'=>5,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $pid3 = DB::table('admin_rules')->insertGetId([
            'name'=>'操作日志',
            'path'=>'/admin/log/operationLog',
            'url'=>'./admin/log/operationLog/index',
            'status'=>1,
            'auth_open'=>1,
            'icon'=>'FileDone',
            'pid'=>$pid2,
            'level'=>3,
            'type'=>3,
            'sort'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        $pid3 = DB::table('admin_rules')->insertGetId([
            'name'=>'异常日志',
            'path'=>'/admin/log/logViewer',
            'url'=>'./admin/log/logViewer/index',
            'status'=>1,
            'auth_open'=>1,
            'icon'=>'Bug',
            'pid'=>$pid2,
            'level'=>3,
            'type'=>3,
            'sort'=>2,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $pid3 = DB::table('admin_rules')->insertGetId([
            'name'=>'日志文件管理',
            'path'=>'/admin/log/storage',
            'url'=>'./admin/log/storage/index',
            'status'=>1,
            'auth_open'=>1,
            'icon'=>'FileDone',
            'pid'=>$pid2,
            'level'=>3,
            'type'=>3,
            'sort'=>3,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        /****************************信息管理******************************************/
        $pid2 = DB::table('admin_rules')->insertGetId([
            'name'=>'信息管理',
            'path'=>'/admin/information',
            'status'=>1,
            'auth_open'=>1,
            'icon'=>'FileSync',
            'pid'=>$pid1,
            'level'=>2,
            'type'=>2,
            'sort'=>6,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $pid3 = DB::table('admin_rules')->insertGetId([
            'name'=>'部门管理',
            'path'=>'/admin/information/department',
            'url'=>'./admin/information/department/index',
            'status'=>1,
            'auth_open'=>1,
            'icon'=>'FileDone',
            'pid'=>$pid2,
            'level'=>3,
            'type'=>3,
            'sort'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $pid3 = DB::table('admin_rules')->insertGetId([
            'name'=>'人员管理',
            'path'=>'/admin/information/personnel',
            'url'=>'./admin/information/personnel/index',
            'status'=>1,
            'auth_open'=>1,
            'icon'=>'FileDone',
            'pid'=>$pid2,
            'level'=>3,
            'type'=>3,
            'sort'=>2,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $pid3 = DB::table('admin_rules')->insertGetId([
            'name'=>'档案模板',
            'path'=>'/admin/information/archiveType',
            'url'=>'./admin/information/archiveType/index',
            'status'=>1,
            'auth_open'=>1,
            'icon'=>'FileDone',
            'pid'=>$pid2,
            'level'=>3,
            'type'=>3,
            'sort'=>3,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $pid3 = DB::table('admin_rules')->insertGetId([
            'name'=>'档案管理',
            'path'=>'/admin/information/archive',
            'url'=>'./admin/information/archive/index',
            'status'=>1,
            'auth_open'=>1,
            'icon'=>'FileDone',
            'pid'=>$pid2,
            'level'=>3,
            'type'=>3,
            'sort'=>4,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        /****************************档案用户******************************************/
        $pid2 = DB::table('admin_rules')->insertGetId([
            'name'=>'档案用户',
            'path'=>'/admin/archiveUser',
            'status'=>1,
            'auth_open'=>1,
            'icon'=>'FileSync',
            'pid'=>$pid1,
            'level'=>2,
            'type'=>2,
            'sort'=>7,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $pid3 = DB::table('admin_rules')->insertGetId([
            'name'=>'用户查询',
            'path'=>'/admin/archiveUser/personnelData',
            'url'=>'./admin/archiveUser/personnelData/index',
            'status'=>1,
            'auth_open'=>1,
            'icon'=>'FileDone',
            'pid'=>$pid2,
            'level'=>3,
            'type'=>3,
            'sort'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
    }
}
