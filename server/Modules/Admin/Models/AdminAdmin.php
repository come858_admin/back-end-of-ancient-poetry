<?php
// +----------------------------------------------------------------------
// | Name: 咪乐多管理系统 [ 为了快速搭建软件应用而生的，希望能够帮助到大家提高开发效率。 ]
// +----------------------------------------------------------------------
// | Copyright: (c) 2020~2021 https://www.lvacms.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed: 这是一个自由软件，允许对程序代码进行修改，但希望您留下原有的注释。
// +----------------------------------------------------------------------
// | Author: 西安咪乐多软件 <997786358@qq.com>
// +----------------------------------------------------------------------
// | Version: V1
// +----------------------------------------------------------------------

/**
 * @Name 管理员管理模型
 * @Description
 * @Auther 西安咪乐多软件
 * @Date 2021/11/20 19:03
 */

namespace Modules\Admin\Models;

use DateTimeInterface;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
class AdminAdmin extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * @name jwt标识
     * @description
     * @author 西安咪乐多软件
     * @date 2021/6/12 3:11
     **/
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    /**
     * @name jwt自定义声明
     * @description
     * @author 西安咪乐多软件
     * @date 2021/6/12 3:11
     **/
    public function getJWTCustomClaims()
    {
        return [];
    }
    /**
     * @name 时间格式传唤
     * @description
     * @author 西安咪乐多软件
     * @date 2021/6/17 16:15
     **/
    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
    /**
     * @name 隐藏数据
     * @author 西安咪乐多软件
     * @date 2021/11/21 16:55
     **/
    protected $hidden = [
        'password'
    ];
    /**
     * @name 关联角色   多对一
     * @author 西安咪乐多软件
     * @date 2021/11/21 17:12
     **/
    public function adminGroup()
    {
        return $this->belongsTo(AdminGroup::class,'group_id','id');
    }
}
