<?php

namespace Modules\BlogAdmin\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class BlogAdminDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

         $this->call(BlogInfosTableSeederTableSeeder::class);
         $this->call(BlogsTableSeederTableSeeder::class);
    }
}
