<?php

namespace Modules\BlogAdmin\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class BlogsTableSeederTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        /****************************博客管理******************************************/
        $pid1 = DB::table('admin_rules')->insertGetId([
            'name'=>'博客管理',
            'status'=>1,
            'auth_open'=>1,
            'path'=>'/blogAdmin',
            'pid'=>0,
            'level'=>1,
            'type'=>1,
            'sort'=>2,
            'icon'=>'Block',
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        /****************************数据看板******************************************/
        $pid2 = DB::table('admin_rules')->insertGetId([
            'name'=>'数据看板',
            'path'=>'/blogAdmin/dashboard',
            'url'=>'./blogAdmin/dashboard/index',
            'status'=>1,
            'auth_open'=>1,
            'icon'=>'AreaChart',
            'pid'=>$pid1,
            'level'=>2,
            'type'=>3,
            'sort'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        /****************************文章管理******************************************/
        $pid2 = DB::table('admin_rules')->insertGetId([
            'name'=>'文章管理',
            'path'=>'/blogAdmin/article',
            'status'=>1,
            'auth_open'=>1,
            'icon'=>'Highlight',
            'pid'=>$pid1,
            'level'=>2,
            'type'=>2,
            'sort'=>2,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        /****************************文章分类******************************************/
        $pid3 = DB::table('admin_rules')->insertGetId([
            'name'=>'文章分类',
            'path'=>'/blogAdmin/article/articleType',
            'url'=>'./blogAdmin/article/articleType/index',
            'status'=>1,
            'auth_open'=>1,
            'icon'=>'FileMarkdown',
            'pid'=>$pid2,
            'level'=>3,
            'type'=>3,
            'sort'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        $pid3 = DB::table('admin_rules')->insertGetId([
            'name'=>'标签管理',
            'path'=>'/blogAdmin/article/label',
            'url'=>'./blogAdmin/article/label/index',
            'status'=>1,
            'auth_open'=>1,
            'icon'=>'BorderOuter',
            'pid'=>$pid2,
            'level'=>3,
            'type'=>3,
            'sort'=>2,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        $pid3 = DB::table('admin_rules')->insertGetId([
            'name'=>'文章列表',
            'path'=>'/blogAdmin/article/article',
            'url'=>'./blogAdmin/article/article/index',
            'status'=>1,
            'auth_open'=>1,
            'icon'=>'AlignCenter',
            'pid'=>$pid2,
            'level'=>3,
            'type'=>3,
            'sort'=>3,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $pid2 = DB::table('admin_rules')->insertGetId([
            'name'=>'配置',
            'path'=>'/blogAdmin/setting',
            'status'=>1,
            'auth_open'=>1,
            'icon'=>'Setting',
            'pid'=>$pid1,
            'level'=>2,
            'type'=>2,
            'sort'=>3,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        /****************************友情链接******************************************/
        $pid3 = DB::table('admin_rules')->insertGetId([
            'name'=>'友情链接',
            'path'=>'/blogAdmin/setting/link',
            'url'=>'./blogAdmin/setting/link/index',
            'status'=>1,
            'auth_open'=>1,
            'icon'=>'link',
            'pid'=>$pid2,
            'level'=>3,
            'type'=>3,
            'sort'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        /****************************图片管理******************************************/
        $pid3 = DB::table('admin_rules')->insertGetId([
            'name'=>'图片管理',
            'path'=>'/blogAdmin/setting/picture',
            'url'=>'./blogAdmin/setting/picture/index',
            'status'=>1,
            'auth_open'=>1,
            'icon'=>'Picture',
            'pid'=>$pid2,
            'level'=>3,
            'type'=>3,
            'sort'=>2,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        /****************************项目配置******************************************/
        $pid3 = DB::table('admin_rules')->insertGetId([
            'name'=>'项目配置',
            'path'=>'/blogAdmin/setting/project',
            'url'=>'./blogAdmin/setting/project/index',
            'status'=>1,
            'auth_open'=>1,
            'icon'=>'CarryOut',
            'pid'=>$pid2,
            'level'=>3,
            'type'=>3,
            'sort'=>3,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $pid2 = DB::table('admin_rules')->insertGetId([
            'name'=>'文件提取',
            'path'=>'/blogAdmin/crawler',
            'status'=>1,
            'auth_open'=>1,
            'icon'=>'Copy',
            'pid'=>$pid1,
            'level'=>2,
            'type'=>2,
            'sort'=>4,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        /****************************视频提取******************************************/
        $pid3 = DB::table('admin_rules')->insertGetId([
            'name'=>'视频提取',
            'path'=>'/blogAdmin/crawler/video',
            'url'=>'./blogAdmin/crawler/video/index',
            'status'=>1,
            'auth_open'=>1,
            'icon'=>'VideoCameraAdd',
            'pid'=>$pid2,
            'level'=>3,
            'type'=>3,
            'sort'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
    }
}
