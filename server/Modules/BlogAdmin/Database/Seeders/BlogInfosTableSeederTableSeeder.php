<?php

namespace Modules\BlogAdmin\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class BlogInfosTableSeederTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $favicon = DB::table('admin_images')->insertGetId([
            'url' => '/static/BlogAdmin/favicon.ico',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $gzh = DB::table('admin_images')->insertGetId([
            'url' => '/static/BlogAdmin/gzh.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        $html = DB::table('admin_images')->insertGetId([
            'url' => '/static/BlogAdmin/html.webp',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        $logo = DB::table('admin_images')->insertGetId([
            'url' => '/static/BlogAdmin/logo.png',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        $wx = DB::table('admin_images')->insertGetId([
            'url' => '/static/BlogAdmin/wx.jpg',
            'open' => 1,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);




        DB::table('admin_projects')->where('id',1)->update([
            'name'=>'程序员大象',
            'logo_id'=>$logo,
            'ico_id'=>$favicon,
            'url'=>'https://dxblog.lvacms.cn',
            'description'=>'（微信：songbo236589）我是一个程序员，工作有八年的时间了，目前从事前后端相关的一些开发，如果有需要共同学习开发相关知识的可以加我微信，可以备注加群，我可以拉你进入先关的微信群。',
            'keywords'=>'程序员大象，IT,前端，nuxt，ts，vue，php，ant design，react，lvacms作者',
            'ext'=>'a:21:{s:12:"gzh_image_id";i:'.$gzh.';s:11:"wx_image_id";i:'.$wx.';s:5:"about";s:28:"<p>暂时未使用这个</p>";s:9:"statement";s:12:"测试描述";s:5:"title";s:15:"程序员大象";s:3:"icp";s:24:"陕ICP备2020013302号-1";s:6:"header";N;s:6:"footer";N;s:14:"general_embody";N;s:12:"quick_embody";N;s:6:"robots";N;s:12:"network_name";s:15:"A~LvaCMS 作者";s:10:"occupation";s:15:"后端程序员";s:17:"current_residence";s:13:"陕西-西安";s:5:"email";s:16:"997786358@qq.com";s:2:"qq";s:9:"997786358";s:2:"wx";s:12:"songbo236589";s:17:"reprint_statement";s:249:"感谢您对程序员大象个人博客网站平台的认可，以及对我们原创作品以及文章的青睐，非常欢迎各位朋友分享到个人站长或者朋友圈，但转载请说明文章出处“来源程序员大象个人博客”。";s:26:"station_establishment_time";s:16:"2023年3月20日";s:15:"website_program";s:27:"《优咪乐古诗词2.0》";s:19:"website_program_url";s:52:"https://gitee.com/song-bo/back-end-of-ancient-poetry";}'

        ]);
        /****************************分类******************************************/
        $pid1 = DB::table('blog_article_types')->insertGetId([
            'project_id'=>1,
            'admin_id'=>1,
            'type'=>1,
            'name'=>'首页',
            'status'=>1,
            'level'=>1,
            'pid'=>0,
            'sort'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        $pid1 = DB::table('blog_article_types')->insertGetId([
            'project_id'=>1,
            'admin_id'=>1,
            'type'=>1,
            'name'=>'前端基础',
            'status'=>1,
            'level'=>1,
            'pid'=>0,
            'sort'=>2,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $pid2 = DB::table('blog_article_types')->insertGetId([
            'project_id'=>1,
            'admin_id'=>1,
            'type'=>1,
            'name'=>'HTML',
            'status'=>1,
            'level'=>2,
            'pid'=>$pid1,
            'sort'=>1,
            'created_at'=>date('Y-m-d H:i:s'),
            'image_id'=>$html,
            'description'=>'HTML的英文全称是 Hyper Text Markup Language，即超文本标记语言。',
            'seo_title'=>'HTML',
            'seo_keyword'=>'HTML',
            'seo_description'=>'HTML的英文全称是 Hyper Text Markup Language，即超文本标记语言。'
        ]);


        $pid2 = DB::table('blog_article_types')->insertGetId([
            'project_id'=>1,
            'admin_id'=>1,
            'type'=>1,
            'name'=>'CSS',
            'status'=>1,
            'level'=>2,
            'pid'=>$pid1,
            'sort'=>2,
            'created_at'=>date('Y-m-d H:i:s'),
        ]);

        $pid2 = DB::table('blog_article_types')->insertGetId([
            'project_id'=>1,
            'admin_id'=>1,
            'type'=>1,
            'name'=>'JS',
            'status'=>1,
            'level'=>2,
            'pid'=>$pid1,
            'sort'=>3,
            'created_at'=>date('Y-m-d H:i:s'),
        ]);




        $pid1 = DB::table('blog_article_types')->insertGetId([
            'project_id'=>1,
            'admin_id'=>1,
            'type'=>1,
            'name'=>'前端进阶',
            'status'=>1,
            'level'=>1,
            'pid'=>0,
            'sort'=>3,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $pid2 = DB::table('blog_article_types')->insertGetId([
            'project_id'=>1,
            'admin_id'=>1,
            'type'=>1,
            'name'=>'VUE',
            'status'=>1,
            'level'=>2,
            'pid'=>$pid1,
            'sort'=>1,
            'created_at'=>date('Y-m-d H:i:s'),
        ]);


        $pid2 = DB::table('blog_article_types')->insertGetId([
            'project_id'=>1,
            'admin_id'=>1,
            'type'=>1,
            'name'=>'React',
            'status'=>1,
            'level'=>2,
            'pid'=>$pid1,
            'sort'=>2,
            'created_at'=>date('Y-m-d H:i:s'),
        ]);

        $pid2 = DB::table('blog_article_types')->insertGetId([
            'project_id'=>1,
            'admin_id'=>1,
            'type'=>1,
            'name'=>'Ts',
            'status'=>1,
            'level'=>2,
            'pid'=>$pid1,
            'sort'=>3,
            'created_at'=>date('Y-m-d H:i:s'),
        ]);




        $pid1 = DB::table('blog_article_types')->insertGetId([
            'project_id'=>1,
            'admin_id'=>1,
            'type'=>1,
            'name'=>'前端框架',
            'status'=>1,
            'level'=>1,
            'pid'=>0,
            'sort'=>4,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $pid2 = DB::table('blog_article_types')->insertGetId([
            'project_id'=>1,
            'admin_id'=>1,
            'type'=>1,
            'name'=>'element-puls',
            'status'=>1,
            'level'=>2,
            'pid'=>$pid1,
            'sort'=>1,
            'created_at'=>date('Y-m-d H:i:s'),
        ]);


        $pid2 = DB::table('blog_article_types')->insertGetId([
            'project_id'=>1,
            'admin_id'=>1,
            'type'=>1,
            'name'=>'ant design pro',
            'status'=>1,
            'level'=>2,
            'pid'=>$pid1,
            'sort'=>2,
            'created_at'=>date('Y-m-d H:i:s'),
        ]);

        $pid2 = DB::table('blog_article_types')->insertGetId([
            'project_id'=>1,
            'admin_id'=>1,
            'type'=>1,
            'name'=>'nuxt',
            'status'=>1,
            'level'=>2,
            'pid'=>$pid1,
            'sort'=>3,
            'created_at'=>date('Y-m-d H:i:s'),
        ]);

        $pid1 = DB::table('blog_article_types')->insertGetId([
            'project_id'=>1,
            'admin_id'=>1,
            'type'=>1,
            'name'=>'后端语言',
            'status'=>1,
            'level'=>1,
            'pid'=>0,
            'sort'=>5,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $pid2 = DB::table('blog_article_types')->insertGetId([
            'project_id'=>1,
            'admin_id'=>1,
            'type'=>1,
            'name'=>'PHP',
            'status'=>1,
            'level'=>2,
            'pid'=>$pid1,
            'sort'=>1,
            'created_at'=>date('Y-m-d H:i:s'),
        ]);


        $pid2 = DB::table('blog_article_types')->insertGetId([
            'project_id'=>1,
            'admin_id'=>1,
            'type'=>1,
            'name'=>'Java',
            'status'=>1,
            'level'=>2,
            'pid'=>$pid1,
            'sort'=>2,
            'created_at'=>date('Y-m-d H:i:s'),
        ]);


        $pid2 = DB::table('blog_article_types')->insertGetId([
            'project_id'=>1,
            'admin_id'=>1,
            'type'=>1,
            'name'=>'Go',
            'status'=>1,
            'level'=>2,
            'pid'=>$pid1,
            'sort'=>3,
            'created_at'=>date('Y-m-d H:i:s'),
        ]);

        $pid2 = DB::table('blog_article_types')->insertGetId([
            'project_id'=>1,
            'admin_id'=>1,
            'type'=>1,
            'name'=>'Python',
            'status'=>1,
            'level'=>2,
            'pid'=>$pid1,
            'sort'=>4,
            'created_at'=>date('Y-m-d H:i:s'),
        ]);


        $pid2 = DB::table('blog_article_types')->insertGetId([
            'project_id'=>1,
            'admin_id'=>1,
            'type'=>1,
            'name'=>'node.js',
            'status'=>1,
            'level'=>2,
            'pid'=>$pid1,
            'sort'=>5,
            'created_at'=>date('Y-m-d H:i:s'),
        ]);




        $pid1 = DB::table('blog_article_types')->insertGetId([
            'project_id'=>1,
            'admin_id'=>1,
            'type'=>1,
            'name'=>'后端框架进阶',
            'status'=>1,
            'level'=>1,
            'pid'=>0,
            'sort'=>6,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        $pid2 = DB::table('blog_article_types')->insertGetId([
            'project_id'=>1,
            'admin_id'=>1,
            'type'=>1,
            'name'=>'laravel',
            'status'=>1,
            'level'=>2,
            'pid'=>$pid1,
            'sort'=>1,
            'created_at'=>date('Y-m-d H:i:s'),
        ]);


        $pid2 = DB::table('blog_article_types')->insertGetId([
            'project_id'=>1,
            'admin_id'=>1,
            'type'=>1,
            'name'=>'Spring Boot',
            'status'=>1,
            'level'=>2,
            'pid'=>$pid1,
            'sort'=>2,
            'created_at'=>date('Y-m-d H:i:s'),
        ]);


        $pid2 = DB::table('blog_article_types')->insertGetId([
            'project_id'=>1,
            'admin_id'=>1,
            'type'=>1,
            'name'=>'Gin',
            'status'=>1,
            'level'=>2,
            'pid'=>$pid1,
            'sort'=>3,
            'created_at'=>date('Y-m-d H:i:s'),
        ]);

        $pid2 = DB::table('blog_article_types')->insertGetId([
            'project_id'=>1,
            'admin_id'=>1,
            'type'=>1,
            'name'=>'Django',
            'status'=>1,
            'level'=>2,
            'pid'=>$pid1,
            'sort'=>4,
            'created_at'=>date('Y-m-d H:i:s'),
        ]);


        $pid2 = DB::table('blog_article_types')->insertGetId([
            'project_id'=>1,
            'admin_id'=>1,
            'type'=>1,
            'name'=>'egg',
            'status'=>1,
            'level'=>2,
            'pid'=>$pid1,
            'sort'=>5,
            'created_at'=>date('Y-m-d H:i:s'),
        ]);

        $pid1 = DB::table('blog_article_types')->insertGetId([
            'project_id'=>1,
            'admin_id'=>1,
            'type'=>1,
            'name'=>'关于我',
            'status'=>1,
            'level'=>1,
            'pid'=>0,
            'sort'=>7,
            'attribute'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);


        DB::table('blog_articles')->insertGetId([
            'article_type_id'=>$pid1,
            'project_id'=>1,
            'admin_id'=>1,
            'name'=>'程序员大象',
            'keywords'=>'程序员大象”',
            'description'=>'我是程序员大象，很高兴在这里和大家分享一些有关软件开发相关的知识。',
            'content'=>'<p><span style="letter-spacing:2px"><span style="font-size:16px">我是程序员大象，一个90后草根程序员！16年入行。我一直潜心研究软件开发相关知识，一边工作一边积累经验，分享一些在开发过程中遇到的问题，以及解决方法。大家也可以叫我“大象”。起初我把大多数时间都用在了前端开发上，我喜欢这种写一行代码就可以看到结果的成就感，也是因为这样我深深的喜欢上了这个行业，前端是我开发的起点，我先后学习了HTML、CSS、JS、VUE、React、TS等前端开发相关知识。当然在学习前端的同时也在学一些后端的知识，先后学习了PHP、Java、Go、Python、node.js等等后端语言。</span></span></p><p></p><p><span style="letter-spacing:2px"><span style="font-size:16px">目前我的工作是做后端开发，当然是也写前端代码，我并不喜欢以前端或后端的方式去定义一个软件开发者，因为它们是相互依赖的。没有后端的前端只是空壳，没有前端的后端就像风一样无法用视觉感知。</span></span></p><p></p><p><span style="letter-spacing:2px"><span style="font-size:16px">我除了日常的工作以外还写了一些开源项目，比如:<a href="https://gitee.com/song-bo/lva-cms2.0" target="_blank">LvaCMS</a>、<a href="https://gitee.com/song-bo/back-end-of-ancient-poetry" target="_blank">优咪乐古诗词2.0</a>。希望能以这种方式和大家多多交流，相互学习。当然大家也可以加我的微信（songbo236589），最后祝大家代码无bug！</span></span></p>',
            'status'=>1,
            'open'=>1,
            'sort'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        DB::table('blog_links')->insert([
            'project_id'=>1,
            'admin_id'=>1,
            'name'=>'咪乐多',
            'url'=>'https://www.lvacms.cn',
            'status'=>1,
            'sort'=>7,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
    }
}
