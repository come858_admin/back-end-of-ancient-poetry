<?php

//use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Jialeo\LaravelSchemaExtend\Schema;
class CreateBlogArticleTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blog_article_types', function (Blueprint $table) {
            $table->comment = '文章分类表';
            $table->increments('id')->comment('分类ID');
            $table->integer('project_id')->comment('项目ID');
            $table->integer('admin_id')->comment('管理员ID');
            $table->tinyInteger('type')->default(1)->comment('分类类型:1=文字列表,2=图片列表,3=下载列表,4=咨询列表');
            $table->string('name',100)->default('')->comment('分类名称');
            $table->integer('image_id')->nullable()->comment('分类图片');
            $table->string('description')->nullable()->default('')->comment('分类描述');
            $table->tinyInteger('status')->default(1)->comment('状态:0=隐藏,1=显示');
            $table->tinyInteger('level')->default(1)->comment('级别');
            $table->integer('pid')->default(0)->comment('父级ID');
            $table->integer('sort')->default(1)->comment('排序');

            $table->tinyInteger('open_type')->default(0)->comment('打开方式:0=原页面打开,1=新页面打开');
            $table->tinyInteger('attribute')->default(0)->comment('栏目属性:0=列表菜单,1=单页菜单,2=外链菜单');

            $table->string('external_link')->nullable()->default('')->comment('外链网址');

            $table->string('seo_title')->nullable()->default('')->comment('Title');
            $table->string('seo_keyword')->nullable()->default('')->comment('Meta Keywords');
            $table->string('seo_description')->nullable()->default('')->comment('Meta Description');

            $table->timestamp('created_at')->nullable()->comment('创建时间');
            $table->timestamp('updated_at')->nullable()->comment('更新时间');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blog_article_types');
    }
}
