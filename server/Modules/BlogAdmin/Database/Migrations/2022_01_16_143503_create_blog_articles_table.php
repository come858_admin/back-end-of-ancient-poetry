<?php

//use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Jialeo\LaravelSchemaExtend\Schema;
class CreateBlogArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blog_articles', function (Blueprint $table) {
            $table->comment = '文章表';
            $table->increments('id')->comment('文章ID');
            $table->integer('article_type_id')->comment('文章类型ID');
            $table->integer('project_id')->comment('项目ID');
            $table->integer('admin_id')->comment('管理员ID');
            $table->string('name',100)->default('')->comment('文章名称');
            $table->string('download_url',100)->nullable()->default('')->comment('下载地址');
            $table->string('download_key',100)->nullable()->default('')->comment('下载秘钥');
            $table->integer('image_id')->nullable()->comment('文章图片');
            $table->string('keywords')->nullable()->default('')->comment('文章关键词');
            $table->string('description')->nullable()->default('')->comment('文章描述');
            $table->longtext('content')->nullable()->comment('文章详情');
            $table->tinyInteger('status')->default(1)->comment('状态:0=隐藏,1=显示');
            $table->tinyInteger('open')->default(0)->comment('推荐首页:0=否,1=是');
            $table->integer('sort')->default(1)->comment('排序');
            $table->timestamp('created_at')->nullable()->comment('创建时间');
            $table->timestamp('updated_at')->nullable()->comment('更新时间');
        });
        Schema::create('blog_labels', function (Blueprint $table) {
            $table->comment = '标签表';
            $table->increments('id')->comment('标签ID');
            $table->integer('project_id')->nullable()->comment('项目ID');
            $table->integer('admin_id')->comment('管理员ID');
            $table->string('name',100)->default('')->comment('标签名称');
            $table->integer('sort')->default(1)->comment('排序');
            $table->tinyInteger('status')->default(1)->comment('状态:0=禁用,1=启用');
            $table->timestamp('created_at')->nullable()->comment('创建时间');
            $table->timestamp('updated_at')->nullable()->comment('更新时间');
        });
        Schema::create("blog_article_labels", function(Blueprint $table){
            $table->comment = '文章标签关联表';
            $table->increments('id')->comment('文章标签关联ID');
            $table->integer('project_id')->nullable()->comment('项目ID');
            $table->integer('admin_id')->comment('管理员ID');
            $table->integer("article_id")->comment('文章ID');
            $table->integer("label_id")->comment('标签ID');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blog_articles');
        Schema::dropIfExists('blog_labels');
        Schema::dropIfExists('blog_article_labels');
    }
}
