<?php
// +----------------------------------------------------------------------
// | Name: 咪乐多管理系统 [ 为了快速搭建软件应用而生的，希望能够帮助到大家提高开发效率。 ]
// +----------------------------------------------------------------------
// | Copyright: (c) 2020~2022 https://www.lvacms.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed: 这是一个自由软件，允许对程序代码进行修改，但希望您留下原有的注释。
// +----------------------------------------------------------------------
// | Author: 西安咪乐多软件 <997786358@qq.com>
// +----------------------------------------------------------------------
// | Version: V1
// +----------------------------------------------------------------------

/**
 * @Name
 * @Description
 * @Auther 西安咪乐多软件
 * @Date 2022/1/26 17:25
 */

namespace Modules\BlogAdmin\Services\Project;


use Modules\Admin\Models\AdminImage;
use Modules\BlogAdmin\Models\AdminProject;
use Modules\BlogAdmin\Services\BaseApiServices;
use Modules\Common\Exceptions\MessageData;

class ProjectServices extends BaseApiServices
{
    public function index(){
        $id = $this->getProjectId();
        $data = AdminProject::query()->with([
            'logoImage'=>function($query){
                $query->select('id','url','open');
            },
            'icoImage'=>function($query){
                $query->select('id','url','open');
            }
        ])->select( "name", "logo_id", "ico_id", "url", "description", "keywords", "status","ext")->find($id)?:$this->apiError(MessageData::GET_API_ERROR);
        $data = $data->toArray();
        $data['gzh_image_to'] = null;
        $data['wx_image_to'] = null;
        $data['about'] = "";
        $data['statement'] = "";
        $data['gzh_image_id'] = null;
        $data['wx_image_id'] = null;

        if($data['ext']){
            $ext = self::unSerialize($data['ext']);
            if(isset($ext['gzh_image_id']) && $ext['gzh_image_id']>0){
                $data['gzh_image_to'] = AdminImage::query()->select('id','url','open')->find($ext['gzh_image_id'])?:null;
            }
            $data['gzh_image_id'] = isset($ext['gzh_image_id'])?$ext['gzh_image_id']:null;
            if(isset($ext['wx_image_id']) && $ext['gzh_image_id']>0){
                $data['wx_image_to'] = AdminImage::query()->select('id','url','open')->find($ext['wx_image_id'])?:null;
            }
            $data['wx_image_id'] = isset($ext['wx_image_id'])?$ext['wx_image_id']:null;
            if(isset($ext['about'])){
                $data['about'] = $this->getReplacePicUrl($ext['about']);
            }
            $data['statement'] = isset($ext['statement'])?$ext['statement']:"";
            $data['title'] = isset($ext['title'])?$ext['title']:"";
            $data['icp'] = isset($ext['icp'])?$ext['icp']:"";
            $data['header'] = isset($ext['header'])?$ext['header']:"";
            $data['footer'] = isset($ext['footer'])?$ext['footer']:"";
            $data['general_embody'] = isset($ext['general_embody'])?$ext['general_embody']:"";
            $data['quick_embody'] = isset($ext['quick_embody'])?$ext['quick_embody']:"";
            $data['robots'] = isset($ext['robots'])?$ext['robots']:"";

            $data['network_name'] = isset($ext['network_name'])?$ext['network_name']:"";
            $data['occupation'] = isset($ext['occupation'])?$ext['occupation']:"";
            $data['current_residence'] = isset($ext['current_residence'])?$ext['current_residence']:"";
            $data['email'] = isset($ext['email'])?$ext['email']:"";
            $data['qq'] = isset($ext['qq'])?$ext['qq']:"";
            $data['wx'] = isset($ext['wx'])?$ext['wx']:"";
            $data['reprint_statement'] = isset($ext['reprint_statement'])?$ext['reprint_statement']:"";

            $data['station_establishment_time'] = isset($ext['station_establishment_time'])?$ext['station_establishment_time']:"";
            $data['website_program'] = isset($ext['website_program'])?$ext['website_program']:"";
            $data['website_program_url'] = isset($ext['website_program_url'])?$ext['website_program_url']:"";
            $data['baidu_propelling_movement_token'] = isset($ext['baidu_propelling_movement_token'])?$ext['baidu_propelling_movement_token']:"";
            $data['baidu_propelling_movement_site'] = isset($ext['baidu_propelling_movement_site'])?$ext['baidu_propelling_movement_site']:"";
            $data['baidu_statistics_url'] = isset($ext['baidu_statistics_url'])?$ext['baidu_statistics_url']:"";
        }
        return $this->apiSuccess(MessageData::GET_API_SUCCESS,$data);
    }
    public function update(array $data){
        $id = $this->getProjectId();
        if(isset($data['about']) && $data['about']){
            $data['about'] = $this->setPicUrl($data['about']);
        }
        $data['ext'] = serialize([
            "gzh_image_id" => isset($data["gzh_image_id"])?$data["gzh_image_id"]:null,
            "wx_image_id" => isset($data["wx_image_id"])?$data["wx_image_id"]:null,
            "about"=>isset($data["about"])?$data["about"]:null,
            "statement"=>isset($data["statement"])?$data["statement"]:null,
            "title"=>isset($data["icp"])?$data["title"]:null,
            "icp"=>isset($data["icp"])?$data["icp"]:null,
            "header"=>isset($data["header"])?$data["header"]:null,
            "footer"=>isset($data["footer"])?$data["footer"]:null,
            "general_embody"=>isset($data["general_embody"])?$data["general_embody"]:null,
            "quick_embody"=>isset($data["quick_embody"])?$data["quick_embody"]:null,
            "robots"=>isset($data["robots"])?$data["robots"]:null,
            "network_name"=>isset($data["network_name"])?$data["network_name"]:null,
            "occupation"=>isset($data["occupation"])?$data["occupation"]:null,
            "current_residence"=>isset($data["current_residence"])?$data["current_residence"]:null,
            "email"=>isset($data["email"])?$data["email"]:null,
            "qq"=>isset($data["qq"])?$data["qq"]:null,
            "wx"=>isset($data["wx"])?$data["wx"]:null,
            "reprint_statement"=>isset($data["reprint_statement"])?$data["reprint_statement"]:null,
            "station_establishment_time"=>isset($data["station_establishment_time"])?$data["station_establishment_time"]:null,
            "website_program"=>isset($data["website_program"])?$data["website_program"]:null,
            "website_program_url"=>isset($data["website_program_url"])?$data["website_program_url"]:null,
            "baidu_propelling_movement_token"=>isset($data["baidu_propelling_movement_token"])?$data["baidu_propelling_movement_token"]:null,
            "baidu_propelling_movement_site"=>isset($data["baidu_propelling_movement_site"])?$data["baidu_propelling_movement_site"]:null,
            "baidu_statistics_url"=>isset($data["baidu_statistics_url"])?$data["baidu_statistics_url"]:null,
        ]);
        unset($data["gzh_image_id"],$data["wx_image_id"],$data["about"],$data["statement"],$data["icp"],$data["header"],$data["footer"],$data["footer"],$data["general_embody"],$data["quick_embody"],$data["robots"],$data["title"],$data["network_name"],$data["occupation"],$data["current_residence"],$data["email"],$data["qq"],$data["wx"],$data["reprint_statement"],$data["station_establishment_time"],$data["website_program"],$data["website_program_url"],$data["baidu_propelling_movement_token"],$data["baidu_propelling_movement_site"],$data["baidu_statistics_url"]);
        return $this->commonUpdate(AdminProject::query(),$id,$data);
    }
}
