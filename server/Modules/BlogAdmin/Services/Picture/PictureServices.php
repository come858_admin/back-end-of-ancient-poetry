<?php
// +----------------------------------------------------------------------
// | Name: 咪乐多管理系统 [ 为了快速搭建软件应用而生的，希望能够帮助到大家提高开发效率。 ]
// +----------------------------------------------------------------------
// | Copyright: (c) 2020~2021 https://www.lvacms.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed: 这是一个自由软件，允许对程序代码进行修改，但希望您留下原有的注释。
// +----------------------------------------------------------------------
// | Author: 西安咪乐多软件 <997786358@qq.com>
// +----------------------------------------------------------------------
// | Version: V1
// +----------------------------------------------------------------------

/**
 * @Name 图片管理服务
 * @Description
 * @Auther 西安咪乐多软件
 * @Date 2021/11/20 19:06
 */

namespace Modules\BlogAdmin\Services\Picture;

use Modules\BlogAdmin\Models\BlogPicture;
use Modules\BlogAdmin\Services\BaseApiServices;
use Modules\Common\Exceptions\MessageData;

class PictureServices extends BaseApiServices
{
    public function index(array $data){
        $model = BlogPicture::query();
        $model = $this->setWhereQueryProject($model);
        $model = $this->queryCondition($model,$data,"content");
        if(isset($data['sort'])){
            $model = $this->querySort($model,$data['sort']);
        }
        if(isset($data['type']) && $data['type'] > 0){
            $model = $model->where('type','=',$data['type']);
        }
        $list = $model->with([
            'imageTo'=>function($query){
                $query->select('id','url','open');
            }
        ])->orderBy('sort','asc')->orderBy('id','desc')
            ->paginate($data['limit'])->toArray();
        return $this->apiSuccess('',[
            'list'=>$list['data'],
            'total'=>$list['total']
        ]);
    }


    public function add(array $data){
        $data = $this->getCommonId($data);
        return $this->commonCreate(BlogPicture::query(),$data);
    }

    public function edit(int $id){
        $data = BlogPicture::query()->with([
            'imageTo'=>function($query){
                $query->select('id','url','open');
            }
        ])->select( "id","content","url","image_id","type", "status","sort")->find($id)?:$this->apiError(MessageData::GET_API_ERROR);
        $data = $data->toArray();
        return $this->apiSuccess(MessageData::GET_API_SUCCESS,$data);
    }

    public function update(int $id,array $data){
        return $this->commonUpdate(BlogPicture::query(),$id,$data);
    }
    public function status(int $id,array $data){
        return $this->commonStatusUpdate(BlogPicture::query(),$id,$data);
    }
    public function sorts(int $id,array $data){
        return $this->commonSortsUpdate(BlogPicture::query(),$id,$data);
    }
    public function del(int $id){
        return $this->commonDestroy(BlogPicture::query(),['id'=>$id]);
    }
    public function delAll(array $idArr){
        return $this->commonDestroy(BlogPicture::query(),$idArr);
    }
}
