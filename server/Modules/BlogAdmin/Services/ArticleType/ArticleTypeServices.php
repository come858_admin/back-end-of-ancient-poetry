<?php
// +----------------------------------------------------------------------
// | Name: 咪乐多管理系统 [ 为了快速搭建软件应用而生的，希望能够帮助到大家提高开发效率。 ]
// +----------------------------------------------------------------------
// | Copyright: (c) 2020~2022 https://www.lvacms.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed: 这是一个自由软件，允许对程序代码进行修改，但希望您留下原有的注释。
// +----------------------------------------------------------------------
// | Author: 西安咪乐多软件 <997786358@qq.com>
// +----------------------------------------------------------------------
// | Version: V1
// +----------------------------------------------------------------------

/**
 * @Name
 * @Description
 * @Auther 西安咪乐多软件
 * @Date 2022/1/15 16:33
 */

namespace Modules\BlogAdmin\Services\ArticleType;

use Modules\BlogAdmin\Models\BlogArticleType;
use Modules\BlogAdmin\Services\BaseApiServices;
use Modules\Common\Exceptions\MessageData;

class ArticleTypeServices extends BaseApiServices
{

    public function getTypeList(){
        $list = BlogArticleType::query()->select('id','name','pid','open_type','attribute')->orderBy('sort','asc')->orderBy('id','desc')->get()->toArray();
        return $this->apiSuccess('',$this->tree($list));
    }
    public function tree(array $array,int $pid=0):Array
    {
        $tree = array();
        foreach ($array as $key => $value) {
            if ($value['pid'] == $pid) {
                $value['value'] = $value['id'];
                $value['key'] = strval($value['id']);
                $value['title'] = $value['name'] . "（" . $value['open_type_value'] . "，" . $value['attribute_value'] . "）";
                $value['children'] = $this->tree($array, $value['id']);
                if (!$value['children']) {
                    unset($value['children']);
                }
                $tree[] = $value;
            }
        }
        return $tree;
    }

    public function getPidList($id){
        $list = BlogArticleType::query();
        if($id){
            $list = $list->where('id','!=',$id);
        }
        $list = $list->select('id','name','pid','open_type','attribute')->get()->toArray();
        $list = $this->tree($list);
        $list = array_merge([['value'=>0,'title'=>'默认顶级分类']],$list);
        return $this->apiSuccess('',$list);
    }
    public function index($data){
        $model = BlogArticleType::query();
        $model = $this->setWhereQueryProject($model);
        if(isset($data['sort'])){
            $model = $this->querySort($model,$data['sort']);
        }else{
            $model = $model->orderBy('sort','asc');
        }
        $list = $model->select('id','type','name','image_id','status','pid','sort','open_type','attribute','external_link','created_at','updated_at')->with([
            'imageTo'=>function($query){
                $query->select('id','url','open');
            }
        ])->orderBy('sort','asc')->orderBy('id','desc')
            ->get()->toArray();
        return $this->apiSuccess('',$this->tree($list));
    }

    public function add(array $data){
        if($data['pid'] == 0){
            $data['level'] = 1;
        }else{
            $data['level'] = BlogArticleType::where(['id'=>$data['pid']])->value('level') + 1;
        }
        $data = $this->getCommonId($data);
        return $this->commonCreate(BlogArticleType::query(),$data);
    }

    public function edit(int $id){
        $data = BlogArticleType::query()->with([
            'imageTo'=>function($query){
                $query->select('id','url','open');
            }
        ])->find($id)?:$this->apiError(BlogArticleType::GET_API_ERROR);
        $data = $data->toArray();
        return $this->apiSuccess(MessageData::GET_API_SUCCESS,$data);
    }

    public function update(int $id,array $data){
        return $this->commonUpdate(BlogArticleType::query(),$id,$data);
    }

    public function status(int $id,array $data){
        return $this->commonStatusUpdate(BlogArticleType::query(),$id,$data);
    }

    public function sorts(int $id,array $data){
        return $this->commonSortsUpdate(BlogArticleType::query(),$id,$data);
    }

    public function del(int $id){
        $idArr = $this->getDelIdArr($id);
        return $this->commonDestroy(BlogArticleType::query(),$idArr);
    }

    public  function getDelIdArr(int $id){
        $ruleList = BlogArticleType::query()->select('id','pid','open_type','attribute')->get()->toArray();
        $arr = $this->delSort($ruleList,$id);
        $arr[] = $id;
        return $arr;
    }

    private function delSort(array $ruleList,int $id)
    {
        static $arr = [];
        foreach ($ruleList as $k=>$v){
            if($v['pid'] == $id){
                $arr[] = $v['id'];
                unset($ruleList[$k]);
                return $this->delSort($ruleList,$v['id']);
            }
        }
        return $arr;
    }

    public  function getIdArr(int $id){
        $list = BlogArticleType::query()->select('id','pid','open_type','attribute')->get()->toArray();
        $arr = $this->idSort($list,$id);
        $arr[] = $id;
        return $arr;
    }

    private function idSort(array $list,int $id)
    {
        static $arr = [];
        foreach ($list as $k=>$v){
            if($v['id'] == $id){
                if($v['pid'] == 0){
                    return $arr;
                }
                $arr[] = $v['pid'];
                unset($list[$k]);
                return $this->idSort($list,$v['pid']);
            }
        }
        return $arr;
    }
}
