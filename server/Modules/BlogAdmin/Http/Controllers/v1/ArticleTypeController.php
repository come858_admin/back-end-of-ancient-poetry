<?php
// +----------------------------------------------------------------------
// | Name: 咪乐多管理系统 [ 为了快速搭建软件应用而生的，希望能够帮助到大家提高开发效率。 ]
// +----------------------------------------------------------------------
// | Copyright: (c) 2020~2022 https://www.lvacms.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed: 这是一个自由软件，允许对程序代码进行修改，但希望您留下原有的注释。
// +----------------------------------------------------------------------
// | Author: 西安咪乐多软件 <997786358@qq.com>
// +----------------------------------------------------------------------
// | Version: V1
// +----------------------------------------------------------------------


namespace Modules\BlogAdmin\Http\Controllers\v1;


use Illuminate\Http\Request;
use Modules\BlogAdmin\Http\Controllers\BaseApiControllers;
use Modules\BlogAdmin\Services\ArticleType\ArticleTypeServices;
use Modules\Common\Requests\CommonSortRequest;
use Modules\Common\Requests\CommonStatusRequest;

class ArticleTypeController extends BaseApiControllers
{
    public function index(Request $request){
        return (new ArticleTypeServices())->index($request->only(['sort']));
    }
    public function add(Request $request)
    {
        return (new ArticleTypeServices())->add($request->only([
            'type',
            'name',
            'image_id',
            'description',
            "status",
            "pid",
            "sort",
            "open_type",
            "attribute",
            "external_link",
            "seo_title",
            "seo_keyword",
            "seo_description",
        ]));
    }
    public function edit(int $id){
        return (new ArticleTypeServices())->edit($id);
    }
    public function update(Request $request,int $id)
    {
        return (new ArticleTypeServices())->update($id,$request->only([
            'type',
            'name',
            'image_id',
            'description',
            "status",
            "level",
            "pid",
            "sort",
            "open_type",
            "attribute",
            "external_link",
            "seo_title",
            "seo_keyword",
            "seo_description",

        ]));
    }
    public function status(CommonStatusRequest $request,int $id){
        return (new ArticleTypeServices())->status($id,$request->only([
            "status"
        ]));
    }
    public function sorts(CommonSortRequest $request,int $id){
        return (new ArticleTypeServices())->sorts($id,$request->only([
            "sort"
        ]));
    }
    public function getPidList(Request $request){
        return (new ArticleTypeServices())->getPidList($request->get('id'));
    }

    public function del(int $id){
        return (new ArticleTypeServices())->del($id);
    }
}
