<?php
// +----------------------------------------------------------------------
// | Name: 咪乐多管理系统 [ 为了快速搭建软件应用而生的，希望能够帮助到大家提高开发效率。 ]
// +----------------------------------------------------------------------
// | Copyright: (c) 2020~2021 https://www.lvacms.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed: 这是一个自由软件，允许对程序代码进行修改，但希望您留下原有的注释。
// +----------------------------------------------------------------------
// | Author: 西安咪乐多软件 <997786358@qq.com>
// +----------------------------------------------------------------------
// | Version: V1
// +----------------------------------------------------------------------



namespace Modules\BlogAdmin\Http\Controllers\v1;


use Illuminate\Http\Request;
use Modules\Admin\Http\Controllers\BaseApiController;
use Modules\BlogAdmin\Services\Link\LinkServices;
use Modules\Common\Requests\CommonIdArrRequest;
use Modules\Common\Requests\CommonPageRequest;
use Modules\Common\Requests\CommonSortRequest;
use Modules\Common\Requests\CommonStatusRequest;

class LinkController extends BaseApiController
{

    public function index(CommonPageRequest $request){
        return (new LinkServices())->index($request->only([
            "limit",
            "name",
            "created_at",
            "updated_at",
            "status",
            "sort"
        ]));
    }


    public function add(Request $request)
    {
        return (new LinkServices())->add($request->only([
            "name",
            "url",
            "image_id",
            "sort",
            "status"
        ]));
    }


    public function edit(int $id){
        return (new LinkServices())->edit($id);
    }

    public function update(Request $request,int $id)
    {
        return (new LinkServices())->update($id,$request->only([
            "name",
            "url",
            "image_id",
            "sort",
            "status"
        ]));
    }


    public function status(CommonStatusRequest $request,int $id){
        return (new LinkServices())->status($id,$request->only([
            "status"
        ]));
    }

    public function sorts(CommonSortRequest $request,int $id){
        return (new LinkServices())->sorts($id,$request->only([
            "sort"
        ]));
    }
    public function del(int $id){
        return (new LinkServices())->del($id);
    }

    public function delAll(CommonIdArrRequest $request)
    {
        return (new LinkServices())->delAll($request->get('idArr'));
    }
}
