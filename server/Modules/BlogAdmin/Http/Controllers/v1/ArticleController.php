<?php
// +----------------------------------------------------------------------
// | Name: 咪乐多管理系统 [ 为了快速搭建软件应用而生的，希望能够帮助到大家提高开发效率。 ]
// +----------------------------------------------------------------------
// | Copyright: (c) 2020~2021 https://www.lvacms.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed: 这是一个自由软件，允许对程序代码进行修改，但希望您留下原有的注释。
// +----------------------------------------------------------------------
// | Author: 西安咪乐多软件 <997786358@qq.com>
// +----------------------------------------------------------------------
// | Version: V1
// +----------------------------------------------------------------------



namespace Modules\BlogAdmin\Http\Controllers\v1;


use Illuminate\Http\Request;
use Modules\Admin\Http\Controllers\BaseApiController;
use Modules\BlogAdmin\Services\Article\ArticleServices;
use Modules\BlogAdmin\Services\ArticleType\ArticleTypeServices;
use Modules\Common\Requests\CommonIdArrRequest;
use Modules\Common\Requests\CommonPageRequest;
use Modules\Common\Requests\CommonSortRequest;
use Modules\Common\Requests\CommonStatusRequest;

class ArticleController extends BaseApiController
{

    public function index(CommonPageRequest $request){
        return (new ArticleServices())->index($request->only([
            "limit",
            "name",
            "created_at",
            "updated_at",
            "status",
            "open",
            "sort",
            "article_type_id"
        ]));
    }


    public function add(Request $request)
    {
        return (new ArticleServices())->add($request->only([
            "article_type_id",
            "name",
            "download_url",
            "download_key",
            "image_id",
            "keywords",
            "description",
            "content",
            "status",
            "sort",
            "labelArr",
            "open",
        ]));
    }


    public function edit(int $id){
        return (new ArticleServices())->edit($id);
    }

    public function update(Request $request,int $id)
    {
        return (new ArticleServices())->update($id,$request->only([
            "article_type_id",
            "name",
            "download_url",
            "download_key",
            "image_id",
            "keywords",
            "description",
            "content",
            "status",
            "sort",
            "labelArr",
            "open",
        ]));
    }

    public function open(Request $request,int $id){
        return (new ArticleServices())->open($id,$request->only([
            "open"
        ]));
    }



    public function status(CommonStatusRequest $request,int $id){
        return (new ArticleServices())->status($id,$request->only([
            "status"
        ]));
    }

    public function sorts(CommonSortRequest $request,int $id){
        return (new ArticleServices())->sorts($id,$request->only([
            "sort"
        ]));
    }
    public function del(int $id){
        return (new ArticleServices())->del($id);
    }

    public function delAll(CommonIdArrRequest $request)
    {
        return (new ArticleServices())->delAll($request->get('idArr'));
    }
    public function getTypeList()
    {
        return (new ArticleTypeServices())->getTypeList();
    }
    public function getLabelList(Request $request){
        return (new ArticleServices())->getLabelList($request->get('name'));
    }


    public function propellingMovementAll(Request $request){
        return (new ArticleServices())->propellingMovementAll($request->get('idArr'));
    }
}
