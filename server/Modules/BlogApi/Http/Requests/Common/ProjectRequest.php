<?php

namespace Modules\BlogApi\Http\Requests\Common;

use Illuminate\Foundation\Http\FormRequest;

class ProjectRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "project_id"            =>'required|is_positive_integer',
        ];
    }

    public function messages()
    {
        return [
            'project_id.required'                 =>'缺少参数project_id',
            'project_id.is_positive_integer'      =>'project_id参数格式错误',
        ];
    }
}
