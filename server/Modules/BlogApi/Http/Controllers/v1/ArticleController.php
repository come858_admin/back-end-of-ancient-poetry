<?php
// +----------------------------------------------------------------------
// | Name: 咪乐多管理系统 [ 为了快速搭建软件应用而生的，希望能够帮助到大家提高开发效率。 ]
// +----------------------------------------------------------------------
// | Copyright: (c) 2020~2021 https://www.lvacms.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed: 这是一个自由软件，允许对程序代码进行修改，但希望您留下原有的注释。
// +----------------------------------------------------------------------
// | Author: 西安咪乐多软件 <997786358@qq.com>
// +----------------------------------------------------------------------
// | Version: V1
// +----------------------------------------------------------------------

/**
 * @Name  项目配置
 * @Description
 * @Auther 西安咪乐多软件
 * @Date 2021/11/20 17:43
 */

namespace Modules\BlogApi\Http\Controllers\v1;
use Modules\BlogApi\Http\Controllers\BaseApiControllers;
use Modules\BlogApi\Http\Requests\Common\PageRequest;
use Modules\BlogApi\Http\Requests\Common\ProjectIdRequest;
use Modules\BlogApi\Services\Article\ArticleServices;

class ArticleController extends BaseApiControllers
{
    public function list(PageRequest $request){
        return (new ArticleServices())->list($request->only([
            'project_id',
            'limit',
            'name',
            'article_type_id',
            'label_id',
            'open'
        ]));
    }

    public function details(ProjectIdRequest $request){
        return (new ArticleServices())->details($request->only([
            'project_id',
            'id'
        ]));
    }
}
