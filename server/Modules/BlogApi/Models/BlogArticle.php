<?php
// +----------------------------------------------------------------------
// | Name: 咪乐多管理系统 [ 为了快速搭建软件应用而生的，希望能够帮助到大家提高开发效率。 ]
// +----------------------------------------------------------------------
// | Copyright: (c) 2020~2021 https://www.lvacms.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed: 这是一个自由软件，允许对程序代码进行修改，但希望您留下原有的注释。
// +----------------------------------------------------------------------
// | Author: 西安咪乐多软件 <997786358@qq.com>
// +----------------------------------------------------------------------
// | Version: V1
// +----------------------------------------------------------------------

/**
 * @Name 文章模型
 * @Description
 * @Auther 西安咪乐多软件
 * @Date 2021/11/20 19:03
 */

namespace Modules\BlogApi\Models;


use Modules\Admin\Models\AdminImage;

class BlogArticle extends BaseApiModel
{

    protected $appends = ['date_value'];
    public function getDateValueAttribute()
    {
        return $this->formatTime(strtotime($this->updated_at?$this->updated_at:$this->created_at));
    }
    public function imageTo()
    {
        return $this->belongsTo(AdminImage::class,'image_id','id');
    }
    public function articleTo()
    {
        return $this->belongsTo(BlogArticleType::class,'article_type_id','id');
    }
    public function labelTo()
    {
        return $this->belongsToMany('Modules\BlogApi\Models\BlogLabel', 'blog_article_labels', 'article_id', 'label_id')->withPivot(['article_id', 'label_id']);
    }


}
