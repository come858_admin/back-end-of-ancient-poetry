<?php
// +----------------------------------------------------------------------
// | Name: 咪乐多管理系统 [ 为了快速搭建软件应用而生的，希望能够帮助到大家提高开发效率。 ]
// +----------------------------------------------------------------------
// | Copyright: (c) 2020~2022 https://www.lvacms.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed: 这是一个自由软件，允许对程序代码进行修改，但希望您留下原有的注释。
// +----------------------------------------------------------------------
// | Author: 西安咪乐多软件 <997786358@qq.com>
// +----------------------------------------------------------------------
// | Version: V1
// +----------------------------------------------------------------------

/**
 * @Name
 * @Description
 * @Auther 西安咪乐多软件
 * @Date 2022/1/26 17:25
 */

namespace Modules\BlogApi\Services\ArticleType;
use Modules\BlogApi\Models\BlogArticleType;
use Modules\BlogApi\Services\BaseApiServices;
class ArticleTypeServices extends BaseApiServices
{
    public function tree(array $array,int $pid=0):Array
    {
        $tree = array();
        foreach ($array as $key => $value) {
            if ($value['pid'] == $pid) {
                $value['children'] = $this->tree($array, $value['id']);
                if (!$value['children']) {
                    unset($value['children']);
                }
                $tree[] = $value;
            }
        }
        return $tree;
    }
    public function list($project_id){
        $model = BlogArticleType::query();
        $list = $model->select('id','type','name','image_id','pid','open_type','attribute','external_link','seo_title','seo_keyword','seo_description')
            ->where([
                "status"=>1,
                "project_id"=>$project_id
            ])
            ->with([
            'imageTo'=>function($query){
                $query->select('id','url','open');
            }
        ])->orderBy('sort','asc')->orderBy('id','desc')
            ->get()->toArray();
        return $this->apiSuccess('',$this->tree($list));
    }
}
