<?php
// +----------------------------------------------------------------------
// | Name: 咪乐多管理系统 [ 为了快速搭建软件应用而生的，希望能够帮助到大家提高开发效率。 ]
// +----------------------------------------------------------------------
// | Copyright: (c) 2020~2022 https://www.lvacms.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed: 这是一个自由软件，允许对程序代码进行修改，但希望您留下原有的注释。
// +----------------------------------------------------------------------
// | Author: 西安咪乐多软件 <997786358@qq.com>
// +----------------------------------------------------------------------
// | Version: V1
// +----------------------------------------------------------------------

/**
 * @Name
 * @Description
 * @Auther 西安咪乐多软件
 * @Date 2022/1/26 17:25
 */

namespace Modules\BlogApi\Services\Article;
use Modules\BlogApi\Models\BlogArticle;
use Modules\BlogApi\Services\BaseApiServices;
use Modules\Common\Exceptions\MessageData;

class ArticleServices extends BaseApiServices
{
    public function list($data){
        $model = BlogArticle::query();
        if (!empty($data['name'])){
            $model = $model->where('name','like','%' . $data['name'] . '%');
        }
        if (isset($data['article_type_id']) && $data['article_type_id'] != '' && $data['article_type_id'] > 0){
            $model = $model->where('article_type_id',$data['article_type_id']);
        }
        if (isset($data['open']) && $data['open'] != ''){
            $model = $model->where('open',$data['open']);
        }
        if(isset($data['label_id']) && $data['label_id'] > 0){
            $model = $model->whereHas('labelTo',function($quest)use($data){
                $quest->whereIn('label_id',explode(',',$data['label_id']));
            });
        }
        $list = $model->select('id','name','description','keywords','image_id','article_type_id','created_at','updated_at')
            ->where([
                "status"=>1,
                "project_id"=>$data['project_id']
            ])
            ->with([
                'imageTo'=>function($query){
                    $query->select('id','url','open');
                },
                'articleTo'=>function($query){
                    $query->select('id','name');
                },
                'labelTo'
            ])
            ->orderBy('sort','asc')->orderBy('id','desc')
            ->paginate($data['limit'])->toArray();
        return $this->apiSuccess('',[
            'list'=>$list['data'],
            'total'=>$list['total']
        ]);
    }



    public function details($data){
        $data = BlogArticle::query()->select('id','name','description','keywords','image_id','article_type_id','content','created_at','updated_at','visits')
            ->where([
                "status"=>1,
                "project_id"=>$data['project_id']
            ])
            ->with([
                'imageTo'=>function($query){
                    $query->select('id','url','open');
                },
                'articleTo'=>function($query){
                    $query->select('id','name');
                },
                'labelTo'
            ])->find($data['id'])?:$this->apiError(MessageData::GET_API_ERROR);
        $data = $data->toArray();
        if($data['content']){
            $data['content'] = $this->getReplacePicUrl($data['content']);
        }
        BlogArticle::query()->where('id',$data['id'])->increment('visits');
        $data['s_info'] = BlogArticle::query()->where('id','<',$data['id'])->orderBy('id','desc')->first();
        if($data['s_info']){
            $data['s_info'] = $data['s_info']->toArray();
        }
        $data['x_info'] = BlogArticle::query()->where('id','>',$data['id'])->orderBy('id','asc')->first();
        if($data['x_info']){
            $data['x_info'] = $data['x_info']->toArray();
        }
        return $this->apiSuccess('',$data);
    }
}
