<?php

namespace Modules\ToolAdmin\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class ToolTableSeederTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /****************************工具管理******************************************/
        $pid1 = DB::table('admin_rules')->insertGetId([
            'name'=>'工具管理',
            'status'=>1,
            'auth_open'=>1,
            'path'=>'/toolAdmin',
            'pid'=>0,
            'level'=>1,
            'type'=>1,
            'sort'=>4,
            'icon'=>'Form',
            'created_at'=>date('Y-m-d H:i:s')
        ]);

        /****************************数据看板******************************************/
        $pid2 = DB::table('admin_rules')->insertGetId([
            'name'=>'数据看板',
            'path'=>'/toolAdmin/dashboard',
            'url'=>'./toolAdmin/dashboard/index',
            'status'=>1,
            'auth_open'=>1,
            'icon'=>'AreaChart',
            'pid'=>$pid1,
            'level'=>2,
            'type'=>3,
            'sort'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        /****************************配置管理******************************************/
        $pid2 = DB::table('admin_rules')->insertGetId([
            'name'=>'配置管理',
            'path'=>'/toolAdmin/setting',
            'status'=>1,
            'auth_open'=>1,
            'icon'=>'Setting',
            'pid'=>$pid1,
            'level'=>2,
            'type'=>2,
            'sort'=>2,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        /****************************项目配置******************************************/
        $pid3 = DB::table('admin_rules')->insertGetId([
            'name'=>'项目配置',
            'path'=>'/toolAdmin/setting/project',
            'url'=>'./toolAdmin/setting/project/index',
            'status'=>1,
            'auth_open'=>1,
            'icon'=>'CarryOut',
            'pid'=>$pid2,
            'level'=>3,
            'type'=>3,
            'sort'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        /****************************图片管理******************************************/
        $pid3 = DB::table('admin_rules')->insertGetId([
            'name'=>'图片管理',
            'path'=>'/toolAdmin/setting/picture',
            'url'=>'./toolAdmin/setting/picture/index',
            'status'=>1,
            'auth_open'=>1,
            'icon'=>'Picture',
            'pid'=>$pid2,
            'level'=>3,
            'type'=>3,
            'sort'=>21,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        /****************************用户管理******************************************/
        $pid2 = DB::table('admin_rules')->insertGetId([
            'name'=>'用户管理',
            'path'=>'/toolAdmin/user',
            'status'=>1,
            'auth_open'=>1,
            'icon'=>'User',
            'pid'=>$pid1,
            'level'=>2,
            'type'=>2,
            'sort'=>5,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        /****************************会员管理******************************************/
        $pid3 = DB::table('admin_rules')->insertGetId([
            'name'=>'会员管理',
            'path'=>'/toolAdmin/user/user',
            'url'=>'./toolAdmin/user/user/index',
            'status'=>1,
            'auth_open'=>1,
            'icon'=>'User',
            'pid'=>$pid2,
            'level'=>3,
            'type'=>3,
            'sort'=>1,
            'created_at'=>date('Y-m-d H:i:s')
        ]);
    }
}
