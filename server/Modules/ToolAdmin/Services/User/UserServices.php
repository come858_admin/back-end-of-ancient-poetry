<?php
// +----------------------------------------------------------------------
// | Name: 咪乐多管理系统 [ 为了快速搭建软件应用而生的，希望能够帮助到大家提高开发效率。 ]
// +----------------------------------------------------------------------
// | Copyright: (c) 2020~2021 https://www.lvacms.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed: 这是一个自由软件，允许对程序代码进行修改，但希望您留下原有的注释。
// +----------------------------------------------------------------------
// | Author: 西安咪乐多软件 <997786358@qq.com>
// +----------------------------------------------------------------------
// | Version: V1
// +----------------------------------------------------------------------

/**
 * @Name 服务
 * @Description
 * @Auther 西安咪乐多软件
 * @Date 2021/11/20 19:06
 */
namespace Modules\ToolAdmin\Services\User;
use Modules\ToolAdmin\Services\BaseApiServices;
use Modules\Common\Exceptions\MessageData;
use Modules\ToolAdmin\Models\ToolUser;

class UserServices extends BaseApiServices
{
    public function index(array $data)
    {
        $model = ToolUser::query();
        $model = $this->setWhereQueryProject($model);
        $model = $this->queryCondition($model, $data, "name");
        if (isset($data['sort'])) {
            $model = $this->querySort($model, $data['sort']);
        }
        if (isset($data['nick_name']) && $data['nick_name'] != ''){
            $model = $model->where('nick_name',$data['nick_name']);
        }
        if (isset($data['mobile']) && $data['mobile'] != ''){
            $model = $model->where('mobile',$data['mobile']);
        }
        if (isset($data['type']) && $data['type'] != ''){
            $model = $model->where('type',$data['type']);
        }
        if (isset($data['pid']) && $data['pid'] != ''){
            $model = $model->where('pid',$data['pid']);
        }
        $list = $model->orderBy('id', 'desc')
            ->paginate($data['limit'])->toArray();
        return $this->apiSuccess('', [
            'list' => $list['data'],
            'total' => $list['total']
        ]);
    }

    public function edit(int $id)
    {
        $data = ToolUser::query()->find($id)?:$this->apiError(MessageData::GET_API_ERROR);
        $data = $data->toArray();
        return $this->apiSuccess(MessageData::GET_API_SUCCESS,$data);
    }

    public function update(int $id,array $data)
    {
        return $this->commonUpdate(ToolUser::query(),$id,$data);
    }

    public function status(int $id,array $data)
    {
        return $this->commonStatusUpdate(ToolUser::query(),$id,$data);
    }

    public function del(int $id)
    {
        return $this->commonDestroy(ToolUser::query(),['id'=>$id]);
    }

    public function delAll(array $idArr)
    {
        return $this->commonDestroy(ToolUser::query(),$idArr);
    }
}
