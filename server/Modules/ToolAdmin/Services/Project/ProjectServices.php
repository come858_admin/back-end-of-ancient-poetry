<?php
// +----------------------------------------------------------------------
// | Name: 咪乐多管理系统 [ 为了快速搭建软件应用而生的，希望能够帮助到大家提高开发效率。 ]
// +----------------------------------------------------------------------
// | Copyright: (c) 2020~2022 https://www.lvacms.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed: 这是一个自由软件，允许对程序代码进行修改，但希望您留下原有的注释。
// +----------------------------------------------------------------------
// | Author: 西安咪乐多软件 <997786358@qq.com>
// +----------------------------------------------------------------------
// | Version: V1
// +----------------------------------------------------------------------

/**
 * @Name
 * @Description
 * @Auther 西安咪乐多软件
 * @Date 2022/1/26 17:25
 */

namespace Modules\ToolAdmin\Services\Project;
use Modules\ToolAdmin\Models\AdminProject;
use Modules\ToolAdmin\Services\BaseApiServices;
use Modules\Common\Exceptions\MessageData;

class ProjectServices extends BaseApiServices
{
    public function index(){
        $id = $this->getProjectId();
        $data = AdminProject::query()->with([
            'logoImage'=>function($query){
                $query->select('id','url','open');
            },
            'icoImage'=>function($query){
                $query->select('id','url','open');
            }
        ])->select( "name", "logo_id", "ico_id", "url", "description", "keywords", "status","ext")->find($id)?:$this->apiError(MessageData::GET_API_ERROR);

        $data['about'] = "";
        $data['gzh_app_id'] = "";
        $data['gzh_app_secret'] = "";
        $data['xcx_app_id'] = "";
        $data['xcx_app_secret'] = "";
        if($data['ext']){
            $ext = self::unSerialize($data['ext']);
            if(isset($ext['about'])){
                $data['about'] = $this->getReplacePicUrl($ext['about']);
            }
            $data['gzh_app_id'] = isset($ext['gzh_app_id'])?$ext['gzh_app_id']:"";
            $data['gzh_app_secret'] = isset($ext['gzh_app_secret'])?$ext['gzh_app_secret']:"";
            $data['xcx_app_id'] = isset($ext['xcx_app_id'])?$ext['xcx_app_id']:"";
            $data['xcx_app_secret'] = isset($ext['xcx_app_secret'])?$ext['xcx_app_secret']:"";
        }
        $data = $data->toArray();
        return $this->apiSuccess(MessageData::GET_API_SUCCESS,$data);
    }
    public function update(array $data){
        $id = $this->getProjectId();
        if(isset($data['about']) && $data['about']){
            $data['about'] = $this->setPicUrl($data['about']);
        }
        $data['ext'] = serialize([
            "about"=>isset($data["about"])?$data["about"]:null,
            "gzh_app_id" => isset($data["gzh_app_id"])?$data["gzh_app_id"]:null,
            "gzh_app_secret" => isset($data["gzh_app_secret"])?$data["gzh_app_secret"]:null,
            "xcx_app_id" => isset($data["xcx_app_id"])?$data["xcx_app_id"]:null,
            "xcx_app_secret" => isset($data["xcx_app_secret"])?$data["xcx_app_secret"]:null,
        ]);
        unset($data["about"],$data["gzh_app_id"],$data["gzh_app_secret"],$data["xcx_app_id"],$data["xcx_app_secret"]);
        return $this->commonUpdate(AdminProject::query(),$id,$data);
    }


    public function getAboutUs($data){
        $about = "";
        $ext = AdminProject::query()->where(['id'=>$data['project_id']])->value( "ext");
        if($ext){
            $ext = self::unSerialize($ext);
            if(isset($ext['about'])){
                $about = $this->getReplacePicUrl($ext['about']);
            }
        }
        return $this->apiSuccess("",['content'=>$about]);
    }
}
