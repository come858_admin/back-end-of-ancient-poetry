import { request } from '../../utils/Request.js';
// 图片
export async function getPictureList(data) {
  return await request('/index/getPictureList', {
    data: data,
  });
}
// 图片详情
export async function getPictureDetails(data) {
  return await request('/index/getPictureDetails', {
    data: data,
  });
}



