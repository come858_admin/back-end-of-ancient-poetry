import { request } from '../../utils/Request.js';
// 登录或注册
export async function loginApi(data) {
  return await request('/my/login', {
    data: data,
	method:'POST'
  });
}