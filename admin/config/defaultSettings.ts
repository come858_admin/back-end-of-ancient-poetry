import { Settings as LayoutSettings } from '@ant-design/pro-layout';

const Settings: LayoutSettings & {
  pwa?: boolean;
  logo?: string;
} = {
  title: 'LvaCMS2.0 后台管理', //标题
  logo: '/logo.png', // logo
  layout: 'mix', //导航模式 side、top、mix
  contentWidth: 'Fluid', //内容区域宽度：流式Fluid、定宽Fixed
  splitMenus: true,
  navTheme: 'light', // 导航的主题，side 和 mix 模式下是左侧菜单的主题，top 模式下是顶部菜单 light | dark
  primaryColor: '#1890ff',
  fixedHeader: true, // 固定header
  fixSiderbar: true, // 固定侧边
  pwa: false,
  menu: {
    locale: false,
  },
};

export default Settings;
