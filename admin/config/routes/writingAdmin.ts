export default [
  {
    path: '/writingAdmin',
    redirect: '/writingAdmin/dashboard',
  },
  {
    path: '/writingAdmin/dashboard',
    component: './writingAdmin/dashboard/index',
  },
  {
    path: '/writingAdmin/setting/project',
    component: './writingAdmin/setting/project/index',
  },
  {
    path: '/writingAdmin/setting/allusion',
    component: './writingAdmin/setting/allusion/index',
  },
  {
    path: '/writingAdmin/setting/anthology',
    component: './writingAdmin/setting/anthology/index',
  },
  {
    path: '/writingAdmin/setting/brand',
    component: './writingAdmin/setting/brand/index',
  },
  {
    path: '/writingAdmin/setting/citysMountain',
    component: './writingAdmin/setting/citysMountain/index',
  },
  {
    path: '/writingAdmin/setting/describeSceneryMountain',
    component: './writingAdmin/setting/describeSceneryMountain/index',
  },
  {
    path: '/writingAdmin/setting/dynasty',
    component: './writingAdmin/setting/dynasty/index',
  },
  {
    path: '/writingAdmin/setting/famousMountain',
    component: './writingAdmin/setting/famousMountain/index',
  },
  {
    path: '/writingAdmin/setting/festivalsMountain',
    component: './writingAdmin/setting/festivalsMountain/index',
  },
  {
    path: '/writingAdmin/setting/flowersPlant',
    component: './writingAdmin/setting/flowersPlant/index',
  },
  {
    path: '/writingAdmin/setting/geography',
    component: './writingAdmin/setting/geography/index',
  },
  {
    path: '/writingAdmin/setting/poemBook',
    component: './writingAdmin/setting/poemBook/index',
  },
  {
    path: '/writingAdmin/setting/season',
    component: './writingAdmin/setting/season/index',
  },
  {
    path: '/writingAdmin/setting/solarTerm',
    component: './writingAdmin/setting/solarTerm/index',
  },
  {
    path: '/writingAdmin/setting/theme',
    component: './writingAdmin/setting/theme/index',
  },
  {
    path: '/writingAdmin/setting/timeMsMountain',
    component: './writingAdmin/setting/timeMsMountain/index',
  },
  {
    path: '/writingAdmin/setting/textbook',
    component: './writingAdmin/setting/textbook/index',
  },
  {
    path: '/writingAdmin/setting/work',
    component: './writingAdmin/setting/work/index',
  },
  {
    path: '/writingAdmin/setting/author',
    component: './writingAdmin/setting/author/index',
  },
  {
    path: '/writingAdmin/setting/picture',
    component: './writingAdmin/setting/picture/index',
  },
  {
    path: '/writingAdmin/setting/notice',
    component: './writingAdmin/setting/notice/index',
  },
  {
    path: '/writingAdmin/user/user',
    component: './writingAdmin/user/user/index',
  },
];
