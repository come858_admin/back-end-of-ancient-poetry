export default [
  {
    path: '/toolAdmin',
    redirect: '/toolAdmin/dashboard',
  },
  {
    path: '/toolAdmin/dashboard',
    component: './toolAdmin/dashboard/index',
  },
  {
    path: '/toolAdmin/setting/project',
    component: './toolAdmin/setting/project/index',
  },
  {
    path: '/toolAdmin/setting/picture',
    component: './toolAdmin/setting/picture/index',
  },
  {
    path: '/toolAdmin/user/user',
    component: './toolAdmin/user/user/index',
  },
];
