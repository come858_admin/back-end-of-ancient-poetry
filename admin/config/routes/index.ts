﻿import Admin from './admin';
import BlogAdmin from './blogAdmin';
import WritingAdmin from './writingAdmin';
import ToolAdmin from './toolAdmin';
export default [
  {
    path: '/login',
    layout: false,
    component: './admin/Login',
  },
  ...Admin,
  ...BlogAdmin,
  ...WritingAdmin,
  ...ToolAdmin,
  {
    path: '/403',
    component: './error/403',
  },
  {
    component: './error/404',
  },
];
