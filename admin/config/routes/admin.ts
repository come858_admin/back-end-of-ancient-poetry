export default [
  {
    path: '/',
    redirect: '/admin/dashboard',
  },
  {
    path: '/admin',
    redirect: '/admin/dashboard',
  },
  {
    path: '/admin/dashboard',
    component: './admin/dashboard/index',
  },
  {
    path: '/admin/auth/admin',
    component: './admin/auth/admin/index',
  },
  {
    path: '/admin/auth/group',
    component: './admin/auth/group/index',
  },
  {
    path: '/admin/auth/rule',
    component: './admin/auth/rule/index',
  },
  {
    path: '/admin/auth/project',
    component: './admin/auth/project/index',
  },
  {
    path: '/admin/config/backstage',
    component: './admin/config/backstage/index',
  },
  {
    path: '/admin/config/aboutUs',
    component: './admin/config/aboutUs/index',
  },
  {
    path: '/admin/dataBase/tables',
    component: './admin/dataBase/tables/index',
  },
  {
    path: '/admin/dataBase/restoreData',
    component: './admin/dataBase/restoreData/index',
  },
  {
    path: '/admin/log/operationLog',
    component: './admin/log/operationLog/index',
  },
  {
    path: '/admin/log/logViewer',
    component: './admin/log/logViewer/index',
  },
  {
    path: '/admin/log/storage',
    component: './admin/log/storage/index',
  },
  {
    path: '/admin/information/department',
    component: './admin/information/department/index',
  },
  {
    path: '/admin/information/personnel',
    component: './admin/information/personnel/index',
  },
  {
    path: '/admin/information/archiveType',
    component: './admin/information/archiveType/index',
  },
  {
    path: '/admin/information/archive',
    component: './admin/information/archive/index',
  },
  {
    path: '/admin/archiveUser/personnelData',
    component: './admin/archiveUser/personnelData/index',
  },
];
