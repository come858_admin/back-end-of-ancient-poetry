import type { IButtonMenu, IDomEditor } from '@wangeditor/editor';
class GetHtmlView implements IButtonMenu {
  title: string = '查看HTML';
  tag: string = 'button';
  iconSvg: string = '<div>HTML</div>';
  constructor() {
    this.title = '查看HTML'; // 自定义菜单标题
    this.tag = 'button';
    this.iconSvg = '<div>HTML</div>';
  }
  // 获取菜单执行时的 value ，用不到则返回空 字符串或 false
  getValue(): string | boolean {
    return false;
  }

  // 菜单是否需要激活（如选中加粗文本，“加粗”菜单会激活），用不到则返回 false
  isActive(): boolean {
    return false;
  }

  // 菜单是否需要禁用（如选中 H1 ，“引用”菜单被禁用），用不到则返回 false
  isDisabled(): boolean {
    return false;
  }
  // 点击菜单时触发的函数
  async exec(editor: IDomEditor) {
    const { onClick } = editor.getMenuConfig('getHtmlView');
    if (onClick) {
      onClick(editor);
      return;
    }
  }
}

export default GetHtmlView;
