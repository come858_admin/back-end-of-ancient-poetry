import React from 'react';
import { Modal } from 'antd';
import MonacoEditor from 'react-monaco-editor';
interface TypeProps {
  isModalVisibleImageView?: boolean;
  html: string;
  onChange: (html: string) => void;
}
class Index extends React.Component<any, TypeProps> {
  constructor(props: TypeProps) {
    super(props);
    this.state = {
      isModalVisibleImageView: false,
      ...props,
    };
  }
  async getOkList() {
    await this.hidden();
    this.state.onChange(this.state.html);
  }
  async show(html: string) {
    this.setState({ isModalVisibleImageView: true, html: html });
  }
  async hidden() {
    await this.setState({ isModalVisibleImageView: false });
  }
  render() {
    return (
      <>
        <Modal
          title="查看HTML"
          okText="修改HTML"
          width={750}
          visible={this.state.isModalVisibleImageView}
          destroyOnClose
          onCancel={async () => {
            await this.hidden();
          }}
          onOk={async () => {
            await this.getOkList();
          }}
        >
          <MonacoEditor
            height="300"
            language="html"
            theme="vs-dark"
            value={this.state.html}
            options={{
              wordWrap: 'on', // 自动换行
              acceptSuggestionOnEnter: 'smart',
              bracketPairColorization: { enabled: true },
              language: 'html',
            }}
            editorDidMount={(editor) => {
              editor.focus();
            }}
            onChange={async (newValue) => {
              this.setState({ html: newValue });
            }}
          />
        </Modal>
      </>
    );
  }
}
export default Index;

// import React, { useState } from 'react';
// import { Modal } from 'antd';
// import type { IDomEditor } from '@wangeditor/editor';
// interface PorpsType {
//   onConfirm: (html: string) => void;
//   editor: IDomEditor | null;
//   isHtmlModalVisible: boolean | undefined;
// }
// const Index: React.FC<PorpsType> = (porps) => {
//   const [isHtmlModalVisible, setIsHtmlModalVisible] = useState(porps.isHtmlModalVisible);
//   const handleOk = () => {
//     setIsHtmlModalVisible(false);
//   };
//   const handleCancel = () => {
//     setIsHtmlModalVisible(false);
//   };
//   return (
//     <Modal title="查看HTML" visible={isHtmlModalVisible} onOk={handleOk} onCancel={handleCancel}>
//       <p>Some contents...</p>
//       <p>Some contents...</p>
//       <p>Some contents...</p>
//     </Modal>
//   );
// };
// export default Index;
