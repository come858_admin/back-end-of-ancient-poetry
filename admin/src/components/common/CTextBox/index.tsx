import React from 'react';
import { Form, Input, Select } from 'antd';
import Color from '@/components/common/Color';
import styles from './index.less';
interface TypeProps {
  onCancel?: any;
  options: any;
}
class CTextBox extends React.Component<any, TypeProps> {
  constructor(props: TypeProps) {
    super(props);
    this.state = {
      ...props,
    };
  }
  async setOptions(value: string, key: string) {
    const options = this.state.options;
    if (value != '') {
      options[key] = value;
    } else {
      delete options[key];
    }
    this.setState({ options: options });
    if (this.state.onCancel) {
      this.state.onCancel(options);
    }
  }
  render() {
    return (
      <div className={styles.infoBox}>
        <Form.Item label="color">
          <Color
            color={this.state.options.color || '#000000'}
            onCancel={async (color: any) => {
              this.setOptions(color, 'color');
            }}
          />
        </Form.Item>
        <Form.Item label="background-color">
          <Color
            color={this.state.options['background-color'] || '#ffffff'}
            onCancel={async (color: any) => {
              this.setOptions(color, 'background-color');
            }}
          />
        </Form.Item>
        <Form.Item label="font-size">
          <Input
            allowClear
            placeholder="font-size"
            value={this.state.options['font-size'] || ''}
            onChange={(e) => {
              this.setOptions(e.target.value, 'font-size');
            }}
          />
        </Form.Item>
        <Form.Item label="text-indent">
          <Input
            allowClear
            placeholder="设置首行的字体缩进，单位可以是em、px"
            value={this.state.options['text-indent'] || ''}
            onChange={(e) => {
              this.setOptions(e.target.value, 'text-indent');
            }}
          />
        </Form.Item>
        <Form.Item label="line-height">
          <Input
            allowClear
            placeholder="line-height"
            value={this.state.options['line-height'] || ''}
            onChange={(e) => {
              this.setOptions(e.target.value, 'line-height');
            }}
          />
        </Form.Item>
        <Form.Item label="text-align">
          <Select
            allowClear
            value={this.state.options['text-align'] || null}
            placeholder="text-align"
            onChange={(value) => {
              this.setOptions(value, 'text-align');
            }}
          >
            <Select.Option value="left" key="1">
              left（把文本排列到左边。默认值：由浏览器决定。）
            </Select.Option>
            <Select.Option value="right" key="2">
              right（把文本排列到右边。）
            </Select.Option>
            <Select.Option value="center" key="3">
              center（把文本排列到中间。）
            </Select.Option>
            <Select.Option value="inherit" key="4">
              inherit（规定应该从父元素继承 text-align 属性的值。）
            </Select.Option>
          </Select>
        </Form.Item>
        <Form.Item label="font-weight">
          <Select
            allowClear
            value={this.state.options['font-weight'] || null}
            placeholder="font-weight"
            onChange={(value) => {
              this.setOptions(value, 'font-weight');
            }}
          >
            <Select.Option value="100" key="1">
              100
            </Select.Option>
            <Select.Option value="200" key="2">
              200
            </Select.Option>
            <Select.Option value="300" key="3">
              300
            </Select.Option>
            <Select.Option value="400" key="4">
              400
            </Select.Option>
            <Select.Option value="500" key="5">
              500
            </Select.Option>
            <Select.Option value="600" key="6">
              600
            </Select.Option>
            <Select.Option value="700" key="7">
              700
            </Select.Option>
          </Select>
        </Form.Item>
        <Form.Item label="font-style">
          <Select
            allowClear
            value={this.state.options['font-style'] || null}
            placeholder="font-style"
            onChange={(value) => {
              this.setOptions(value, 'font-style');
            }}
          >
            <Select.Option value="normal" key="1">
              normal（默认值。浏览器显示一个标准的字体样式。）
            </Select.Option>
            <Select.Option value="italic" key="2">
              italic（浏览器会显示一个斜体的字体样式。）
            </Select.Option>
            <Select.Option value="oblique" key="3">
              oblique（浏览器会显示一个倾斜的字体样式。）
            </Select.Option>
            <Select.Option value="inherit" key="4">
              inherit（规定应该从父元素继承字体样式。）
            </Select.Option>
          </Select>
        </Form.Item>

        <Form.Item label="white-space（规定段落中的文本不进行换行）">
          <Select
            allowClear
            value={this.state.options['white-space'] || null}
            placeholder="white-space"
            onChange={(value) => {
              this.setOptions(value, 'white-space');
            }}
          >
            <Select.Option value="normal" key="1">
              normal（默认。空白会被浏览器忽略。）
            </Select.Option>
            <Select.Option value="pre" key="2">
              pre（空白会被浏览器保留。其行为方式类似 HTML 中的 "pre" 标签。）
            </Select.Option>
            <Select.Option value="nowrap" key="3">
              nowrap（文本不会换行，文本会在在同一行上继续，直到遇到 br 标签为止。）
            </Select.Option>
            <Select.Option value="pre-wrap" key="4">
              pre-wrap（保留空白符序列，但是正常地进行换行。）
            </Select.Option>

            <Select.Option value="pre-line" key="5">
              pre-wrap（合并空白符序列，但是保留换行符。）
            </Select.Option>

            <Select.Option value="inherit" key="6">
              inherit（规定应该从父元素继承 white-space 属性的值。）
            </Select.Option>
          </Select>
        </Form.Item>
        <Form.Item label="overflow">
          <Select
            allowClear
            value={this.state.options.overflow || null}
            placeholder="overflow"
            onChange={(value) => {
              this.setOptions(value, 'overflow');
            }}
          >
            <Select.Option value="visible" key="1">
              visible（默认值。内容不会被修剪，会呈现在元素框之外。）
            </Select.Option>
            <Select.Option value="hidden" key="2">
              hidden（内容会被修剪，并且其余内容是不可见的。）
            </Select.Option>
            <Select.Option value="scroll" key="3">
              scroll（内容会被修剪，但是浏览器会显示滚动条以便查看其余的内容。）
            </Select.Option>
            <Select.Option value="auto" key="4">
              auto（如果内容被修剪，则浏览器会显示滚动条以便查看其余的内容。）
            </Select.Option>

            <Select.Option value="inherit" key="5">
              inherit（规定应该从父元素继承 overflow 属性的值。）
            </Select.Option>
          </Select>
        </Form.Item>
        <Form.Item label="border-radius">
          <Input
            allowClear
            placeholder="border-radius"
            value={this.state.options['border-radius'] || ''}
            onChange={(e) => {
              this.setOptions(e.target.value, 'border-radius');
            }}
          />
        </Form.Item>
      </div>
    );
  }
}
export default CTextBox;
