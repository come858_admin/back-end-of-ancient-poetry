import React from 'react';
import styles from './index.less';
import { Modal, message, Pagination, Spin, Empty } from 'antd';
import { CheckCircleOutlined } from '@ant-design/icons';
import { getImageList } from '@/services/admin/common/api';
import CUploadImage from '../CUploadImage';
import { isAssetType } from '@/utils/utils';
interface TypeProps {
  isModalVisibleImageView?: boolean;
  imageList?: TypeImage[];
  count: number | null;
  imageIdArr?: number[];
  limit?: number;
  page?: number;
  total?: number;
  onChange: any;
  spinning?: boolean;
}
interface TypeImage {
  id: number;
  http_url: string;
}
class CUploadView extends React.Component<any, TypeProps> {
  constructor(props: TypeProps) {
    super(props);
    this.state = {
      isModalVisibleImageView: false,
      imageIdArr: [],
      page: 1,
      limit: 10,
      total: 0,
      imageList: [],
      spinning: false,
      ...props,
    };
  }
  static defaultProps: TypeProps = {
    count: null,
    onChange: null,
  };
  async getOkList() {
    const imageIdArr = this.state.imageIdArr;
    const imageList = this.state.imageList;
    let arr: any = [];
    if (imageIdArr && imageList && imageList?.length > 0 && imageIdArr?.length > 0) {
      arr = imageList.filter((item: any) => {
        if (imageIdArr.indexOf(item.id) > -1) {
          return item;
        }
      });
    }
    await this.hidden();
    this.state.onChange(arr);
  }
  async show() {
    this.setState({ isModalVisibleImageView: true });
    await this.getImageList();
  }
  async getImageList() {
    await this.setState({ imageList: [], spinning: true });
    const res = await getImageList({ page: this.state.page, limit: this.state.limit });
    if (res.status === 20000) {
      this.setState({
        total: res.data.total,
        imageList: res.data.list,
        spinning: false,
      });
    }
  }
  async hidden() {
    await this.setState({ isModalVisibleImageView: false, imageIdArr: [] });
  }
  imageItemView(item: TypeImage) {
    return (
      <div key={item.id} className={styles.imageItem}>
        <div
          className={styles.imageView}
          onClick={() => {
            const imageIdArr = this.state.imageIdArr;
            if (imageIdArr && imageIdArr.length > 0 && imageIdArr.indexOf(item.id) > -1) {
              imageIdArr?.splice(imageIdArr.indexOf(item.id), 1);
            } else {
              const count = this.state.count;
              if (!count || (imageIdArr && count <= imageIdArr?.length)) {
                message.warning('最多可选' + this.state.count + '个文件');
                return;
              } else {
                imageIdArr?.push(item.id);
              }
            }
            this.setState({
              imageIdArr: imageIdArr,
            });
          }}
        >
          {isAssetType(item.http_url) == 1 ? (
            <img src={item.http_url} alt="" />
          ) : (
            <>{item.http_url}</>
          )}

          {this.state.imageIdArr &&
          this.state.imageIdArr.length > 0 &&
          this.state.imageIdArr.indexOf(item.id) > -1 ? (
            <div className={styles.action}>
              <CheckCircleOutlined />
            </div>
          ) : (
            ''
          )}
        </div>
      </div>
    );
  }
  render() {
    return (
      <>
        <Modal
          title={`图片上传（可选择：${this.state.count}张，已选择：${
            this.state.imageIdArr?.length || 0
          }张）`}
          width={750}
          visible={this.state.isModalVisibleImageView}
          destroyOnClose
          onCancel={async () => {
            await this.hidden();
          }}
          onOk={async () => {
            await this.getOkList();
          }}
        >
          <div>
            <CUploadImage
              onChange={async () => {
                await this.setState({ page: 1 });
                await this.getImageList();
              }}
            />
          </div>
          <Spin spinning={this.state.spinning}>
            {this.state.imageList && this.state.imageList?.length > 0 ? (
              <div className={styles.imageList}>
                {this.state.imageList?.map((item: TypeImage) => {
                  return this.imageItemView(item);
                })}
              </div>
            ) : (
              <div className={styles.noList}>
                <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />
              </div>
            )}
          </Spin>
          <Pagination
            showSizeChanger
            total={this.state.total}
            showTotal={(total, range) => {
              return `第${range[0]}-${range[1]}  条/总共 ${total} 条`;
            }}
            defaultPageSize={this.state.limit}
            current={this.state.page}
            defaultCurrent={this.state.page}
            onChange={async (page: number, pageSize?: number) => {
              await this.setState({
                page: page,
              });
              if (pageSize) {
                await this.setState({
                  limit: pageSize,
                });
              }
              await this.getImageList();
            }}
          />
        </Modal>
      </>
    );
  }
}
export default CUploadView;
