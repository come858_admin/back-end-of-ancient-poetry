import React from 'react';
import { PlusOutlined, DeleteOutlined, EyeOutlined, CopyOutlined } from '@ant-design/icons';
import styles from './index.less';
import CImageInfo from '@/components/common/CImageInfo';
import CUploadView from './CUploadView';
import type { SortEnd } from 'react-sortable-hoc';
import { arrayMove, SortableContainer, SortableElement } from 'react-sortable-hoc';
import { isAssetType } from '@/utils/utils';
import { notification } from 'antd';
interface TypeImageList {
  id: number;
  url: string;
  open: number;
  http_url: string;
}
interface TypeProps {
  imageList?: TypeImageList[];
  count?: number;
  src?: string;
  isCount?: number;
  onChange?: any;
}

const ImageItemView = SortableElement((params: any) => {
  const type = isAssetType(params.item.http_url);
  if (type == 1) {
    return (
      <div key={params.index} className={styles.imageBox}>
        <div className={styles.imageView}>
          <img src={params.item.http_url} />
          <div className={styles.imageInfo}>
            <EyeOutlined
              className={styles.info}
              onClick={() => {
                params.onInfoSrc(params.item.http_url);
              }}
            />
            <DeleteOutlined
              className={styles.info}
              onClick={() => {
                params.deleteImageItem(params.indexKey);
              }}
            />
          </div>
        </div>
      </div>
    );
  } else {
    return (
      <div key={params.index} className={styles.imageBox}>
        <div className={styles.imageView}>
          {params.item.http_url}
          <div className={styles.imageInfo}>
            <CopyOutlined
              className={styles.info}
              onClick={() => {
                const oInput: any = document.createElement('input');
                oInput.value = params.item.http_url;
                document.body.appendChild(oInput);
                oInput.select(); // 选择对象
                document.execCommand('Copy'); // 执行浏览器复制命令
                oInput.remove();
                notification.open({
                  message: '复制成功',
                  description: params.item.http_url,
                });
                // params.onInfoSrc();
              }}
            />
            <DeleteOutlined
              className={styles.info}
              onClick={() => {
                params.deleteImageItem(params.indexKey);
              }}
            />
          </div>
        </div>
      </div>
    );
  }
});
const ImageViewList = SortableContainer((params: any) => {
  return (
    <div className={styles.imageList}>
      {params.imageList?.map((item: TypeImageList, index: number) => {
        return (
          <ImageItemView
            key={`${item.id + '-' + index}`}
            item={item}
            indexKey={index}
            index={index}
            onInfoSrc={params.onInfoSrc}
            deleteImageItem={params.deleteImageItem}
          />
        );
      })}
    </div>
  );
});
class CUpload extends React.Component<any, TypeProps> {
  constructor(props: TypeProps) {
    super(props);
    this.state = {
      src: '',
      ...props,
      isCount: (props.count || 1) - (props.imageList ? props.imageList?.length : 0),
    };
  }
  async getIsCount() {
    await this.setState({
      isCount:
        (this.state.count ? this.state.count : 1) -
        (this.state.imageList ? this.state.imageList?.length : 0),
    });
  }
  modalVisibleImageViewModal: any;
  static defaultProps: TypeProps = {
    count: 1,
    imageList: [],
    src: '',
    onChange: null,
  };
  async deleteImageItem(index: number) {
    const list = this.state?.imageList || [];
    const arr = [];
    for (let i = 0; i < list.length; i++) {
      if (i !== index) {
        arr.push(list[i]);
      }
    }
    await this.setState({ imageList: arr });
    await this.getIsCount();
    if (this.state.onChange) {
      await this.state.onChange(arr);
    }
  }
  onSortEnd = ({ oldIndex, newIndex }: SortEnd) => {
    if (oldIndex !== newIndex) {
      const imageList: TypeImageList[] = this.state.imageList || [];
      this.setState({
        imageList: arrayMove(imageList, oldIndex, newIndex),
      });
    }
  };

  render() {
    return (
      <>
        <div className={styles.container}>
          <div className={styles.imageList}>
            <ImageViewList
              imageList={this.state.imageList}
              onSortEnd={this.onSortEnd}
              onInfoSrc={(src: string) => {
                this.setState({ src: src });
              }}
              deleteImageItem={(index: number) => {
                this.deleteImageItem(index);
              }}
            />
            {this.state.isCount ? (
              <div
                className={styles.uploadBtn}
                onClick={() => {
                  this.modalVisibleImageViewModal.show();
                }}
              >
                <PlusOutlined />
                <div style={{ marginTop: 8 }}>上传图片</div>
              </div>
            ) : (
              ''
            )}
          </div>
          {this.state.src !== '' && (
            <CImageInfo
              src={this.state.src}
              onCancel={() => {
                this.setState({ src: '' });
              }}
            />
          )}
          <CUploadView
            ref={(com) => {
              this.modalVisibleImageViewModal = com;
            }}
            key={this.state.isCount}
            count={this.state.isCount}
            onChange={async (data: []) => {
              if (data.length) {
                const imageList = this.state.imageList;
                if (imageList?.length) {
                  await this.setState({ imageList: [...imageList, ...data] });
                } else {
                  await this.setState({ imageList: [...data] });
                }
                await this.getIsCount();
                if (this.state.onChange) {
                  await this.state.onChange(this.state.imageList);
                }
              }
            }}
          />
        </div>
      </>
    );
  }
}
export default CUpload;
