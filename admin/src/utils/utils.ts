import moment from 'moment';
const waitTime = (time: number = 100) => {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(true);
    }, time);
  });
};

const getSort = (params: any, sort: any) => {
  if (JSON.stringify(sort) !== '{}') {
    for (const x in sort) {
      if (sort[x] === 'ascend') {
        sort[x] = 'desc';
      } else {
        sort[x] = 'asc';
      }
    }
    params.sort = sort;
  }
  return params;
};

const setLsetData = (res: API.ResponseData) => {
  if (res.status === 20000) {
    return { success: true, data: res.data.list, total: res.data.total };
  }
  return {};
};
const toUp = (str: string) => {
  let newStr = '';
  const arr = str.split('-'); //先用字符串分割成数组
  arr.forEach((item: any, index: number) => {
    if (index > 0) {
      return (newStr += item.replace(item[0], item[0].toUpperCase()));
    } else {
      return (newStr += item);
    }
  });
  return newStr;
};
const toUpAll = (optionsObj: any) => {
  const newOptionsObj = {};
  for (const x in optionsObj) {
    newOptionsObj[toUp(x)] = optionsObj[x];
  }
  return newOptionsObj;
};
//saveForm方法
const saveFormDate = (value: string) => {
  return moment(value, 'YYYY-MM-DD');
};
const randomString = (len: any = 32) => {
  const chars = 'ABCDEFGHJKMNPQRSTWXYZabcdefhijkmnprstwxyz2345678';
  const maxPos = chars.length;
  let pwd = '';
  for (let i = 0; i < len; i++) {
    pwd += chars.charAt(Math.floor(Math.random() * maxPos));
  }
  return pwd;
};

// 返回文件后缀的类型

const isAssetType = (url: string) => {
  //获取最后一个.的位置
  const index = url.lastIndexOf('.');
  // 获取后缀
  const ext = url.substr(index + 1);
  console.log(ext);
  if (
    ['png', 'jpg', 'jpeg', 'bmp', 'gif', 'webp', 'psd', 'svg', 'tiff', 'ico'].indexOf(
      ext.toLowerCase(),
    ) !== -1
  ) {
    return 1;
  } else {
    return ext;
  }
};

export { waitTime, getSort, setLsetData, toUp, toUpAll, saveFormDate, randomString, isAssetType };
