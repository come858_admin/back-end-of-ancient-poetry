import { request as umiRequest } from 'umi';

const download = async (url: string) => {
  return await umiRequest(url, {
    method: 'GET',
    responseType: 'blob', // 必须注明返回二进制流
  });
};
export { download };
