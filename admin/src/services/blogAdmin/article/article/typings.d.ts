declare namespace API {
  type BlogAdminArticleList = {
    id: number;
    name: string;
    status: 1 | 0;
    open: 1 | 0;
    sort: number;
    created_at: string;
    image_to: any;
    updated_at: string | null;
    article_to: any;
    web_url: string;
  };
  type BlogAdminArticleForm = {
    id: number | null;
    article_type_id: number | null;
    name: string;
    status: 1 | 0;
    open: 1 | 0;
    sort: number;
    download_url: string;
    download_key: string;
    image_id: number | null;
    keywords: string;
    description: string;
    content: string;
    labelArr: [];
  };
}
