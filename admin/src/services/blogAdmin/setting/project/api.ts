import { request } from '@/utils/Request';
// 项目配置页面
export async function getInfo() {
  return await request('/blogAdmin/project/index');
}

// 项目配置提交
export async function setInfo(data: API.BlogAdminProjectForm) {
  return await request('/blogAdmin/project/update', {
    method: 'put',
    data: data,
  });
}
