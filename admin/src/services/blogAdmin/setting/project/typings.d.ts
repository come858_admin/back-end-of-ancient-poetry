declare namespace API {
  type BlogAdminProjectForm = {
    name: string;
    logo_id: number | null;
    ico_id: number | null;
    url: string;
    description: string;
    keywords: string;
    status: 0 | 1;
    gzh_image_id: number | null;
    wx_image_id: number | null;
    about: string;
    statement: string;
    network_name: string;
    occupation: string;
    current_residence: string;
    email: string;
    qq: string;
    wx: string;
    reprint_statement: string;
    station_establishment_time: string;
    website_program: string;
    website_program_url: string;
    baidu_propelling_movement_token: string;
    baidu_propelling_movement_site: string;
    baidu_statistics_url: string;
  };
}
