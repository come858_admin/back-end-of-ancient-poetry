declare namespace API {
  type BlogAdminLinkList = {
    id: number;
    name: string;
    url: string;
    status: 1 | 0;
    image_to: any;
    sort: number;
    created_at: string;
    updated_at: string | null;
  };
  type BlogAdminLinkForm = {
    id: number | null;
    name: string;
    url: string;
    image_id: number | null;
    status: 1 | 0;
    sort: number;
  };
}
