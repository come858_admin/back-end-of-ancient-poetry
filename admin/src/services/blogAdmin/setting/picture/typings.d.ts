declare namespace API {
  type BlogAdminPictureList = {
    id: number;
    type: any;
    name: string;
    url: string;
    status: 1 | 0;
    image_to: any;
    sort: number;
    created_at: string;
    updated_at: string | null;
  };
  type BlogAdminPictureForm = {
    id: number | null;
    type: number | null;
    content: string;
    url: string;
    image_id: number | null;
    status: 1 | 0;
    sort: number;
  };
}
