import { request } from '@/utils/Request';

/** 图片管理 GET */
export async function getList(data: API.ListQequest) {
  return await request('/blogAdmin/picture/index', {
    params: data,
  });
}
// 调整状态
export async function setStatus(id: number, data: API.StatusQequest) {
  return await request('/blogAdmin/picture/status/' + id, {
    method: 'put',
    data: data,
  });
}
// 排序
export async function setSorts(id: number, data: API.SortQequest) {
  return await request('/blogAdmin/picture/sorts/' + id, {
    method: 'put',
    data: data,
  });
}

// 添加
export async function add(data: API.BlogAdminPictureForm) {
  return await request('/blogAdmin/picture/add', {
    method: 'post',
    data: data,
  });
}
// 编辑页面
export async function edit(id: number) {
  return await request('/blogAdmin/picture/edit/' + id);
}

// 编辑提交
export async function update(id: number, data: API.BlogAdminPictureForm) {
  return await request('/blogAdmin/picture/update/' + id, {
    method: 'put',
    data: data,
  });
}
// 删除
export async function del(id: number) {
  return await request('/blogAdmin/picture/del/' + id, {
    method: 'delete',
  });
}
// 批量删除
export async function delAll(data: { idArr: any[] }) {
  return await request('/blogAdmin/picture/delAll/', {
    method: 'delete',
    data: data,
  });
}
