import { request } from '@/utils/Request';
// 添加
export async function getVideoUrl(data: API.BlogAdminCrawlerUrl) {
  return await request('/blogAdmin/crawler/video', {
    method: 'post',
    data: data,
  });
}
