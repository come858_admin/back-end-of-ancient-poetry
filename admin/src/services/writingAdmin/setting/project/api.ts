import { request } from '@/utils/Request';
// 项目配置页面
export async function getInfo() {
  return await request('/writingAdmin/project/index');
}

// 项目配置提交
export async function setInfo(data: API.WritingAdminProjectForm) {
  return await request('/writingAdmin/project/update', {
    method: 'put',
    data: data,
  });
}
