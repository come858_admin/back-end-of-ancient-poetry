import { request } from '@/utils/Request';

/** 图片管理 GET */
export async function getList(data: API.ListQequest) {
  return await request('/writingAdmin/picture/index', {
    params: data,
  });
}
// 调整状态
export async function setStatus(id: number, data: API.StatusQequest) {
  return await request('/writingAdmin/picture/status/' + id, {
    method: 'put',
    data: data,
  });
}
// 排序
export async function setSorts(id: number, data: API.SortQequest) {
  return await request('/writingAdmin/picture/sorts/' + id, {
    method: 'put',
    data: data,
  });
}

// 添加
export async function add(data: API.WritingAdminPictureForm) {
  return await request('/writingAdmin/picture/add', {
    method: 'post',
    data: data,
  });
}
// 编辑页面
export async function edit(id: number) {
  return await request('/writingAdmin/picture/edit/' + id);
}

// 编辑提交
export async function update(id: number, data: API.WritingAdminPictureForm) {
  return await request('/writingAdmin/picture/update/' + id, {
    method: 'put',
    data: data,
  });
}
// 删除
export async function del(id: number) {
  return await request('/writingAdmin/picture/del/' + id, {
    method: 'delete',
  });
}
// 批量删除
export async function delAll(data: { idArr: any[] }) {
  return await request('/writingAdmin/picture/delAll/', {
    method: 'delete',
    data: data,
  });
}
