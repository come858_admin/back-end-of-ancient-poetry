import { request } from '@/utils/Request';
// 存储配置
export async function setStorage() {
  return await request('/writingAdmin/config/setStorage', {
    method: 'post',
  });
}
// 获取配置
export async function getStorage() {
  return await request('/writingAdmin/config/getStorage');
}
