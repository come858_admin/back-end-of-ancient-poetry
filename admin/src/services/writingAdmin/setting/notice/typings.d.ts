declare namespace API {
  type WritingAdminNoticeList = {
    id: number;
    type: any;
    open: any;
    title: string;
    url: string;
    status: 1 | 0;
    image_to: any;
    sort: number;
    created_at: string;
    updated_at: string | null;
  };
  type WritingAdminNoticeForm = {
    id: number | null;
    type: number | null;
    open: number | null;
    content: string;
    url: string;
    title: string;
    status: 1 | 0;
    sort: number;
  };
}
