import { request } from '@/utils/Request';
export async function getList(data: API.ListQequest) {
  return await request('/writingAdmin/brand/index', {
    params: data,
  });
}
// 调整状态
export async function setStatus(id: number, data: API.StatusQequest) {
  return await request('/writingAdmin/brand/status/' + id, {
    method: 'put',
    data: data,
  });
}
// 排序
export async function setSorts(id: number, data: API.SortQequest) {
  return await request('/writingAdmin/brand/sorts/' + id, {
    method: 'put',
    data: data,
  });
}

// 添加
export async function add(data: API.ReadAdminBookForm) {
  return await request('/writingAdmin/brand/add', {
    method: 'post',
    data: data,
  });
}
// 编辑页面
export async function edit(id: number) {
  return await request('/writingAdmin/brand/edit/' + id);
}

// 编辑提交
export async function update(id: number, data: API.WritingAdminBrandForm) {
  return await request('/writingAdmin/brand/update/' + id, {
    method: 'put',
    data: data,
  });
}
// 删除
export async function del(id: number) {
  return await request('/writingAdmin/brand/del/' + id, {
    method: 'delete',
  });
}
// 批量删除
export async function delAll(data: { idArr: any[] }) {
  return await request('/writingAdmin/brand/delAll/', {
    method: 'delete',
    data: data,
  });
}
