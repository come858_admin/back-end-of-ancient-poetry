declare namespace API {
  type WritingAdminFestivalsMountainList = {
    id: number;
    name: string;
    status: 1 | 0;
    sort: number;
    created_at: string;
    image_to: any;
    updated_at: string | null;
    baidu_wiki: string;
  };
  type WritingAdminFestivalsMountainForm = {
    id: number | null;
    name: string;
    name_tr: string;
    image_id: string;
    content: string;
    content_tr: string;
    baidu_wiki: string;
    status: 1 | 0;
    sort: number;
  };
}
