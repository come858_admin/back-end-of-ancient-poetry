declare namespace API {
  type WritingAdminDynastyList = {
    id: number;
    name: string;
    status: 1 | 0;
    sort: number;
    created_at: string;
    updated_at: string | null;
    baidu_wiki: string;
  };
  type WritingAdminDynastyForm = {
    id: number | null;
    name: string;
    name_tr: string;
    content: string;
    content_tr: string;
    baidu_wiki: string;
    status: 1 | 0;
    sort: number;
  };
}
