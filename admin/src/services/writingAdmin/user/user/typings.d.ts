declare namespace API {
  type WritingAdminUserList = {
    id: number;
    nick_name: string;
    avatar_url: string;
    code_url: string;
    login_code_url: string;
    name: string;
    mobile: string;
    type: number;
    status: 1 | 0;
    created_at: string;
    open: number;
    updated_at: string | null;
    gzh_open_id: any;
  };
  type WritingAdminUserForm = {
    id: number | null;
    name: string;
    mobile: string;
    type: number;
    status: 1 | 0;
    pid: number;
  };
}
