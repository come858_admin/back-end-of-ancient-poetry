import { request } from '@/utils/Request';

/** 管理员列表 GET */
export async function getList(data: API.ListQequest) {
  return await request('/admin/admin/index', {
    params: data,
  });
}
// 调整状态
export async function setStatus(id: number, data: API.StatusQequest) {
  return await request('/admin/admin/status/' + id, {
    method: 'put',
    data: data,
  });
}
// 添加
export async function add(data: API.AdminProjectForm) {
  return await request('/admin/admin/add', {
    method: 'post',
    data: data,
  });
}
// 编辑页面
export async function edit(id: number) {
  return await request('/admin/admin/edit/' + id);
}

// 编辑提交
export async function update(id: number, data: API.AdminProjectForm) {
  return await request('/admin/admin/update/' + id, {
    method: 'put',
    data: data,
  });
}
// 删除
export async function del(id: number) {
  return await request('/admin/admin/del/' + id, {
    method: 'delete',
  });
}

// 初始化密码
export async function resetPwd(id: number) {
  return await request('/admin/admin/resetPwd/' + id, {
    method: 'put',
  });
}
