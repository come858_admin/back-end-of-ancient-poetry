declare namespace API {
  type AdminAdminList = {
    id: number;
    name: string;
    phone: string;
    username: string;
    group_id: number;
    project_id: number;
    admin_group?:
      | {
          id: number;
          name: string;
        }
      | any;
    project_value_arr?:
      | {
          id: number;
          name: string;
        }
      | any;
    status: 1 | 0;
    created_at: string;
    updated_at: string | null;
  };
  type AdminAdminForm = {
    id: number | null;
    group_id: number | null;
    project_id: [];
    username: string;
    password?: string;
    password_confirmation?: string;
    phone: string;
    name: string;
    status: 1 | 0;
  };
}
