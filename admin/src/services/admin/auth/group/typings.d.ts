declare namespace API {
  type AdminGroupList = {
    id: number;
    name: string;
    content: string;
    status: 1 | 0;
    created_at: string;
    updated_at: string | null;
  };
  type AdminGroupForm = {
    id: number | null;
    name: string;
    content: string;
    status: 1 | 0;
  };
  type AdminGroupAccess = {
    id: number | null;
    rules: [];
  };
}
