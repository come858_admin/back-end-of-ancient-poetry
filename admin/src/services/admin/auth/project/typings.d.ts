declare namespace API {
  type AdminProjectList = {
    id: number;
    name: string;
    url: string;
    description: string;
    keywords: string;
    status: 1 | 0;
    created_at: string;
    updated_at: string | null;
    logo_image: any;
    ico_image: any;
  };
  type AdminProjectForm = {
    id: number | null;
    name: string;
    url: string;
    description: string;
    keywords: string;
    status: 1 | 0;
    logo_id: number | null;
    ico_id: number | null;
    type: number | null;
  };
}
