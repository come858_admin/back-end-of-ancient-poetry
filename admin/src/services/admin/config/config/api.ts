import { request } from '@/utils/Request';
// 编辑页面
export async function getConfig(code: string) {
  return await request('/admin/config/getConfig/' + code);
}

// 编辑提交
export async function setConfig(code: string, data: API.AdminConfigForm) {
  return await request('/admin/config/setConfig/' + code, {
    method: 'put',
    data: data,
  });
}
