import { request } from '@/utils/Request';

/** 登录接口 POST */
export async function login(data: API.LoginType) {
  return await request('/admin/common/login', {
    method: 'post',
    data: data,
  });
}
/** 获取管理员信息接口 GET */
export async function my() {
  return await request('/admin/common/my');
}
// 图片列表
export async function getImageList(data: API.ListQequest) {
  return await request('/admin/common/getImageList', {
    params: data,
  });
}

/** 图片上传 POST */
export async function uploadImg(data: any) {
  return await request('/admin/common/upload', {
    method: 'post',
    data: data,
    // headers: {
    //   'Content-Type': 'multipart/form-data',
    // },
  });
}

/** 角色列表 GET */
export async function getGroupList() {
  return await request('/admin/common/getGroupList');
}

/** 项目列表 GET */
export async function getProjectList() {
  return await request('/admin/common/getProjectList');
}
/** 管理员列表 GET */
export async function getAdminList() {
  return await request('/admin/common/getAdminList');
}
// 删除
export async function logout() {
  return await request('/admin/common/logout', {
    method: 'delete',
  });
}
/** 修改密码 PUT */
export async function setUpdatePwd(data: API.UpdatePwd) {
  return await request('/admin/common/updatePwd', {
    method: 'put',
    data: data,
  });
}
/** 菜单列表 GET */
export async function getMenuList() {
  return await request('/admin/common/getMenuList');
}
/** 刷新token PUT */
export async function setRefreshToken() {
  return await request('/admin/common/refreshToken', {
    method: 'put',
  });
}

/** 修改密码 PUT */
export async function setContent(content: string) {
  return await request('/admin/common/setContent', {
    method: 'post',
    data: { content: content },
  });
}
/** 获取站点配置 GET */
export async function getConfigInfo() {
  return await request('/admin/common/getConfigInfo');
}
/** 生成验证码 GET */
export async function getCode() {
  return await request('/admin/common/getCode');
}
