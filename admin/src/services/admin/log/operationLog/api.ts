import { request } from '@/utils/Request';

/** 操作日志列表 GET */
export async function getList(data: API.ListQequest) {
  return await request('/admin/operationLog/index', {
    params: data,
  });
}
// 删除
export async function del(id: number) {
  return await request('/admin/operationLog/del/' + id, {
    method: 'delete',
  });
}
// 批量删除
export async function delAll(data: { idArr: any[] }) {
  return await request('/admin/operationLog/delAll/', {
    method: 'delete',
    data: data,
  });
}
