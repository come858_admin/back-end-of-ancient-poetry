import { request } from '@/utils/Request';
// 列表
export async function getRestoreData() {
  return await request('/admin/storage/index');
}

// 删除
export async function del(data: API.AdminDataBaseRestoreDataDelSqlFiles) {
  return await request('/admin/storage/del', {
    data: data,
    method: 'delete',
  });
}
// 查询文件详情
export async function getFiles(data: API.AdminDataBaseGetFiles) {
  return await request('/admin/storage/getFiles', {
    params: data,
  });
}
