declare namespace API {
  type AdminArchiveTypeList = {
    id: number;
    name: string;
    description: string;
    status: 1 | 0;
    level: number;
    pid: number;
    sort: number;
    images: any;
    created_at: string;
    updated_at: string | null;
  };
  type AdminArchiveTypeForm = {
    id: number | null;
    name: string;
    description: string;
    status: 1 | 0;
    level: number;
    pid: number;
    sort: number;
  };
}
