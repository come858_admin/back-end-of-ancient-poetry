declare namespace API {
  type AdminArchiveList = {
    id: number;
    description: string;
    status: 1 | 0;
    created_at: string;
    images: any;
    updated_at: string | null;
    archive_to: any;
    personnel_to: any;
  };
  type AdminArchiveForm = {
    id: number | null;
    personnel_id: number | null;
    archive_type_id: number | null;
    description: string;
    status: 1 | 0;
    created_at: string;
    images: any;
  };
}
