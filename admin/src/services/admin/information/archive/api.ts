import { request } from '@/utils/Request';

/** 文章列表 GET */
export async function getList(data: API.ListQequest) {
  return await request('/admin/archive/index', {
    params: data,
  });
}
// 调整状态
export async function setStatus(id: number, data: API.StatusQequest) {
  return await request('/admin/archive/status/' + id, {
    method: 'put',
    data: data,
  });
}

// 添加
export async function add(data: API.AdminArchiveForm) {
  return await request('/admin/archive/add', {
    method: 'post',
    data: data,
  });
}
// 编辑页面
export async function edit(id: number) {
  return await request('/admin/archive/edit/' + id);
}

// 编辑提交
export async function update(id: number, data: API.AdminArchiveForm) {
  return await request('/admin/archive/update/' + id, {
    method: 'put',
    data: data,
  });
}
// 删除
export async function del(id: number) {
  return await request('/admin/archive/del/' + id, {
    method: 'delete',
  });
}
// 批量删除
export async function delAll(data: { idArr: any[] }) {
  return await request('/admin/archive/delAll/', {
    method: 'delete',
    data: data,
  });
}
