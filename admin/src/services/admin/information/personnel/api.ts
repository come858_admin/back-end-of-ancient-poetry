import { request } from '@/utils/Request';
export async function getList(data: API.ListQequest) {
  return await request('/admin/personnel/index', {
    params: data,
  });
}
// 调整状态
export async function setStatus(id: number, data: API.StatusQequest) {
  return await request('/admin/personnel/status/' + id, {
    method: 'put',
    data: data,
  });
}

// 添加
export async function add(data: API.AdminPersonnelForm) {
  return await request('/admin/personnel/add', {
    method: 'post',
    data: data,
  });
}
// 编辑页面
export async function edit(id: number) {
  return await request('/admin/personnel/edit/' + id);
}

// 编辑提交
export async function update(id: number, data: API.AdminPersonnelForm) {
  return await request('/admin/personnel/update/' + id, {
    method: 'put',
    data: data,
  });
}
// 删除
export async function del(id: number) {
  return await request('/admin/personnel/del/' + id, {
    method: 'delete',
  });
}
// 批量删除
export async function delAll(data: { idArr: any[] }) {
  return await request('/admin/personnel/delAll/', {
    method: 'delete',
    data: data,
  });
}

export async function getPersonnelList() {
  return await request('/admin/personnel/getPersonnelList');
}
