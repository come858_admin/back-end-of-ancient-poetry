declare namespace API {
  type AdminDataBaseTables = {
    name?: string;
  };
  type AdminDataBaseTable = {
    table: string;
  };
  type AdminDataBaseTablesArr = {
    tables: any[];
  };
  type AdminDataBaseTablesList = {
    Name: string;
    Rows: number;
    Engine: string;
    Collation: string;
    Create_time: string;
    Update_time: string;
    Comment: string;
    size: string;
  };
  type AdminDataBasetableDataList = {
    COLUMN_NAME: string;
    COLUMN_TYPE: number;
    COLUMN_DEFAULT: string;
    IS_NULLABLE: string;
    EXTRA: string;
    COLUMN_COMMENT: string;
  };
  type AdminDataBaseRestoreDataList = {
    name: string;
    date: string;
    size: string;
    url: string;
  };
  type AdminDataBaseRestoreDataDelSqlFiles = {
    nameArr: any[];
  };
  type AdminDataBaseGetFiles = {
    name: string;
  };
}
