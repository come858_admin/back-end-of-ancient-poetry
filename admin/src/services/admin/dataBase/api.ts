import { request } from '@/utils/Request';
// 数据表管理
export async function getTables(data: API.AdminDataBaseTables) {
  return await request('/admin/dataBase/tables', {
    params: data,
  });
}
// 数据表管理
export async function gettableData(data: API.AdminDataBaseTable) {
  return await request('/admin/dataBase/tableData', {
    params: data,
  });
}

// 备份表
export async function setBackUp(data: API.AdminDataBaseTablesArr) {
  return await request('/admin/dataBase/backUp', {
    data: data,
    method: 'post',
  });
}
// 备份列表
export async function getRestoreData() {
  return await request('/admin/dataBase/restoreData');
}

// 删除
export async function delSqlFiles(data: API.AdminDataBaseRestoreDataDelSqlFiles) {
  return await request('/admin/dataBase/delSqlFiles', {
    data: data,
    method: 'delete',
  });
}
// 查询文件详情
export async function getFiles(data: API.AdminDataBaseGetFiles) {
  return await request('/admin/dataBase/getFiles', {
    params: data,
  });
}
