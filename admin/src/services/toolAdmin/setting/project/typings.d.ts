declare namespace API {
  type ToolAdminProjectForm = {
    name: string;
    logo_id: number | null;
    ico_id: number | null;
    url: string;
    description: string;
    keywords: string;
    status: 0 | 1;
    about: string;
    gzh_app_id: string;
    gzh_app_secret: string;
    xcx_app_id: string;
    xcx_app_secret: string;
  };
}
