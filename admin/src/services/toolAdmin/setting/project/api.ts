import { request } from '@/utils/Request';
// 项目配置页面
export async function getInfo() {
  return await request('/toolAdmin/project/index');
}

// 项目配置提交
export async function setInfo(data: API.ToolAdminProjectForm) {
  return await request('/toolAdmin/project/update', {
    method: 'put',
    data: data,
  });
}
