declare namespace API {
  type ToolAdminPictureList = {
    id: number;
    type: any;
    open: any;
    name: string;
    url: string;
    status: 1 | 0;
    image_to: any;
    sort: number;
    created_at: string;
    updated_at: string | null;
  };
  type ToolAdminPictureForm = {
    id: number | null;
    type: number | null;
    open: number | null;
    content: string;
    url: string;
    image_id: number | null;
    status: 1 | 0;
    sort: number;
  };
}
