import { request } from '@/utils/Request';
export async function getList(data: API.ListQequest) {
  return await request('/toolAdmin/user/index', {
    params: data,
  });
}
// 调整状态
export async function setStatus(id: number, data: API.StatusQequest) {
  return await request('/toolAdmin/user/status/' + id, {
    method: 'put',
    data: data,
  });
}
// 编辑页面
export async function edit(id: number) {
  return await request('/toolAdmin/user/edit/' + id);
}

// 编辑提交
export async function update(id: number, data: API.ToolAdminUserForm) {
  return await request('/toolAdmin/user/update/' + id, {
    method: 'put',
    data: data,
  });
}
// 删除
export async function del(id: number) {
  return await request('/toolAdmin/user/del/' + id, {
    method: 'delete',
  });
}
// 批量删除
export async function delAll(data: { idArr: any[] }) {
  return await request('/toolAdmin/user/delAll/', {
    method: 'delete',
    data: data,
  });
}
