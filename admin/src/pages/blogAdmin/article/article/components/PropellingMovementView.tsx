import React from 'react';
import { Popconfirm, Button } from 'antd';
import { ApiOutlined } from '@ant-design/icons';
interface TypeProps {
  onCancel?: any;
  visible?: boolean;
  confirmLoading?: boolean;
  title?: string;
  disabled?: boolean;
}
class PropellingMovementView extends React.Component<any, TypeProps> {
  constructor(props: TypeProps) {
    super(props);
    this.state = {
      visible: false,
      confirmLoading: false,
      ...props,
    };
  }
  static defaultProps: TypeProps = {
    title: '您确定要推送当前文章吗？',
  };
  handleOk = async () => {
    this.setState({ confirmLoading: true });
    await this.state.onCancel();
    this.setState({ confirmLoading: false, visible: false });
  };

  handleCancel = () => {
    this.setState({ visible: false });
  };
  showPopconfirm = () => {
    this.setState({ visible: true });
  };
  render() {
    return (
      <>
        <Popconfirm
          title={this.state.title}
          visible={this.state.visible}
          onConfirm={this.handleOk}
          okButtonProps={{ loading: this.state.confirmLoading }}
          onCancel={this.handleCancel}
        >
          <Button
            type="primary"
            size="small"
            disabled={this.state.disabled ? true : false}
            onClick={() => {
              this.showPopconfirm();
            }}
          >
            <ApiOutlined />
            文章推送
          </Button>
        </Popconfirm>
      </>
    );
  }
}
export default PropellingMovementView;
