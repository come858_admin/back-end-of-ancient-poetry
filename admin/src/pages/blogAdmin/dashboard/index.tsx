import React from 'react';
import { Avatar, Card, Col, List, Row, Image } from 'antd';
import { PageContainer } from '@ant-design/pro-layout';
import styles from './index.less';
import wxImage from '@/assets/images/wx.jpg';
import gzhImage from '@/assets/images/gzh.jpg';
import xcxImage from '@/assets/images/xcx.jpg';
const Index: React.FC = () => {
  const serverList = [
    {
      id: 1,
      title: 'PHP 7.3',
      logo: 'https://www.php.net/images/logos/php-logo.svg',
      description: 'PHP 是服务器端脚本语言。',
      updatedAt: '2022-01-07',
      member: 'Rasmus Lerdorf',
      href: 'https://www.php.net/',
    },
    {
      id: 2,
      title: 'Laravel 8.5',
      logo: 'https://cdn.learnku.com//uploads/communities/WtC3cPLHzMbKRSZnagU9.png!/both/44x44',
      description: 'Laravel 是一个基于 PHP 的 Web 应用框架，有着表现力强、语法优雅的特点。',
      updatedAt: '2022-01-07',
      member: 'Taylor Otwell',
      href: 'https://learnku.com/docs/laravel/8.5',
    },
    {
      id: 3,
      title: 'laravel-modules 8.2.0',
      logo: 'https://avatars.githubusercontent.com/u/882397?s=48&v=4',
      description:
        '用于使用模块管理大型 Laravel 应用程序。模块就像一个 Laravel 包，它有一些视图、控制器或模型。',
      updatedAt: '2022-01-07',
      member: 'Nicolas Widart',
      href: 'https://github.com/nWidart/laravel-modules',
    },
    {
      id: 4,
      title: 'laravel-wechat 5.1.0',
      logo: 'https://easywechat.vercel.app/w7team.jpg',
      description: '一个开源的 微信 非官方 SDK。',
      updatedAt: '2022-01-07',
      member: 'Overtrue',
      href: 'https://easywechat.vercel.app/5.x/',
    },
    {
      id: 5,
      title: 'jwt-auth 1.0.2',
      logo: 'https://avatars.githubusercontent.com/u/5436950?s=48&v=4',
      description: '一个好用的API生成Token验证插件。',
      updatedAt: '2022-01-07',
      member: 'Adam Hanna',
      href: 'https://github.com/adam-hanna/jwt-auth',
    },
    {
      id: 6,
      title: 'laravel-schema-extend 1.4.1',
      logo: 'https://avatars.githubusercontent.com/u/8280666?s=48&v=4',
      description: '基于Laravel数据库迁移生成表注释插件。',
      updatedAt: '2022-01-07',
      member: 'Zedisdog',
      href: 'https://github.com/zedisdog/laravel-schema-extend',
    },
    {
      id: 7,
      title: 'qiniu-laravel-storage 0.10.4',
      logo: 'https://avatars.githubusercontent.com/u/312404?s=48&v=4',
      description: '基于Laravel对七牛云API请求的插件。',
      updatedAt: '2022-01-07',
      member: 'Zgldh',
      href: 'https://github.com/zgldh/qiniu-laravel-storage',
    },
    {
      id: 8,
      title: 'ali-oss-storage 2.1.0',
      logo: 'https://avatars.githubusercontent.com/u/906128?s=48&v=4',
      description: '基于Laravel对阿里云OSS的API请求的插件。',
      updatedAt: '2022-01-07',
      member: 'Jacobcyl',
      href: 'https://github.com/jacobcyl/Aliyun-oss-storage',
    },
    {
      id: 9,
      title: 'l5-swagger 8.0',
      logo: 'https://avatars.githubusercontent.com/u/761541?s=48&v=4',
      description: 'Laravel 项目的 OpenApi 或 Swagger 规范变得简单。',
      updatedAt: '2022-01-07',
      member: 'Ashish Khokhar',
      href: 'https://github.com/DarkaOnLine/L5-Swagger',
    },
    {
      id: 10,
      title: 'predis 1.1.10',
      logo: 'https://avatars.githubusercontent.com/u/17923?s=48&v=4',
      description: '一个灵活且功能齐全的 PHP Redis 插件。',
      updatedAt: '2022-01-07',
      member: 'Daniele Alessandri',
      href: 'https://github.com/predis/predis/tree/documentation',
    },
    {
      id: 11,
      title: 'log-viewer 8.2.0',
      logo: 'https://avatars.githubusercontent.com/u/3282340?s=48&v=4',
      description: 'laravel日志管理工具。',
      updatedAt: '2022-01-07',
      member: 'Maroc',
      href: 'https://github.com/ARCANEDEV/LogViewer',
    },
    {
      id: 12,
      title: 'volc-sdk-php 1.0',
      logo: 'https://avatars.githubusercontent.com/u/67365215?s=200&v=4',
      description: '火山引擎SDK for PHP。',
      updatedAt: '2022-01-07',
      member: 'Volcengine',
      href: 'https://github.com/volcengine/volc-sdk-php',
    },
    {
      id: 13,
      title: 'zipstream-php 2.1',
      logo: 'https://avatars.githubusercontent.com/u/333918?v=4',
      description:
        '一个快速简单的 PHP 流式 zip 文件下载器。使用这个库将使您不必将 Zip 写入磁盘。您可以直接将其发送给用户，这要快得多。它可以与 S3 存储桶或任何 PSR7 流一起使用。',
      updatedAt: '2022-01-07',
      member: 'Jonatan Männchen',
      href: 'https://github.com/maennchen/ZipStream-PHP',
    },
    {
      id: 14,
      title: 'overtrue/pinyin 4.0',
      logo: 'https://avatars.githubusercontent.com/u/15724152?s=48&v=4',
      description:
        '🇨🇳 基于 mozillazg/pinyin-data 词典的中文转拼音工具，更准确的支持多音字的汉字转拼音解决方案。',
      updatedAt: '2022-01-07',
      member: 'Mao Wang',
      href: 'https://github.com/overtrue/pinyin',
    },
    {
      id: 15,
      title: 'mews/captcha 3.2',
      logo: 'https://avatars.githubusercontent.com/u/2125733?v=4',
      description: '一个php的验证码扩展',
      updatedAt: '2022-01-07',
      member: 'mewebstudio',
      href: 'https://github.com/mewebstudio/captcha',
    },
  ];
  const webList = [
    {
      id: 1,
      title: 'Ant Design Pro 5.0',
      logo: 'https://gw.alipayobjects.com/zos/rmsportal/KDpgvguMpGfqaHPjicRK.svg',
      description: '开箱即用的中台前端/设计解决方案',
      updatedAt: '2022-01-07',
      member: '阿里',
      href: 'https://pro.ant.design/zh-CN',
    },
    {
      id: 2,
      title: 'React 17.0.0',
      logo: 'data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9Ii0xMS41IC0xMC4yMzE3NCAyMyAyMC40NjM0OCI+CiAgPHRpdGxlPlJlYWN0IExvZ288L3RpdGxlPgogIDxjaXJjbGUgY3g9IjAiIGN5PSIwIiByPSIyLjA1IiBmaWxsPSIjNjFkYWZiIi8+CiAgPGcgc3Ryb2tlPSIjNjFkYWZiIiBzdHJva2Utd2lkdGg9IjEiIGZpbGw9Im5vbmUiPgogICAgPGVsbGlwc2Ugcng9IjExIiByeT0iNC4yIi8+CiAgICA8ZWxsaXBzZSByeD0iMTEiIHJ5PSI0LjIiIHRyYW5zZm9ybT0icm90YXRlKDYwKSIvPgogICAgPGVsbGlwc2Ugcng9IjExIiByeT0iNC4yIiB0cmFuc2Zvcm09InJvdGF0ZSgxMjApIi8+CiAgPC9nPgo8L3N2Zz4K',
      description: '用于构建用户界面的 JavaScript 库',
      updatedAt: '2022-01-07',
      member: 'Facebook',
      href: 'https://react.docschina.org/',
    },
    {
      id: 3,
      title: 'TypeScript',
      logo: '	https://typescript.bootcss.com/images/typescript-icon.svg',
      description:
        'TypeScript具有类型系统，且是JavaScript的超集。 它可以编译成普通的JavaScript代码。',
      updatedAt: '2022-01-07',
      member: 'Microsoft',
      href: 'https://typescript.bootcss.com/',
    },
    {
      id: 4,
      title: 'antd 4.16.13',
      logo: 'https://gw.alipayobjects.com/zos/rmsportal/KDpgvguMpGfqaHPjicRK.svg',
      description:
        'antd 是基于 Ant Design 设计体系的 React UI 组件库，主要用于研发企业级中后台产品。',
      updatedAt: '2022-01-07',
      member: '阿里',
      href: 'https://ant.design/index-cn',
    },
    {
      id: 5,
      title: 'UmiJS 3.5.0',
      logo: 'https://img.alicdn.com/tfs/TB1zomHwxv1gK0jSZFFXXb0sXXa-200-200.png',
      description: '插件化的企业级前端应用框架。',
      updatedAt: '2022-01-07',
      member: '阿里',
      href: 'https://umijs.org/zh-CN',
    },
    {
      id: 6,
      title: 'Pro Components',
      logo: 'https://gw.alipayobjects.com/zos/antfincdn/upvrAjAPQX/Logo_Tech%252520UI.svg',
      description:
        'ProComponents 是基于 Ant Design 而开发的模板组件，提供了更高级别的抽象支持，开箱即用。',
      updatedAt: '2022-01-07',
      member: '阿里',
      href: 'https://procomponents.ant.design/',
    },
    {
      id: 7,
      title: 'react-sortable-hoc 2.0.0',
      logo: 'https://avatars.githubusercontent.com/u/1416436?s=48&v=4',
      description: '拖拽插件，一组高阶组件，可将任何列表转换为动画、可访问且触摸友好的可排序列表',
      updatedAt: '2022-01-07',
      member: 'Shopify',
      href: 'https://github.com/clauderic/react-sortable-hoc',
    },
    {
      id: 8,
      title: 'braft-editor 2.3.9',
      logo: 'https://avatars.githubusercontent.com/u/18643034?s=48&v=4',
      description: '关于编辑易用的React富文本编辑器器，基于draft-js开发',
      updatedAt: '2022-01-07',
      member: 'Psaren',
      href: 'https://github.com/margox/braft-editor',
    },
    {
      id: 9,
      title: 'react-json-view 1.21.3',
      logo: 'https://avatars.githubusercontent.com/u/4097374?s=48&v=4',
      description: '一个用于显示和编辑 javascript数组和JSON 对象的 React 组件。',
      updatedAt: '2022-01-07',
      member: 'Mac Gainor',
      href: 'https://github.com/mac-s-g/react-json-view',
    },
    {
      id: 10,
      title: 'react-monaco-editor 0.48.0',
      logo: 'https://avatars.githubusercontent.com/u/49051982?s=200&v=4',
      description: 'React 的 Monaco 编辑器。',
      updatedAt: '2022-01-07',
      member: 'react-monaco-editor',
      href: 'https://github.com/react-monaco-editor/react-monaco-editor',
    },
  ];
  const planList = [
    {
      id: 1,
      logo: '/logo.png',
      description: '后台系统管理',
      status: 1,
      updatedAt: '2022-1-10',
    },
    {
      id: 2,
      logo: '/logo.png',
      description: '古诗文小程序',
      status: 1,
      updatedAt: '2022-3-10',
    },
  ];
  return (
    <PageContainer
      title={false}
      content={
        <div className={styles.pageHeaderContent}>
          <div className={styles.avatar}>
            <Avatar size="large" src="/logo.png" />
          </div>
          <div className={styles.content}>
            <div className={styles.contentTitle}>您好 ，欢迎来到后台管理，祝你开心每一天！</div>
            <div>
              后台管理是基于最新Laravel 8.5框架和Ant Design Pro 5.0的后台管理系统。创立于2022年初。
            </div>
          </div>
        </div>
      }
    >
      <Row gutter={24}>
        <Col xl={24} lg={24} md={24} sm={24} xs={24}>
          <Card
            className={styles.projectList}
            style={{ marginBottom: 24 }}
            title="联系方式"
            bordered={false}
            bodyStyle={{ padding: 0 }}
          >
            <Card.Grid className={styles.projectGrid}>
              <Card bodyStyle={{ padding: 0 }} bordered={false}>
                <Card.Meta title="公众号" />
                <div className={styles.projectItemContent}>
                  <span className={styles.datetime}>咪乐多</span>
                </div>
                <div className={styles.imageBox}>
                  <Image width={200} src={gzhImage} />
                </div>
              </Card>
            </Card.Grid>
            <Card.Grid className={styles.projectGrid}>
              <Card bodyStyle={{ padding: 0 }} bordered={false}>
                <Card.Meta title="小程序" />
                <div className={styles.projectItemContent}>
                  <span className={styles.datetime}>优咪乐</span>
                </div>
                <div className={styles.imageBox}>
                  <Image width={200} src={xcxImage} />
                </div>
              </Card>
            </Card.Grid>
            <Card.Grid className={styles.projectGrid}>
              <Card bodyStyle={{ padding: 0 }} bordered={false}>
                <Card.Meta title="作者微信（有微信群）" />
                <div className={styles.projectItemContent}>
                  <span className={styles.datetime}>bosong236589</span>
                </div>
                <div className={styles.imageBox}>
                  <Image width={200} src={wxImage} />
                </div>
              </Card>
            </Card.Grid>
            <Card.Grid className={styles.projectGrid}>
              <Card bodyStyle={{ padding: 0 }} bordered={false}>
                <Card.Meta title="码云地址" />
                <div className={styles.projectItemContent}>
                  <span className={styles.datetime}>
                    <a href="https://gitee.com/song-bo/back-end-of-ancient-poetry" target="_blank">
                      https://gitee.com/song-bo/back-end-of-ancient-poetry
                    </a>
                  </span>
                </div>
              </Card>
            </Card.Grid>
          </Card>

          <Card
            className={styles.projectList}
            style={{ marginBottom: 24 }}
            title="前端框架/插件"
            bordered={false}
            bodyStyle={{ padding: 0 }}
          >
            {webList.map((item) => (
              <Card.Grid className={styles.projectGrid} key={item.id}>
                <Card bodyStyle={{ padding: 0 }} bordered={false}>
                  <Card.Meta
                    title={
                      <div className={styles.cardTitle}>
                        <Avatar size="small" src={item.logo} />
                        <a href={item.href} target="_blank">
                          {item.title}
                        </a>
                      </div>
                    }
                    description={item.description}
                  />
                  <div className={styles.projectItemContent}>
                    <a>{item.member}</a>
                    <span className={styles.datetime} title={item.updatedAt}>
                      {item.updatedAt}
                    </span>
                  </div>
                </Card>
              </Card.Grid>
            ))}
          </Card>

          <Card
            className={styles.projectList}
            style={{ marginBottom: 24 }}
            title="后端语言/框架/插件"
            bordered={false}
            bodyStyle={{ padding: 0 }}
          >
            {serverList.map((item) => (
              <Card.Grid className={styles.projectGrid} key={item.id}>
                <Card bodyStyle={{ padding: 0 }} bordered={false}>
                  <Card.Meta
                    title={
                      <div className={styles.cardTitle}>
                        <Avatar size="small" src={item.logo} />
                        <a href={item.href} target="_blank">
                          {item.title}
                        </a>
                      </div>
                    }
                    description={item.description}
                  />
                  <div className={styles.projectItemContent}>
                    <a>{item.member}</a>
                    <span className={styles.datetime} title={item.updatedAt}>
                      {item.updatedAt}
                    </span>
                  </div>
                </Card>
              </Card.Grid>
            ))}
          </Card>

          <Card
            bodyStyle={{ padding: 0 }}
            bordered={false}
            className={styles.activeCard}
            title="动态"
          >
            <List
              renderItem={(item: any) => {
                return (
                  <List.Item key={item.id}>
                    <List.Item.Meta
                      avatar={<Avatar src={item.logo} />}
                      title={<span className={styles.event}>{item.description}</span>}
                      description={
                        <span className={styles.datetime} title={item.updatedAt}>
                          {item.updatedAt}
                        </span>
                      }
                    />
                    {item.status === 1 ? <div>已完成</div> : <div>开发中</div>}
                  </List.Item>
                );
              }}
              dataSource={planList}
              className={styles.activitiesList}
              size="large"
            />
          </Card>
        </Col>
      </Row>
    </PageContainer>
  );
};
export default Index;
