import React, { useRef, useState } from 'react';
import { Button, message, Form, Input, Radio, Spin, InputNumber, Select } from 'antd';
import { DrawerForm } from '@ant-design/pro-form';
import type { ProFormInstance } from '@ant-design/pro-form';
import { PlusOutlined, FormOutlined } from '@ant-design/icons';
import { add, edit, update } from '@/services/toolAdmin/setting/picture/api';
import CUpload from '@/components/common/CUpload/index';
import CBraftEditor from '@/components/common/CBraftEditor';
interface PorpsType {
  onConfirm: () => void;
  id?: number;
}
const FormIndex: React.FC<PorpsType> = (porps) => {
  const restFormRef = useRef<ProFormInstance>();
  const [loading, setLoading] = useState(porps.id ? false : true);
  const formData: API.ToolAdminPictureForm = {
    id: null,
    content: '',
    status: 1,
    sort: 1,
    url: '',
    image_id: null,
    type: null,
    open: null,
  };
  return (
    <DrawerForm<API.ToolAdminPictureForm>
      layout="horizontal"
      labelCol={{ span: 3 }}
      wrapperCol={{ span: 20 }}
      formRef={restFormRef}
      initialValues={formData}
      title={porps.id ? '编辑' : '添加'}
      autoFocusFirstInput
      isKeyPressSubmit
      trigger={
        porps.id ? (
          <Button type="primary" size="small">
            <FormOutlined />
            编辑
          </Button>
        ) : (
          <Button type="primary">
            <PlusOutlined />
            添加
          </Button>
        )
      }
      drawerProps={{
        forceRender: true,
        destroyOnClose: true,
      }}
      submitter={{
        render: (props, defaultDoms) => {
          return [
            ...defaultDoms,
            <Button
              loading={loading ? false : true}
              key="extra-reset"
              onClick={async () => {
                const id = props.form?.getFieldValue('id');
                if (id) {
                  await setLoading(false);
                  const res = await edit(id);
                  if (res.status === 20000) {
                    restFormRef.current?.setFieldsValue(res.data);
                    await setLoading(true);
                  }
                } else {
                  props.reset();
                }
              }}
            >
              重置
            </Button>,
          ];
        },
      }}
      onFinish={async (values: any) => {
        if (!Number.isInteger(values.image_id)) {
          values.image_id = values.image_id && values.image_id[0].id;
        }
        if (!values.image_id) {
          values.image_id = null;
        }
        let res: any = {};
        if (porps.id) {
          res = await update(porps.id, values);
        } else {
          res = await add(values);
        }
        if (res.status === 20000) {
          message.success(res.message);
          porps.onConfirm();
          return true;
        }
        return false;
      }}
      onVisibleChange={async (visible) => {
        if (visible && porps.id) {
          await setLoading(false);
          const res = await edit(porps.id);
          if (res.status === 20000) {
            await restFormRef.current?.setFieldsValue(res.data);
            await setLoading(true);
          }
        }
      }}
    >
      <Spin spinning={loading ? false : true}>
        {loading && (
          <>
            <Form.Item
              label="图片类型"
              name="type"
              rules={[{ required: true, message: '请选择图片类型' }]}
            >
              <Select allowClear placeholder="请选择图片类型">
                <Select.Option value={1}>首页轮播图</Select.Option>
              </Select>
            </Form.Item>
            <Form.Item
              label="打开方式"
              name="open"
              rules={[{ required: true, message: '请选择打开方式' }]}
            >
              <Select allowClear placeholder="请选择打开方式">
                <Select.Option value={1}>小程序链接</Select.Option>
                <Select.Option value={2}>网页地址</Select.Option>
                <Select.Option value={3}>详情展示</Select.Option>
              </Select>
            </Form.Item>
            <Form.Item
              name="image_id"
              label="图片"
              rules={[{ required: true, message: '请选择图片' }]}
            >
              <CUpload
                key="image_id"
                imageList={
                  restFormRef.current?.getFieldValue('image_to')
                    ? [restFormRef.current.getFieldValue('image_to')]
                    : []
                }
                onChange={(data: any) => {
                  if (!data.length) {
                    restFormRef.current?.setFieldsValue({ image_id: null });
                  }
                }}
              />
            </Form.Item>
            <Form.Item label="链接地址" name="url">
              <Input allowClear placeholder="请输入链接地址" />
            </Form.Item>
            <Form.Item label="详情" name="content">
              <CBraftEditor
                value={formData.content}
                onChange={async (value: string) => {
                  await restFormRef.current?.setFieldsValue({ content: value });
                }}
              />
            </Form.Item>
            <Form.Item
              name="status"
              label="状态"
              rules={[{ required: true, message: '请选择状态' }]}
            >
              <Radio.Group name="status">
                <Radio value={0}>禁用</Radio>
                <Radio value={1}>启用</Radio>
              </Radio.Group>
            </Form.Item>
            <Form.Item label="排序" name="sort" rules={[{ required: true, message: '请输入排序' }]}>
              <InputNumber
                maxLength={11}
                placeholder="请输入排序"
                min={0}
                style={{ width: '100%' }}
              />
            </Form.Item>
          </>
        )}
      </Spin>
    </DrawerForm>
  );
};
export default FormIndex;
