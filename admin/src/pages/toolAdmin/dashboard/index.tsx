import React from 'react';
import { Avatar } from 'antd';
import { PageContainer } from '@ant-design/pro-layout';
import styles from './index.less';
const Index: React.FC = () => {
  return (
    <PageContainer
      title={false}
      content={
        <div className={styles.pageHeaderContent}>
          <div className={styles.avatar}>
            <Avatar size="large" src="/logo.png" />
          </div>
          <div className={styles.content}>
            <div className={styles.contentTitle}>您好，欢迎来到后台管理，祝你开心每一天！</div>
            <div>
              后台管理是基于最新Laravel 8.5框架和Ant Design Pro 5.0的后台管理系统。创立于2022年初。
            </div>
          </div>
        </div>
      }
    />
  );
};
export default Index;
