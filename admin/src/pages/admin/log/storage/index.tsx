import React, { useRef, useState } from 'react';
import type { ActionType, ProColumns } from '@ant-design/pro-table';
import ProTable from '@ant-design/pro-table';
import { getRestoreData, del } from '@/services/admin/log/storage/api';
import { Space, message, Button } from 'antd';
import { PageContainer } from '@ant-design/pro-layout';
import { exportExcel } from '@/utils/ExportExcel';
import FileDetails from './components/FileDetails';

import { DownloadOutlined } from '@ant-design/icons';
import CDel from '@/components/common/CDel';
import CDelAll from '@/components/common/CDelAll';

const Index: React.FC = () => {
  const actionRef = useRef<ActionType>();
  const [tableData, setTableData] = useState<any>(null);
  let maps = {};
  const columns: ProColumns<API.AdminDataBaseRestoreDataList>[] = [
    {
      title: '文件名称',
      dataIndex: 'name',
      align: 'center',
      width: 150,
      fixed: true,
      hideInSearch: true,
      render: (text, record) => <FileDetails name={record.name} />,
    },
    {
      title: '链接',
      dataIndex: 'url',
      copyable: true,
      ellipsis: true,
      width: 150,
      hideInSearch: true,
      align: 'center',
      tip: '链接过长会自动收缩',
    },
    {
      title: '占用空间',
      dataIndex: 'size',
      align: 'center',
      hideInSearch: true,
      width: 80,
    },
    {
      title: '创建时间',
      dataIndex: 'date',
      align: 'center',
      hideInSearch: true,
      width: 100,
    },
    {
      title: '操作',
      key: 'option',
      width: 100,
      valueType: 'option',
      align: 'center',
      fixed: 'right',
      render: (_, row) => [
        <Button
          key={row.name}
          type="primary"
          icon={<DownloadOutlined />}
          size="small"
          href={row.url}
          download={row.name}
        >
          下载
        </Button>,
        <CDel
          key={row.name}
          onCancel={async () => {
            const res = await del({ nameArr: [row.name] });
            if (res.status === 20000) {
              message.success(res.message);
              actionRef.current?.reload();
            }
          }}
        />,
      ],
    },
  ];
  return (
    <PageContainer fixedHeader={true} title={false}>
      <ProTable
        scroll={{ x: 1000 }}
        actionRef={actionRef}
        rowKey="name"
        columns={columns}
        pagination={false}
        search={false}
        request={async () => {
          const res = await getRestoreData();
          await setTableData(res.data);
          return { success: true, data: res.data.data };
        }}
        options={{ fullScreen: true }}
        rowSelection={{ fixed: true }}
        tableAlertRender={({ selectedRowKeys, onCleanSelected }) => (
          <Space size={24}>
            <span>
              已选 {selectedRowKeys.length} 项
              <a style={{ marginLeft: 8 }} onClick={onCleanSelected}>
                取消选择
              </a>
            </span>
          </Space>
        )}
        columnsState={{
          onChange: (map) => {
            maps = map;
          },
        }}
        tableAlertOptionRender={({ selectedRows, selectedRowKeys, onCleanSelected }) => {
          return (
            <Space size={16}>
              <CDelAll
                key={selectedRowKeys.length}
                count={selectedRowKeys.length}
                onCancel={async () => {
                  const res = await del({ nameArr: selectedRowKeys });
                  if (res.status === 20000) {
                    message.success(res.message);
                    actionRef.current?.reload();
                    onCleanSelected();
                  }
                }}
              />
              <a
                onClick={() => {
                  exportExcel({ fileName: '备份文件', columns, maps, selectedRows });
                }}
              >
                导出数据
              </a>
            </Space>
          );
        }}
        headerTitle={
          tableData && (
            <>
              <Space size={24}>
                <span>
                  共计 {tableData.tableNum} 个文件（{tableData.total}）
                </span>
              </Space>
            </>
          )
        }
      />
    </PageContainer>
  );
};
export default Index;
