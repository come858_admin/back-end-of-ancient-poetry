import React, { useRef, useState } from 'react';
import { Button, message, Form, Input, Radio, Spin, Cascader, Select } from 'antd';
import { DrawerForm } from '@ant-design/pro-form';
import type { ProFormInstance } from '@ant-design/pro-form';
import { PlusOutlined, FormOutlined } from '@ant-design/icons';
import { add, edit, update } from '@/services/admin/information/archive/api';
import { getPidList } from '@/services/admin/information/archiveType/api';
import { getPersonnelList } from '@/services/admin/information/personnel/api';
import CUpload from '@/components/common/CUpload/index';
interface PorpsType {
  onConfirm: () => void;
  id?: number;
}
const FormIndex: React.FC<PorpsType> = (porps) => {
  const restFormRef = useRef<ProFormInstance>();
  const [loading, setLoading] = useState(porps.id ? false : true);
  const [typeList, setTypeList] = useState<any>([]);
  const [typeListLoading, setTypeListLoading] = useState<boolean>(false);
  const [typeArr, settypeArr] = useState<any>([]);
  const [personnelList, setPersonnelList] = useState<any>([]);
  const [personnelListLoading, setPersonnelListLoading] = useState<boolean>(false);
  const formData: API.AdminArchiveForm = {
    id: null,
    status: 1,
    personnel_id: null,
    archive_type_id: null,
    description: '',
    created_at: '',
    images: [],
  };
  return (
    <DrawerForm<API.AdminProjectForm>
      layout="horizontal"
      labelCol={{ span: 3 }}
      wrapperCol={{ span: 20 }}
      formRef={restFormRef}
      initialValues={formData}
      title={porps.id ? '编辑' : '添加'}
      autoFocusFirstInput
      isKeyPressSubmit
      trigger={
        porps.id ? (
          <Button type="primary" size="small">
            <FormOutlined />
            编辑
          </Button>
        ) : (
          <Button type="primary">
            <PlusOutlined />
            添加
          </Button>
        )
      }
      drawerProps={{
        forceRender: true,
        destroyOnClose: true,
      }}
      submitter={{
        render: (props, defaultDoms) => {
          return [
            ...defaultDoms,
            <Button
              loading={loading ? false : true}
              key="extra-reset"
              onClick={async () => {
                const id = props.form?.getFieldValue('id');
                if (id) {
                  await setLoading(false);
                  const res = await edit(id);
                  if (res.status === 20000) {
                    restFormRef.current?.setFieldsValue(res.data);
                    await settypeArr(res.data.type_arr);
                    await setLoading(true);
                  }
                } else {
                  props.reset();
                }
              }}
            >
              重置
            </Button>,
          ];
        },
      }}
      onFinish={async (values: any) => {
        let res: any = {};
        if (porps.id) {
          res = await update(porps.id, values);
        } else {
          res = await add(values);
        }
        if (res.status === 20000) {
          message.success(res.message);
          porps.onConfirm();
          return true;
        }
        return false;
      }}
      onVisibleChange={async (visible) => {
        if (visible) {
          if (porps.id) {
            await setLoading(false);
            const res = await edit(porps.id);
            if (res.status === 20000) {
              await restFormRef.current?.setFieldsValue(res.data);
              await settypeArr(res.data.type_arr);
              await setLoading(true);
            }
          }
          await setTypeListLoading(true);
          const typeRes = await getPidList({});
          if (typeRes.status == 20000) {
            await setTypeListLoading(false);
            await setTypeList(typeRes.data);
          }
          await setPersonnelListLoading(true);
          const routeRes = await getPersonnelList();
          if (routeRes.status == 20000) {
            await setPersonnelListLoading(false);
            await setPersonnelList(routeRes.data);
          }
        }
      }}
    >
      <Spin spinning={loading ? false : true}>
        {loading && (
          <>
            <Form.Item
              label="人员"
              name="personnel_id"
              rules={[{ required: true, message: '请选择人员' }]}
            >
              <Select
                allowClear={true}
                showSearch={true}
                filterOption={(input, option: any) => {
                  return option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0;
                }}
                loading={personnelListLoading}
                notFoundContent="暂无人员"
                placeholder="请选择人员"
              >
                {personnelList.map((item: any) => {
                  return (
                    <Select.Option key={item.id} value={item.id}>
                      {item.name}
                    </Select.Option>
                  );
                })}
              </Select>
            </Form.Item>
            <Spin spinning={typeListLoading}>
              <Form.Item
                label="模板"
                name="archive_type_id"
                rules={[{ required: true, message: '请选择模板' }]}
              >
                {typeListLoading === false && (
                  <>
                    <Cascader
                      options={typeList}
                      defaultValue={typeArr}
                      allowClear
                      placeholder="请选择模板"
                      onChange={async (value) => {
                        if (value.length) {
                          await restFormRef.current?.setFieldsValue({
                            archive_type_id: value[value.length - 1],
                          });
                        } else {
                          await restFormRef.current?.setFieldsValue({ archive_type_id: null });
                        }
                      }}
                      fieldNames={{ value: 'id', label: 'name' }}
                    />
                  </>
                )}
              </Form.Item>
            </Spin>
            <Form.Item
              name="images"
              label="文件"
              rules={[{ required: true, message: '请选择文件' }]}
            >
              <CUpload
                key="images"
                count={9}
                imageList={
                  restFormRef.current?.getFieldValue('images')
                    ? restFormRef.current.getFieldValue('images')
                    : []
                }
                onChange={(data: any) => {
                  if (!data.length) {
                    restFormRef.current?.setFieldsValue({ images: [] });
                  }
                }}
              />
            </Form.Item>
            <Form.Item label="描述" name="description">
              <Input maxLength={100} allowClear placeholder="请输入描述" />
            </Form.Item>
            <Form.Item
              name="status"
              label="状态"
              rules={[{ required: true, message: '请选择状态' }]}
            >
              <Radio.Group name="status">
                <Radio value={0}>禁用</Radio>
                <Radio value={1}>启用</Radio>
              </Radio.Group>
            </Form.Item>
          </>
        )}
      </Spin>
    </DrawerForm>
  );
};
export default FormIndex;
