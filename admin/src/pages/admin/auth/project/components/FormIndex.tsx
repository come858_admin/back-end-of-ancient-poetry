import React, { useRef, useState } from 'react';
import { Button, message, Form, Input, Radio, Spin, Select } from 'antd';
import { DrawerForm } from '@ant-design/pro-form';
import type { ProFormInstance } from '@ant-design/pro-form';
import { PlusOutlined, FormOutlined } from '@ant-design/icons';
import CUpload from '@/components/common/CUpload/index';
import { add, edit, update } from '@/services/admin/auth/project/api';
interface PorpsType {
  onConfirm: () => void;
  id?: number;
}
const FormIndex: React.FC<PorpsType> = (porps) => {
  const restFormRef = useRef<ProFormInstance>();
  const [loading, setLoading] = useState(porps.id ? false : true);
  const { TextArea } = Input;
  const formData: API.AdminProjectForm = {
    id: null,
    name: '',
    url: '',
    description: '',
    keywords: '',
    status: 1,
    logo_id: null,
    ico_id: null,
    type: null,
  };
  return (
    <DrawerForm<API.AdminProjectForm>
      layout="horizontal"
      labelCol={{ span: 3 }}
      wrapperCol={{ span: 20 }}
      formRef={restFormRef}
      initialValues={formData}
      title={porps.id ? '编辑' : '添加'}
      autoFocusFirstInput
      isKeyPressSubmit
      trigger={
        porps.id ? (
          <Button type="primary" size="small">
            <FormOutlined />
            编辑
          </Button>
        ) : (
          <Button type="primary">
            <PlusOutlined />
            添加
          </Button>
        )
      }
      drawerProps={{
        forceRender: true,
        destroyOnClose: true,
      }}
      submitter={{
        render: (props, defaultDoms) => {
          return [
            ...defaultDoms,
            <Button
              loading={loading ? false : true}
              key="extra-reset"
              onClick={async () => {
                const id = props.form?.getFieldValue('id');
                if (id) {
                  await setLoading(false);
                  const res = await edit(id);
                  if (res.status === 20000) {
                    restFormRef.current?.setFieldsValue(res.data);
                    await setLoading(true);
                  }
                } else {
                  props.reset();
                }
              }}
            >
              重置
            </Button>,
          ];
        },
      }}
      onFinish={async (values: any) => {
        if (!Number.isInteger(values.ico_id)) {
          values.ico_id = values.ico_id && values.ico_id[0].id;
        }
        if (!Number.isInteger(values.logo_id)) {
          values.logo_id = values.logo_id && values.logo_id[0].id;
        }
        let res: any = {};
        if (porps.id) {
          res = await update(porps.id, values);
        } else {
          res = await add(values);
        }
        if (res.status === 20000) {
          message.success(res.message);
          porps.onConfirm();
          return true;
        }
        return false;
      }}
      onVisibleChange={async (visible) => {
        if (visible && porps.id) {
          await setLoading(false);
          const res = await edit(porps.id);
          if (res.status === 20000) {
            await restFormRef.current?.setFieldsValue(res.data);
            await setLoading(true);
          }
        }
      }}
    >
      <Spin spinning={loading ? false : true}>
        {loading && (
          <>
            <Form.Item
              label="项目类型"
              name="type"
              rules={[{ required: true, message: '请选择项目类型' }]}
            >
              <Select allowClear placeholder="请选择项项目类型">
                <Select.Option value={1}>内容管理</Select.Option>
                <Select.Option value={2}>企业官网</Select.Option>
                <Select.Option value={3}>数据中心</Select.Option>
                <Select.Option value={4}>其他数据</Select.Option>
                <Select.Option value={5}>快应用管理</Select.Option>
                <Select.Option value={6}>站点分发</Select.Option>
                <Select.Option value={7}>小说管理</Select.Option>
                <Select.Option value={8}>快速建站</Select.Option>
                <Select.Option value={9}>阅读</Select.Option>
              </Select>
            </Form.Item>
            <Form.Item
              label="项目名称"
              name="name"
              rules={[{ required: true, message: '请输入项目名称' }]}
            >
              <Input maxLength={100} allowClear placeholder="请输入项目名称" />
            </Form.Item>
            <Form.Item
              name="logo_id"
              label="logo图"
              rules={[{ required: true, message: '请选择logo图' }]}
            >
              <CUpload
                key="logo_id"
                imageList={
                  restFormRef.current?.getFieldValue('logo_image')
                    ? [restFormRef.current.getFieldValue('logo_image')]
                    : []
                }
              />
            </Form.Item>
            <Form.Item
              name="ico_id"
              label="站点标识"
              rules={[{ required: true, message: '请选择站点标识' }]}
            >
              <CUpload
                key="ico_id"
                imageList={
                  restFormRef.current?.getFieldValue('ico_image')
                    ? [restFormRef.current.getFieldValue('ico_image')]
                    : []
                }
              />
            </Form.Item>
            <Form.Item
              label="项目网址"
              name="url"
              rules={[{ required: true, message: '请输入项目网址' }]}
            >
              <Input maxLength={100} allowClear placeholder="请输入项目网址" />
            </Form.Item>
            <Form.Item
              label="项目描述"
              name="description"
              rules={[{ required: true, message: '请输入项目描述' }]}
            >
              <TextArea showCount maxLength={100} placeholder="请输入项目描述" />
            </Form.Item>
            <Form.Item
              label="项目关键词"
              name="keywords"
              rules={[{ required: true, message: '请输入项目关键词' }]}
            >
              <TextArea showCount maxLength={100} placeholder="请输入项目关键词" />
            </Form.Item>
            <Form.Item
              name="status"
              label="状态"
              rules={[{ required: true, message: '请选择状态' }]}
            >
              <Radio.Group name="status">
                <Radio value={0}>禁用</Radio>
                <Radio value={1}>启用</Radio>
              </Radio.Group>
            </Form.Item>
          </>
        )}
      </Spin>
    </DrawerForm>
  );
};
export default FormIndex;
