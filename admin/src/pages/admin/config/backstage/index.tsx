import React, { useEffect, useRef, useState } from 'react';
import ProForm from '@ant-design/pro-form';
import type { ProFormInstance } from '@ant-design/pro-form';
import { Button, message, Form, Input, Radio, Spin, Card } from 'antd';
import { PageContainer } from '@ant-design/pro-layout';
import CUpload from '@/components/common/CUpload/index';
import { getConfig, setConfig } from '@/services/admin/config/config/api';
interface ValueType {
  name: string;
  image_status: number | null;
  logo_id: number | null;
  keywords: string;
  description: string;
}
interface PorpsType {
  code: string;
  value: ValueType;
}
const Index: React.FC = () => {
  const formData: PorpsType = {
    code: 'SiteConfiguration',
    value: {
      name: '',
      image_status: 1,
      logo_id: null,
      keywords: '',
      description: '',
    },
  };

  const { TextArea } = Input;
  const restFormRef = useRef<ProFormInstance<ValueType>>();
  const [loading, setLoading] = useState(true);
  const [submitLoading, setSubmitLoading] = useState(true);
  const fetchApi = async () => {
    await setLoading(false);
    const res = await getConfig(formData.code);
    if (res.status == 20000) {
      await restFormRef.current?.setFieldsValue(res.data);
      await setLoading(true);
    }
  };
  useEffect(() => {
    fetchApi();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <PageContainer
      fixedHeader={true}
      title={false}
      footer={[
        <Button
          key={1}
          loading={loading ? false : true}
          onClick={async () => {
            await fetchApi();
          }}
        >
          重置
        </Button>,
        <Button
          key={2}
          type="primary"
          loading={submitLoading ? false : true}
          onClick={() => {
            restFormRef.current?.submit();
          }}
        >
          提交
        </Button>,
      ]}
    >
      <Card>
        <ProForm<ValueType>
          layout="horizontal"
          labelCol={{ span: 2 }}
          wrapperCol={{ span: 22 }}
          autoFocusFirstInput
          isKeyPressSubmit
          formRef={restFormRef}
          initialValues={formData.value}
          submitter={false}
          onFinish={async (values: any) => {
            await setSubmitLoading(false);
            if (!Number.isInteger(values.logo_id)) {
              values.logo_id = values.logo_id && values.logo_id[0].id;
            }
            const res = await setConfig(formData.code, { value: values });
            if (res.status === 20000) {
              message.success(res.message);
            }
            await setSubmitLoading(true);
          }}
        >
          <Spin spinning={loading ? false : true}>
            {loading && (
              <>
                <Form.Item
                  label="项目名称"
                  name="name"
                  rules={[{ required: true, message: '请输入项目名称' }]}
                >
                  <Input maxLength={100} allowClear placeholder="请输入项目名称" />
                </Form.Item>
                <Form.Item
                  name="logo_id"
                  label="logo图"
                  rules={[{ required: true, message: '请选择logo图' }]}
                >
                  <CUpload
                    key="logo_id"
                    imageList={
                      restFormRef.current?.getFieldValue('logo_image')
                        ? [restFormRef.current.getFieldValue('logo_image')]
                        : []
                    }
                  />
                </Form.Item>
                <Form.Item
                  label="项目描述"
                  name="description"
                  rules={[{ required: true, message: '请输入项目描述' }]}
                >
                  <TextArea showCount maxLength={100} placeholder="请输入项目描述" />
                </Form.Item>
                <Form.Item
                  label="项目关键词"
                  name="keywords"
                  rules={[{ required: true, message: '请输入项目关键词' }]}
                >
                  <TextArea showCount maxLength={100} placeholder="请输入项目关键词" />
                </Form.Item>
                <Form.Item
                  name="image_status"
                  label="图片储存"
                  rules={[{ required: true, message: '请选择图片储存' }]}
                >
                  <Radio.Group name="image_status">
                    <Radio value={1}>本地</Radio>
                    <Radio value={2}>七牛云</Radio>
                    <Radio value={3}>阿里云OSS</Radio>
                  </Radio.Group>
                </Form.Item>
              </>
            )}
          </Spin>
        </ProForm>
      </Card>
    </PageContainer>
  );
};
export default Index;
