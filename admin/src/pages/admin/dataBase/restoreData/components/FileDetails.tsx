import React, { useState } from 'react';
import { Modal, Spin } from 'antd';
import { getFiles } from '@/services/admin/dataBase/api';
import MonacoEditor from 'react-monaco-editor';
interface PorpsType {
  name: string;
}
const FileDetails: React.FC<PorpsType> = (porps) => {
  const [isModalVisible, setIsModalVisible] = useState<boolean>(false);
  const [fileInfo, setFileInfo] = useState<string>('');
  return (
    <>
      <a
        onClick={async () => {
          await setIsModalVisible(true);
          const res = await getFiles({ name: porps.name });
          if (res.status === 20000) {
            await setFileInfo(res.data.content);
          }
        }}
      >
        {porps.name}
      </a>
      <Modal
        title={'文件' + porps.name + '详情'}
        visible={isModalVisible}
        width={750}
        footer={null}
        onCancel={() => {
          setIsModalVisible(false);
        }}
      >
        <Spin spinning={fileInfo ? false : true}>
          {fileInfo && (
            <MonacoEditor
              width="700"
              height="400"
              language="sql"
              theme="vs-dark"
              value={fileInfo}
              options={{
                wordWrap: 'on', // 自动换行
                acceptSuggestionOnEnter: 'smart',
                bracketPairColorization: { enabled: true },
                language: 'sql',
                unicodeHighlight: {
                  ambiguousCharacters: false,
                },
              }}
              editorDidMount={(editor) => {
                editor.focus();
              }}
            />
          )}
        </Spin>
      </Modal>
    </>
  );
};
export default FileDetails;
