import React, { useRef, useState } from 'react';
import type { ActionType, ProColumns } from '@ant-design/pro-table';
import ProTable from '@ant-design/pro-table';
import { getTables, setBackUp } from '@/services/admin/dataBase/api';
import { Space, message } from 'antd';
import { PageContainer } from '@ant-design/pro-layout';
import { exportExcel } from '@/utils/ExportExcel';
import TableDetails from './components/TableDetails';
const Index: React.FC = () => {
  const actionRef = useRef<ActionType>();
  const [tableData, setTableData] = useState<any>(null);
  let maps = {};
  const columns: ProColumns<API.AdminDataBaseTablesList>[] = [
    {
      title: '数据表名',
      dataIndex: 'Name',
      align: 'center',
      width: 100,
      fixed: true,
      render: (text, record) => <TableDetails name={record.Name} />,
    },
    {
      title: '数据条数',
      dataIndex: 'Rows',
      align: 'center',
      hideInSearch: true,
      width: 60,
    },
    {
      title: '类型',
      dataIndex: 'Engine',
      align: 'center',
      hideInSearch: true,
      width: 40,
    },
    {
      title: '排序规则',
      dataIndex: 'Collation',
      align: 'center',
      hideInSearch: true,
      width: 80,
    },
    {
      title: '占用空间',
      dataIndex: 'size',
      align: 'center',
      hideInSearch: true,
      width: 80,
    },
    {
      title: '备注',
      dataIndex: 'Comment',
      align: 'center',
      hideInSearch: true,
      width: 100,
    },
    {
      title: '创建时间',
      dataIndex: 'Create_time',
      align: 'center',
      hideInSearch: true,
      width: 100,
    },
    {
      title: '更新时间',
      dataIndex: 'Update_time',
      align: 'center',
      hideInSearch: true,
      width: 100,
    },
  ];
  return (
    <PageContainer fixedHeader={true} title={false}>
      <ProTable
        scroll={{ x: 1300 }}
        actionRef={actionRef}
        rowKey="Name"
        columns={columns}
        pagination={false}
        request={async (params: API.ListQequest) => {
          const res = await getTables(params);
          await setTableData(res.data);
          return { success: true, data: res.data.data };
        }}
        options={{ fullScreen: true }}
        rowSelection={{ fixed: true }}
        tableAlertRender={({ selectedRowKeys, onCleanSelected }) => (
          <Space size={24}>
            <span>
              已选 {selectedRowKeys.length} 项
              <a style={{ marginLeft: 8 }} onClick={onCleanSelected}>
                取消选择
              </a>
            </span>
          </Space>
        )}
        columnsState={{
          onChange: (map) => {
            maps = map;
          },
        }}
        tableAlertOptionRender={({ selectedRows, selectedRowKeys }) => {
          return (
            <Space size={16}>
              <a
                onClick={async () => {
                  const res = await setBackUp({ tables: selectedRowKeys });
                  if (res.status === 20000) {
                    message.success(res.message);
                  }
                }}
              >
                备份数据
              </a>
              <a
                onClick={() => {
                  exportExcel({ fileName: '数据表', columns, maps, selectedRows });
                }}
              >
                导出数据
              </a>
            </Space>
          );
        }}
        headerTitle={
          tableData && (
            <>
              <Space size={24}>
                <span>
                  共计 {tableData.tableNum} 张表（{tableData.total}）
                </span>
              </Space>
            </>
          )
        }
      />
    </PageContainer>
  );
};
export default Index;
