import React, { useEffect, useRef, useState } from 'react';
import ProForm from '@ant-design/pro-form';
import type { ProFormInstance } from '@ant-design/pro-form';
import { Button, message, Form, Input, Radio, Spin, Card, Divider } from 'antd';
import { PageContainer } from '@ant-design/pro-layout';
import CUpload from '@/components/common/CUpload/index';
import { getInfo, setInfo } from '@/services/writingAdmin/setting/project/api';
import CBraftEditor from '@/components/common/CBraftEditor';
const Index: React.FC = () => {
  const formData: API.WritingAdminProjectForm = {
    name: '',
    logo_id: null,
    ico_id: null,
    url: '',
    description: '',
    keywords: '',
    status: 0,
    about: '',
    gzh_app_id: '',
    gzh_app_secret: '',
    xcx_app_id: '',
    xcx_app_secret: '',
  };

  const { TextArea } = Input;
  const restFormRef = useRef<ProFormInstance<API.WritingAdminProjectForm>>();
  const [loading, setLoading] = useState(true);
  const [submitLoading, setSubmitLoading] = useState(true);
  const fetchApi = async () => {
    await setLoading(false);
    const res = await getInfo();
    if (res.status == 20000) {
      await restFormRef.current?.setFieldsValue(res.data);
      await setLoading(true);
    }
  };
  useEffect(() => {
    fetchApi();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <PageContainer
      fixedHeader={true}
      title={false}
      footer={[
        <Button
          key={1}
          loading={loading ? false : true}
          onClick={async () => {
            await fetchApi();
          }}
        >
          重置
        </Button>,
        <Button
          key={2}
          type="primary"
          loading={submitLoading ? false : true}
          onClick={() => {
            restFormRef.current?.submit();
          }}
        >
          提交
        </Button>,
      ]}
    >
      <Card>
        <ProForm<API.DataAdminProjectForm>
          layout="horizontal"
          labelCol={{ span: 2 }}
          wrapperCol={{ span: 22 }}
          autoFocusFirstInput
          isKeyPressSubmit
          formRef={restFormRef}
          initialValues={formData}
          submitter={false}
          onFinish={async (values: any) => {
            await setSubmitLoading(false);
            if (!Number.isInteger(values.ico_id)) {
              values.ico_id = values.ico_id && values.ico_id[0].id;
            }
            if (!Number.isInteger(values.logo_id)) {
              values.logo_id = values.logo_id && values.logo_id[0].id;
            }
            const res = await setInfo(values);
            if (res.status === 20000) {
              message.success(res.message);
            }
            await setSubmitLoading(true);
          }}
        >
          <Spin spinning={loading ? false : true}>
            {loading && (
              <>
                <Divider orientation="left">基础配置</Divider>
                <Form.Item
                  label="项目名称"
                  name="name"
                  rules={[{ required: true, message: '请输入项目名称' }]}
                >
                  <Input maxLength={100} allowClear placeholder="请输入项目名称" />
                </Form.Item>
                <Form.Item
                  name="logo_id"
                  label="logo图"
                  rules={[{ required: true, message: '请选择logo图' }]}
                >
                  <CUpload
                    key="logo_id"
                    imageList={
                      restFormRef.current?.getFieldValue('logo_image')
                        ? [restFormRef.current.getFieldValue('logo_image')]
                        : []
                    }
                  />
                </Form.Item>
                <Form.Item
                  name="ico_id"
                  label="站点标识"
                  rules={[{ required: true, message: '请选择站点标识' }]}
                >
                  <CUpload
                    key="ico_id"
                    imageList={
                      restFormRef.current?.getFieldValue('ico_image')
                        ? [restFormRef.current.getFieldValue('ico_image')]
                        : []
                    }
                  />
                </Form.Item>
                <Form.Item
                  label="项目网址"
                  name="url"
                  rules={[{ required: true, message: '请输入项目网址' }]}
                >
                  <Input maxLength={100} allowClear placeholder="请输入项目网址" />
                </Form.Item>
                <Form.Item
                  label="项目描述"
                  name="description"
                  rules={[{ required: true, message: '请输入项目描述' }]}
                >
                  <TextArea showCount maxLength={100} placeholder="请输入项目描述" />
                </Form.Item>
                <Form.Item
                  label="项目关键词"
                  name="keywords"
                  rules={[{ required: true, message: '请输入项目关键词' }]}
                >
                  <TextArea showCount maxLength={100} placeholder="请输入项目关键词" />
                </Form.Item>
                <Form.Item
                  name="status"
                  label="状态"
                  rules={[{ required: true, message: '请选择状态' }]}
                >
                  <Radio.Group name="status">
                    <Radio value={0}>禁用</Radio>
                    <Radio value={1}>启用</Radio>
                  </Radio.Group>
                </Form.Item>
                <Divider orientation="left">公众号配置</Divider>
                <Form.Item
                  label="AppID"
                  name="gzh_app_id"
                  rules={[{ required: true, message: '请输入AppID' }]}
                >
                  <Input allowClear placeholder="请输入AppID" />
                </Form.Item>
                <Form.Item
                  label="AppSecret"
                  name="gzh_app_secret"
                  rules={[{ required: true, message: '请输入gzh_app_secret' }]}
                >
                  <Input allowClear placeholder="请输入gzh_app_secret" />
                </Form.Item>
                <Divider orientation="left">小程序配置</Divider>
                <Form.Item
                  label="AppID"
                  name="xcx_app_id"
                  rules={[{ required: true, message: '请输入AppID' }]}
                >
                  <Input allowClear placeholder="请输入AppID" />
                </Form.Item>
                <Form.Item
                  label="AppSecret"
                  name="xcx_app_secret"
                  rules={[{ required: true, message: '请输入gzh_app_secret' }]}
                >
                  <Input allowClear placeholder="请输入gzh_app_secret" />
                </Form.Item>
                <Form.Item
                  label="关于我们"
                  name="about"
                  rules={[{ required: true, message: '请输入关于我们' }]}
                >
                  <CBraftEditor
                    value={restFormRef.current?.getFieldValue('about')}
                    onChange={async (value: string) => {
                      await restFormRef.current?.setFieldsValue({ about: value });
                    }}
                  />
                </Form.Item>
              </>
            )}
          </Spin>
        </ProForm>
      </Card>
    </PageContainer>
  );
};
export default Index;
