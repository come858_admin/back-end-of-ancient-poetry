import React, { useRef, useState } from 'react';
import { Button, message, Form, Input, Radio, Spin, InputNumber, Select } from 'antd';
import { DrawerForm } from '@ant-design/pro-form';
import type { ProFormInstance } from '@ant-design/pro-form';
import { PlusOutlined, FormOutlined } from '@ant-design/icons';
import { add, edit, update } from '@/services/writingAdmin/setting/work/api';
import { getStorage } from '@/services/writingAdmin/setting/config/api';
interface PorpsType {
  onConfirm: () => void;
  id?: number;
}
const FormIndex: React.FC<PorpsType> = (porps) => {
  const restFormRef = useRef<ProFormInstance>();
  const [loading, setLoading] = useState(porps.id ? false : true);
  const [storageData, setStorageData] = useState<any>(null);
  const [storageDataLoading, setStorageDataLoading] = useState<boolean>(false);
  const { TextArea } = Input;
  const formData: API.WritingAdminWorkForm = {
    id: null,
    title: '',
    title_tr: '',
    author_id: null,
    author: '',
    author_tr: '',
    dynasty_id: null,
    dynasty: '',
    dynasty_tr: '',
    kind: '',
    kind_cn: '',
    kind_cn_tr: '',
    baidu_wiki: '',
    content: '',
    content_tr: '',
    intro: '',
    intro_tr: '',
    annotation: '',
    annotation_tr: '',
    translation: '',
    translation_tr: '',
    master_comment: '',
    master_comment_tr: '',
    quote: '',
    quote_tr: '',
    allusion_id: null,
    anthology_id: null,
    theme_id: null,
    brand_id: null,
    geography_id: null,
    famous_mountain_id: null,
    city_id: null,
    time_m_id: null,
    describe_scenery_id: null,
    festival_id: null,
    solar_term_id: null,
    season_id: null,
    flowers_plant_id: null,
    textbook_id: null,
    poem_book_id: null,
    status: 1,
    sort: 1,
    textbook_to: [],
  };
  return (
    <DrawerForm<API.WritingAdminWorkForm>
      layout="horizontal"
      labelCol={{ span: 3 }}
      wrapperCol={{ span: 20 }}
      formRef={restFormRef}
      initialValues={formData}
      title={porps.id ? '编辑' : '添加'}
      autoFocusFirstInput
      isKeyPressSubmit
      trigger={
        porps.id ? (
          <Button type="primary" size="small">
            <FormOutlined />
            编辑
          </Button>
        ) : (
          <Button type="primary">
            <PlusOutlined />
            添加
          </Button>
        )
      }
      drawerProps={{
        forceRender: true,
        destroyOnClose: true,
      }}
      submitter={{
        render: (props, defaultDoms) => {
          return [
            ...defaultDoms,
            <Button
              loading={loading ? false : true}
              key="extra-reset"
              onClick={async () => {
                const id = props.form?.getFieldValue('id');
                if (id) {
                  await setLoading(false);
                  const res = await edit(id);
                  if (res.status === 20000) {
                    restFormRef.current?.setFieldsValue(res.data);
                    await setLoading(true);
                  }
                } else {
                  props.reset();
                }
              }}
            >
              重置
            </Button>,
          ];
        },
      }}
      onFinish={async (values: any) => {
        values.dynasty = null;
        values.dynasty_tr = null;
        if (values.dynasty_id > 0) {
          for (let i = 0; i < storageData?.dynasty_list?.length; i++) {
            if (values.dynasty_id == storageData.dynasty_list[i].id) {
              values.dynasty = storageData.dynasty_list[i].name;
              values.dynasty_tr = storageData.dynasty_list[i].name_tr;
            }
          }
        }
        values.author = null;
        values.author_tr = null;
        if (values.author_id > 0) {
          for (let i = 0; i < storageData?.author_list?.length; i++) {
            if (values.author_id == storageData.author_list[i].id) {
              values.author = storageData.author_list[i].name;
              values.author_tr = storageData.author_list[i].name_tr;
            }
          }
        }
        let res: any = {};
        if (porps.id) {
          res = await update(porps.id, values);
        } else {
          res = await add(values);
        }
        if (res.status === 20000) {
          message.success(res.message);
          porps.onConfirm();
          return true;
        }
        return false;
      }}
      onVisibleChange={async (visible) => {
        if (visible) {
          await setStorageDataLoading(true);
          const resStorage = await getStorage();
          if (resStorage.status === 20000) {
            await setStorageData(resStorage.data);
            await setStorageDataLoading(false);
          }
          if (porps.id) {
            await setLoading(false);
            const res = await edit(porps.id);
            if (res.status === 20000) {
              await restFormRef.current?.setFieldsValue(res.data);
              await setLoading(true);
            }
          }
        }
      }}
    >
      <Spin spinning={loading ? false : true}>
        {loading && (
          <>
            <Form.Item label="用典" name="allusion_id">
              <Select
                allowClear={true}
                showSearch={true}
                filterOption={(input, option: any) => {
                  return option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0;
                }}
                loading={storageDataLoading}
                notFoundContent="暂无可选用典"
                placeholder="请选择用典"
              >
                {storageData?.allusion_list?.map((item: any) => {
                  return (
                    <Select.Option key={item.id} value={item.id}>
                      {item.name}
                    </Select.Option>
                  );
                })}
              </Select>
            </Form.Item>
            <Form.Item label="选集" name="anthology_id">
              <Select
                allowClear={true}
                showSearch={true}
                filterOption={(input, option: any) => {
                  return option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0;
                }}
                loading={storageDataLoading}
                notFoundContent="暂无可选选集"
                placeholder="请选择选集"
              >
                {storageData?.anthology_list?.map((item: any) => {
                  return (
                    <Select.Option key={item.id} value={item.id}>
                      {item.name}
                    </Select.Option>
                  );
                })}
              </Select>
            </Form.Item>
            <Form.Item label="主题" name="theme_id">
              <Select
                allowClear={true}
                showSearch={true}
                filterOption={(input, option: any) => {
                  return option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0;
                }}
                loading={storageDataLoading}
                notFoundContent="暂无可选主题"
                placeholder="请选择主题"
              >
                {storageData?.theme_list?.map((item: any) => {
                  return (
                    <Select.Option key={item.id} value={item.id}>
                      {item.name}
                    </Select.Option>
                  );
                })}
              </Select>
            </Form.Item>
            <Form.Item label="词牌" name="brand_id">
              <Select
                allowClear={true}
                showSearch={true}
                filterOption={(input, option: any) => {
                  return option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0;
                }}
                loading={storageDataLoading}
                notFoundContent="暂无可选词牌"
                placeholder="请选择词牌"
              >
                {storageData?.brand_list?.map((item: any) => {
                  return (
                    <Select.Option key={item.id} value={item.id}>
                      {item.name}
                    </Select.Option>
                  );
                })}
              </Select>
            </Form.Item>
            <Form.Item label="地理" name="geography_id">
              <Select
                allowClear={true}
                showSearch={true}
                filterOption={(input, option: any) => {
                  return option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0;
                }}
                loading={storageDataLoading}
                notFoundContent="暂无可选地理"
                placeholder="请选择地理"
              >
                {storageData?.geography_list?.map((item: any) => {
                  return (
                    <Select.Option key={item.id} value={item.id}>
                      {item.name}
                    </Select.Option>
                  );
                })}
              </Select>
            </Form.Item>
            <Form.Item label="名山" name="famous_mountain_id">
              <Select
                allowClear={true}
                showSearch={true}
                filterOption={(input, option: any) => {
                  return option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0;
                }}
                loading={storageDataLoading}
                notFoundContent="暂无可选名山"
                placeholder="请选择名山"
              >
                {storageData?.famous_mountain_list?.map((item: any) => {
                  return (
                    <Select.Option key={item.id} value={item.id}>
                      {item.name}
                    </Select.Option>
                  );
                })}
              </Select>
            </Form.Item>

            <Form.Item label="城市" name="city_id">
              <Select
                allowClear={true}
                showSearch={true}
                filterOption={(input, option: any) => {
                  return option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0;
                }}
                loading={storageDataLoading}
                notFoundContent="暂无可选城市"
                placeholder="请选择城市"
              >
                {storageData?.citys_mountain_list?.map((item: any) => {
                  return (
                    <Select.Option key={item.id} value={item.id}>
                      {item.name}
                    </Select.Option>
                  );
                })}
              </Select>
            </Form.Item>

            <Form.Item label="时间" name="time_m_id">
              <Select
                allowClear={true}
                showSearch={true}
                filterOption={(input, option: any) => {
                  return option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0;
                }}
                loading={storageDataLoading}
                notFoundContent="暂无可选时间"
                placeholder="请选择时间"
              >
                {storageData?.time_ms_mountain_list?.map((item: any) => {
                  return (
                    <Select.Option key={item.id} value={item.id}>
                      {item.name}
                    </Select.Option>
                  );
                })}
              </Select>
            </Form.Item>

            <Form.Item label="写景" name="describe_scenery_id">
              <Select
                allowClear={true}
                showSearch={true}
                filterOption={(input, option: any) => {
                  return option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0;
                }}
                loading={storageDataLoading}
                notFoundContent="暂无可选写景"
                placeholder="请选择写景"
              >
                {storageData?.describe_scenerys_mountain_list?.map((item: any) => {
                  return (
                    <Select.Option key={item.id} value={item.id}>
                      {item.name}
                    </Select.Option>
                  );
                })}
              </Select>
            </Form.Item>

            <Form.Item label="节日" name="festival_id">
              <Select
                allowClear={true}
                showSearch={true}
                filterOption={(input, option: any) => {
                  return option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0;
                }}
                loading={storageDataLoading}
                notFoundContent="暂无可选节日"
                placeholder="请选择节日"
              >
                {storageData?.festivals_mountain_list?.map((item: any) => {
                  return (
                    <Select.Option key={item.id} value={item.id}>
                      {item.name}
                    </Select.Option>
                  );
                })}
              </Select>
            </Form.Item>

            <Form.Item label="节气" name="solar_term_id">
              <Select
                allowClear={true}
                showSearch={true}
                filterOption={(input, option: any) => {
                  return option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0;
                }}
                loading={storageDataLoading}
                notFoundContent="暂无可选节气"
                placeholder="请选择节气"
              >
                {storageData?.solar_term_list?.map((item: any) => {
                  return (
                    <Select.Option key={item.id} value={item.id}>
                      {item.name}
                    </Select.Option>
                  );
                })}
              </Select>
            </Form.Item>

            <Form.Item label="时令" name="season_id">
              <Select
                allowClear={true}
                showSearch={true}
                filterOption={(input, option: any) => {
                  return option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0;
                }}
                loading={storageDataLoading}
                notFoundContent="暂无可选时令"
                placeholder="请选择时令"
              >
                {storageData?.season_list?.map((item: any) => {
                  return (
                    <Select.Option key={item.id} value={item.id}>
                      {item.name}
                    </Select.Option>
                  );
                })}
              </Select>
            </Form.Item>

            <Form.Item label="花卉" name="flowers_plant_id">
              <Select
                allowClear={true}
                showSearch={true}
                filterOption={(input, option: any) => {
                  return option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0;
                }}
                loading={storageDataLoading}
                notFoundContent="暂无可选花卉"
                placeholder="请选择花卉"
              >
                {storageData?.flowers_plant_list?.map((item: any) => {
                  return (
                    <Select.Option key={item.id} value={item.id}>
                      {item.name}
                    </Select.Option>
                  );
                })}
              </Select>
            </Form.Item>

            <Form.Item label="课本" name="textbook_to">
              <Select
                allowClear={true}
                showSearch={true}
                mode="tags"
                filterOption={(input, option: any) => {
                  return option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0;
                }}
                loading={storageDataLoading}
                notFoundContent="暂无可选课本"
                placeholder="请选择课本"
              >
                {storageData?.textbook_list?.map((item: any) => {
                  return (
                    <Select.Option key={item.id} value={item.id}>
                      {item.name}
                    </Select.Option>
                  );
                })}
              </Select>
            </Form.Item>
            <Form.Item
              label="标题"
              name="title"
              rules={[{ required: true, message: '请输入标题' }]}
            >
              <Input allowClear placeholder="请输入标题" />
            </Form.Item>

            <Form.Item
              label="标题繁体"
              name="title_tr"
              rules={[{ required: true, message: '请输入标题繁体' }]}
            >
              <Input allowClear placeholder="请输入标题繁体" />
            </Form.Item>

            <Form.Item label="作者" name="author_id">
              <Select
                allowClear={true}
                showSearch={true}
                filterOption={(input, option: any) => {
                  return option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0;
                }}
                loading={storageDataLoading}
                notFoundContent="暂无可选作者"
                placeholder="请选择作者"
              >
                {storageData?.author_list?.map((item: any) => {
                  return (
                    <Select.Option key={item.id} value={item.id}>
                      {item.name}
                    </Select.Option>
                  );
                })}
              </Select>
            </Form.Item>

            <Form.Item label="朝代" name="dynasty_id">
              <Select
                allowClear={true}
                showSearch={true}
                filterOption={(input, option: any) => {
                  return option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0;
                }}
                loading={storageDataLoading}
                notFoundContent="暂无可选朝代"
                placeholder="请选择朝代"
              >
                {storageData?.dynasty_list?.map((item: any) => {
                  return (
                    <Select.Option key={item.id} value={item.id}>
                      {item.name}
                    </Select.Option>
                  );
                })}
              </Select>
            </Form.Item>

            <Form.Item label="体裁拼音" name="kind">
              <Input allowClear placeholder="请输入体裁拼音" />
            </Form.Item>
            <Form.Item label="体裁" name="kind_cn">
              <Input allowClear placeholder="请输入体裁" />
            </Form.Item>
            <Form.Item label="体裁繁体" name="kind_cn_tr">
              <Input allowClear placeholder="请输入体裁繁体" />
            </Form.Item>

            <Form.Item label="抓取地址" name="baidu_wiki">
              <Input maxLength={100} allowClear placeholder="请输入抓取地址" />
            </Form.Item>

            <Form.Item label="内容" name="content">
              <TextArea showCount placeholder="请输入内容" />
            </Form.Item>

            <Form.Item label="内容繁体" name="content_tr">
              <TextArea showCount placeholder="请输入内容繁体" />
            </Form.Item>

            <Form.Item label="评析" name="intro">
              <TextArea showCount placeholder="请输入评析" />
            </Form.Item>

            <Form.Item label="评析繁体" name="intro_tr">
              <TextArea showCount placeholder="请输入评析繁体" />
            </Form.Item>

            <Form.Item label="注释" name="annotation">
              <TextArea showCount placeholder="请输入注释" />
            </Form.Item>

            <Form.Item label="注释繁体" name="annotation_tr">
              <TextArea showCount placeholder="请输入注释繁体" />
            </Form.Item>

            <Form.Item label="译文" name="translation">
              <TextArea showCount placeholder="请输入译文" />
            </Form.Item>

            <Form.Item label="译文繁体" name="translation_tr">
              <TextArea showCount placeholder="请输入译文繁体" />
            </Form.Item>

            <Form.Item label="辑评" name="master_comment">
              <TextArea showCount placeholder="请输入辑评" />
            </Form.Item>

            <Form.Item label="辑评繁体" name="master_comment_tr">
              <TextArea showCount placeholder="请输入辑评繁体" />
            </Form.Item>

            <Form.Item label="名句" name="quote">
              <TextArea showCount maxLength={100} placeholder="请输入名句" />
            </Form.Item>

            <Form.Item label="名句繁体" name="quote_tr">
              <TextArea showCount maxLength={100} placeholder="请输入名句繁体" />
            </Form.Item>

            <Form.Item
              name="status"
              label="状态"
              rules={[{ required: true, message: '请选择状态' }]}
            >
              <Radio.Group name="status">
                <Radio value={0}>禁用</Radio>
                <Radio value={1}>启用</Radio>
              </Radio.Group>
            </Form.Item>

            <Form.Item label="排序" name="sort" rules={[{ required: true, message: '请输入排序' }]}>
              <InputNumber
                maxLength={11}
                placeholder="请输入排序"
                min={0}
                style={{ width: '100%' }}
              />
            </Form.Item>
          </>
        )}
      </Spin>
    </DrawerForm>
  );
};
export default FormIndex;
