import React, { useState } from 'react';
import { Avatar, Empty, Button, Popconfirm, message } from 'antd';
import { PageContainer } from '@ant-design/pro-layout';
import styles from './index.less';
import { setStorage } from '@/services/writingAdmin/setting/config/api';
const Index: React.FC = () => {
  const [visible, setVisible] = useState(false);
  const [confirmLoading, setConfirmLoading] = useState(false);
  const handleOk = async () => {
    await setConfirmLoading(true);
    const res = await setStorage();
    if (res.status === 20000) {
      message.success(res.message);
    }
    await setVisible(false);
    await setConfirmLoading(false);
  };

  const handleCancel = () => {
    setVisible(false);
  };
  const showPopconfirm = () => {
    setVisible(true);
  };
  return (
    <PageContainer
      title={false}
      content={
        <div className={styles.pageHeaderContent}>
          <div className={styles.avatar}>
            <Avatar size="large" src="/logo.png" />
          </div>
          <div className={styles.content}>
            <div className={styles.contentTitle}>您好，欢迎来到后台管理，祝你开心每一天！</div>
            <div>
              后台管理是基于最新Laravel 8.5框架和Ant Design Pro 5.0的后台管理系统。创立于2022年初。
            </div>
          </div>
        </div>
      }
    >
      <>
        <Empty
          image="https://gw.alipayobjects.com/zos/antfincdn/ZHrcdLPrvN/empty.svg"
          imageStyle={{
            height: 60,
            marginTop: 100,
          }}
          description={<span>由于项目的信息较多，点击以下按钮将配置信息存入缓存。</span>}
        >
          <Popconfirm
            title="你确定要创新存储配置吗？"
            visible={visible}
            onConfirm={handleOk}
            okButtonProps={{ loading: confirmLoading }}
            onCancel={handleCancel}
          >
            <Button
              type="primary"
              onClick={() => {
                showPopconfirm();
              }}
            >
              存储配置
            </Button>
          </Popconfirm>
        </Empty>
      </>
    </PageContainer>
  );
};
export default Index;
