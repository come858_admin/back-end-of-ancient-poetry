// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  ssr: true,
  app: {
    head: {
      htmlAttrs: {
        lang: 'zh',
      },
      charset: 'utf-8',
      viewport: 'width = device-width, initial-scale = 1.0, maximum-scale = 1.0, user-scalable = 0',
      title: '程序员大象',
      meta: [
        {
          name: 'keywords',
          content: 'IT,前端，nuxt，ts，vue，php，ant design，react，lvacms作者',
        },
        {
          name: 'description',
          content:
            '（微信：songbo236589）我是一个程序员，工作有八年的时间了，目前从事前后端相关的一些开发，如果有需要共同学习开发相关知识的可以加我微信，可以备注加群，我可以拉你进入先关的微信群，微信：songbo236589。',
        },
      ],
    },
  },
  modules: ['@element-plus/nuxt', '@nuxtjs/html-validator', '@nuxtjs/color-mode'],
  css: ['~/assets/css/base.less'],
  colorMode: {
    classSuffix: '',
  },
  typescript: {
    strict: true,
    shim: false,
  },
  runtimeConfig: {
    isServer: true,
    public: {
      projectId: '我的项目ID',
      baseApiUrl: '我的API地址',
    },
  },
});
