export const useDefaultActive = () =>
  useState<string | undefined>('defaultActive', () => undefined);
export const useIsMode = () => useState<boolean>('isMode', () => true);
export const useIsModeHandle = () => useState<boolean>('isModeHandle', () => false);
