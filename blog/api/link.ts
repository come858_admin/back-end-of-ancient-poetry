export async function linkList() {
  return await useRequest('link/list', {
    method: 'POST',
    params: {
      limit: 100,
      page: 1,
    },
  });
}
