export async function pictureList(data: any = {}) {
  return await useRequest('picture/list', {
    method: 'POST',
    params: data,
  });
}
